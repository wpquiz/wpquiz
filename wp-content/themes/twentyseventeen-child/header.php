<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
    <title><?php wp_title( '', true,'' ); ?></title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,300,700' rel='stylesheet' type='text/css'/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>

<body <?php body_class(); ?>>
    <?php if ( ! is_home() && ! is_front_page() )
    {
        echo '<div class="inner_banner_agileinfo" id="home">';
    }
    ?>
	<div class="top_banner">
        <!-- SVG Arrows -->
        <div class="svg-wrap">
            <svg width="64" height="64" viewBox="0 0 64 64">
                <path id="arrow-left" d="M46.077 55.738c0.858 0.867 0.858 2.266 0 3.133s-2.243 0.867-3.101 0l-25.056-25.302c-0.858-0.867-0.858-2.269 0-3.133l25.056-25.306c0.858-0.867 2.243-0.867 3.101 0s0.858 2.266 0 3.133l-22.848 23.738 22.848 23.738z" />
            </svg>
            <svg width="64" height="64" viewBox="0 0 64 64">
                <path id="arrow-right" d="M17.919 55.738c-0.858 0.867-0.858 2.266 0 3.133s2.243 0.867 3.101 0l25.056-25.302c0.858-0.867 0.858-2.269 0-3.133l-25.056-25.306c-0.858-0.867-2.243-0.867-3.101 0s-0.858 2.266 0 3.133l22.848 23.738-22.848 23.738z" />
            </svg>
        </div>
        <div class="top_header_agile_info_kgls">
            <nav class="navbar navbar-default">
                <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h1><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><span>D</span>ecline</a></h1>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                     <div id="m_nav_container" class="m_nav wthree_bg">
                        <nav class="menu menu--sebastian">
                            <?php wp_nav_menu( array( 'menu' =>'Main Menu', 'menu_class' => 'm_nav menu__list', 'theme_location' => 'Top Menu','container' => 'false' ) ); ?>
                        </nav>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <?php if ( ! is_home() && ! is_front_page() )
    { ?>
        </div>
        <!--/kg_short-->
        <div class="services-breadcrumb">
            <div class="container">

                <ul class="kg_short">
                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a><i>|</i></li>
                    <li><?php echo get_the_title(); ?></li>
                </ul>
            </div>
        </div>
        <!--//kg_short-->
        <?php
    }
    ?>