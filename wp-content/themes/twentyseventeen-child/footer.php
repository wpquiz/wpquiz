<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
    <div class="footer">
        <div class="agileinfo_footer_bottom">
            <div class="container">
                <div class="col-md-12 agileinfo_footer_bottom_grid">
                	<ul class="footer-menu">
                		<li><a href="javascript:;">Login</a></li>
                		<li><a href="javascript:;">Faq</a></li>
                		<li><a href="javascript:;">About us</a></li>
                		<li><a href="javascript:;">Contact us</a></li>
                	</ul>                    
                    <ul class="social-nav model-3d-0 footer-social kg_agile_social">
                        <li>
                            <a href="#" class="facebook">
                                <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="twitter">
                                <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="instagram">
                                <div class="front"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="pinterest">
                                <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="agileinfo_footer_bottom1">
            <div class="container">
                <p>© 2017 knowledge season All rights reserved.</p>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //footer -->

    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
				<h1>Login to Your Account</h1><br>
			  <form>
				<input type="text" name="user" placeholder="Username">
				<input type="password" name="pass" placeholder="Password">
				<input type="submit" name="login" class="login loginmodal-submit" value="Login">
			  </form>
				
			  <div class="login-help">
				<a href="javascript:;" data-toggle="modal" data-target="#register-modal" data-dismiss="modal">Register</a> - <a href="javascript:;" data-toggle="modal" data-target="#forgot-modal" data-dismiss="modal">Forgot Password</a>
			  </div>
			</div>
		</div>
	  </div>

	  <!-- Register model -->
	  <div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
				<h1>Sign Up to Your Account</h1><br>
			  <form>
				<input type="text" name="user" placeholder="Username">
				<input type="text" name="email" placeholder="Email">
				<input type="text" name="password" placeholder="Password">
				<input type="text" name="confirm password" placeholder="Confirm Password">
				<input type="text" name="number" placeholder="number">
				<input type="submit" name="login" class="login loginmodal-submit" value="Sign Up">
			  </form>
				
			  <div class="login-help">
				Already have a account? <a href="javascript:;" data-toggle="modal" data-target="#login-modal" data-dismiss="modal">Login</a>
			  </div>
			</div>
		</div>
	  </div>

	  <!-- Register model -->
	  <div class="modal fade" id="forgot-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
				<h1>Forgot Password</h1><br>
			  <form>
				<input type="text" name="email" placeholder="Enter your email">
				<input type="submit" name="login" class="login loginmodal-submit" value="Forgot password">
			  </form>
			</div>
		</div>
	  </div>


			<!-- Register model -->
	  <div class="modal fade" id="student-portal-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
				<h1>Student Quick Register</h1><br>
			  <form>
				<input type="text" name="email" placeholder="Enter your register number">
				<input type="text" name="email" placeholder="Enter your name">
				<input type="text" name="email" placeholder="Enter your email">
				<input type="text" name="email" placeholder="Enter your mobile number">
				<input type="submit" name="login" class="login loginmodal-submit" value="Submit">
			  </form>
			</div>
		</div>
	  </div>

<!-- Cs model -->
	  <div class="modal fade course-model" id="cs-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			<h3>Computer science engineering</h3>
			<p>Here we have covered the question are base on the your engineering syllabus and the mainly focus on the technical knowledge related question, below we are got the question from your favorite subjects.</p>
			<ul>
				<li>Data Structures</li>
				<li>Analog And Digital Communication</li>
				<li>Object Oriented Programming</li>
				<li>Design And Analysis Of Algorithms</li>
				<li>Operating Systems</li>
				<li>Database Management Systems</li>
				<li>System Software</li>
				<li>Software Engineering</li>
				<li>Advanced Database Technology</li>
				<li>Wireless Networks</li>
				<li>Computer Graphics and Multimedia</li>
				<li>Software Project Management</li>
				<li>Web Technology</li>
			</ul>
			<a href="test.html" id='start' class="login loginmodal-submit">Take a Test </a> 
			</div>
		</div>
	  </div>

	<!-- ece model -->
	  <div class="modal fade course-model" id="ece-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			<h3>electronic communication engineering</h3>
			<p>Here we have covered the question are base on the your engineering syllabus and the mainly focus on the technical knowledge related question, below we are got the question from your favorite subjects.</p>
			<ul>
				<li>Digital Electronics</li>
				<li>Signals And Systems</li>
				<li>Data Structures</li>
				<li>Electromagnetic Fields</li>
				<li>Control Systems</li>
				<li>Probability and Random Processes</li>
				<li>Digital Communication</li>
				<li>Microprocessors And Microcontroller</li>
				<li>Transmission Lines And Wave Guides</li>
				<li>VLSI Design</li>
				<li>Satellite Communication</li>
				<li>Digital Image Processing</li>
				<li>Wireless Networks</li>
			</ul>
			<input type="submit" name="login" class="login loginmodal-submit" value="Take a Test">
			</div>
		</div>
	  </div>

	  <!-- Cs model -->
	  <div class="modal fade course-model" id="it-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			<h3>information technology</h3>
			<p>Here we have covered the question are base on the your engineering syllabus and the mainly focus on the technical knowledge related question, below we are got the question from your favorite subjects.</p>
			<ul>
				<li>Data Structures</li>
				<li>Analog And Digital Communication</li>
				<li>Object Oriented Programming</li>
				<li>Design And Analysis Of Algorithms</li>
				<li>Operating Systems</li>
				<li>Database Management Systems</li>
				<li>System Software</li>
				<li>Software Engineering</li>
				<li>Advanced Database Technology</li>
				<li>Wireless Networks</li>
				<li>Computer Graphics and Multimedia</li>
				<li>Software Project Management</li>
				<li>Web Technology</li>
			</ul>
			<input type="submit" name="login" class="login loginmodal-submit" value="Take a Test">
			</div>
		</div>
	  </div>

	  <!-- Cs model -->
	  <div class="modal fade course-model" id="aero-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			<h3>Aeronautical engineering</h3>
			<p>Here we have covered the question are base on the your engineering syllabus and the mainly focus on the technical knowledge related question, below we are got the question from your favorite subjects.</p>
			<ul>
				<li>Aircraft Structures</li>
				<li>Propulsion</li>
				<li>Aircraft Structures</li>
				<li>Aerodynamics</li>
				<li>Wind Tunnel Techniques</li>
				<li>High Temperature Materials</li>
				<li>Experimental Stress Analysis</li>
				<li>Aero Engine Maintenance and Repair</li>
				<li>Computational Fluid Dynamics</li>
				<li>Vibrations And Elements Of Aero Elasticity</li>
				<li>Airframe Maintenance And Repair</li>
				<li>Air Traffic Control And Planning</li>
				<li>Composite Materials And Structures</li>
			</ul>
			<input type="submit" name="login" class="login loginmodal-submit" value="Take a Test">
			</div>
		</div>
	  </div>

	  <!-- Cs model -->
	  <div class="modal fade course-model" id="civil-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			<h3>Civil engineering</h3>
			<p>Here we have covered the question are base on the your engineering syllabus and the mainly focus on the technical knowledge related question, below we are got the question from your favorite subjects.</p>
			<ul>
				<li>Mechanics Of Fluids</li>
				<li>Mechanics Of Solids</li>
				<li>Surveying</li>
				<li>Applied Geology</li>
				<li>Applied Hydraulics Engineering</li>
				<li>Soil Mechanics</li>
				<li>Highway Engineering</li>
				<li>Foundation Engineering</li>
				<li>Irrigation Engineering</li>
				<li>Hydrology</li>
				<li>Ground Improvement Techniques</li>
				<li>Industrial Waste Management</li>
				<li>Municipal Solid Waste And Management</li>
			</ul>
			<input type="submit" name="login" class="login loginmodal-submit" value="Take a Test">
			</div>
		</div>
	  </div>

	  <!-- Cs model -->
	  <div class="modal fade course-model" id="eee-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			<h3>electrical and electronics engineering</h3>
			<p>Here we have covered the question are base on the your engineering syllabus and the mainly focus on the technical knowledge related question, below we are got the question from your favorite subjects.</p>
			<ul>
				<li>Circuit Theory</li>
				<li>Electronic Devices And Circuits</li>
				<li>Control Systems</li>
				<li>Power Plant Engineering</li>
				<li>Digital Logic Circuits</li>
				<li>Digital Signal Processing</li>
				<li>Transmission And Distribution</li>
				<li>Power Electronics</li>
				<li>Communication Engineering</li>
				<li>Microprocessors and Microcontrollers</li>
				<li>Power System Analysis</li>
				<li>Renewable Energy Sources</li>
				<li>Power Quality</li>
			</ul>
			<input type="submit" name="login" class="login loginmodal-submit" value="Take a Test">
			</div>
		</div>
	  </div>

	  <!-- Cs model -->
	  <div class="modal fade course-model" id="mech-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			<h3>mechanical engineering</h3>
			<p>Here we have covered the question are base on the your engineering syllabus and the mainly focus on the technical knowledge related question, below we are got the question from your favorite subjects.</p>
			<ul>
				<li>Engineering Thermodynamics</li>
				<li>Electrical Drives and Control</li>
				<li>Fluid Mechanics and Machinery</li>
				<li>Engineering Materials and Metallurgy</li>
				<li>Strength of Materials</li>
				<li>Statistics and Numerical Methods</li>
				<li>Thermal Engineering</li>
				<li>Kinematics of Machinery</li>
				<li>Design of Machine Elements</li>
				<li>Dynamics Of Machinery</li>
				<li>Heat and Mass Transfer</li>
				<li>Production Planning and Control</li>
				<li>Maintenance Engineering</li>
			</ul>
			<input type="submit" name="login" class="login loginmodal-submit" value="Take a Test">
			</div>
		</div>
	  </div>

	  <!-- Cs model -->
	  <div class="modal fade course-model" id="auto-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			<h3>automobile engineering</h3>
			<p>Here we have covered the question are base on the your engineering syllabus and the mainly focus on the technical knowledge related question, below we are got the question from your favorite subjects.</p>
			<ul>
				<li>Fluid Mechanics and Machinery</li>
				<li>Mechanics Of Machines</li>
				<li>Applied Thermodynamics And Heat Transfer</li>
				<li>Strength of Materials</li>
				<li>Statistics and Numerical Methods</li>
				<li>Database Management Systems</li>
				<li>Engineering Materials and Metallurgy</li>
				<li>Automotive Electrical And Electronics</li>
				<li>Vehicle Design And Data Characteristics</li>
				<li>Automotive Fuels And Lubricants</li>
				<li>Design of Machine Elements</li>
				<li>Finite Element Analysis</li>
				<li>Robotics</li>
			</ul>
			<input type="submit" name="login" class="login loginmodal-submit" value="Take a Test">
			</div>
		</div>
	  </div>

	  <!-- Cs model -->
	  <div class="modal fade course-model" id="bio-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			<h3>biomedical engineering</h3>
			<p>Here we have covered the question are base on the your engineering syllabus and the mainly focus on the technical knowledge related question, below we are got the question from your favorite subjects.</p>
			<ul>
				<li>Signals And Systems</li>
				<li>Basics Of Electrical Engineering</li>
				<li>Analog And Digital Communication</li>
				<li>Pathology And Microbiology</li>
				<li>Probability and Random Processes</li>
				<li>Digital Signal Processing</li>
				<li>Radiological Equipment</li>
				<li>Medical Optics</li>
				<li>Digital Image Processing</li>
			</ul>
			<input type="submit" name="login" class="login loginmodal-submit" value="Take a Test">
			</div>
		</div>
	  </div>

<?php wp_footer(); ?>

</body>
</html>
