<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
 ?>

<?php
get_header(); ?>
         <div class="sleekslider" id="home">
            <!-- Slider Pages -->
            <div class="slide active bg-1">
                <div class="slide-container">
                    <div class="slide-content">
                    	<h2><?php echo get_field('slide_content_1'); ?></h2>
                    </div>
                </div>
            </div>
            <div class="slide bg-2">
                <div class="slide-container">
                    <div class="slide-content">
                    	<h2><?php echo get_field('slide_content_2'); ?></h2>
                    </div>
                </div>
            </div>
            <div class="slide bg-3">
                <div class="slide-container">
                    <div class="slide-content">
                    	<h2><?php echo get_field('slide_content_3'); ?></h2>
                    </div>
                </div>
            </div>
            <div class="slide bg-4">
                <div class="slide-container">
                    <div class="slide-content">
                    	<h2><?php echo get_field('slide_content_4'); ?></h2>
                    </div>
                </div>
            </div>
            <div class="slide bg-5">
                <div class="slide-container">
                    <div class="slide-content">
                    	<h2><?php echo get_field('slide_content_5'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <!--welcome-->
    </div>

    <div class="about-us" id="about">
    	<h3 class="title-kg-agileits"><?php echo get_field('about_us_title'); ?></h3>
    	<h1><?php echo get_field('about_us_caption_text'); ?></h1>
    	<?php echo get_field('about_us_content'); ?>
    </div>

    <!--/about-kg-agile-info-->
    <div class="about-kg-agileinfo">
        <div class="container">
            <div class="about-top kgls-agile">
                <div class="col-md-6 red-kgl">
                    <div class="red-kgl-images">
                        <img class="img-responsive" src="<?php echo get_field('online_exam_image'); ?>" alt="">
                    </div>
                </div>
                <div class="col-md-6 come">
                    <div class="about-wel">
                        <h3 class="title-kg-agileits"><?php echo get_field('online_exam_title'); ?></h3>
                        <?php echo get_field('online_exam_content'); ?>
                    </div>
                    <div class="steps-wel">
                        <h5><?php echo get_field('exam_steps_title'); ?></h5>
                        <div class="col-md-3 col-sm-3 col-xs-3 kgls_banner_bottom_grids first-posi-kgl">
                            <div class="kgl_banner_bottom_grid1">
                                <i class="fa fa-sign-in hvr-pulse-shrink" aria-hidden="true"></i>
                            </div>
                            <div class="kgl_banner_bottom_grid1">
                                <i class="fa fa-list hvr-pulse-shrink" aria-hidden="true"></i>
                            </div>
                            <div class="kgl_banner_bottom_grid1">
                                <i class="fa fa-file-text-o hvr-pulse-shrink" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 kgls_banner_bottom_grids">
                            <h6><?php echo get_field('exam_question_number_1'); ?></h6>
                            <h6><?php echo get_field('exam_question_number_2'); ?></h6>
                            <h6><?php echo get_field('exam_question_number_3'); ?></h6>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-7 kgls_banner_bottom_grids">
                            <p><?php echo get_field('exam_question_number_1_title'); ?></p>
                            <p><?php echo get_field('exam_question_number_2_title'); ?></p>
                            <p><?php echo get_field('exam_question_number_3_title'); ?></p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!--//about-kg-agile-info-->
    <!-- stats -->
    <div class="stats" id="stats">
        <div class="container">
            <div class="inner_kgl_agile_grids">
                <div class="col-md-3 kg_stats_left kg_counter_grid">
                    <i class="fa fa-university" aria-hidden="true"></i>
                    <p class="counter"><?php echo get_field('title_1_count'); ?></p>
                    <h3><?php echo get_field('title_1'); ?></h3>
                </div>
                <div class="col-md-3 kg_stats_left kg_counter_grid1">
                    <i class="fa fa-building" aria-hidden="true"></i>
                    <p class="counter"><?php echo get_field('title_2_count'); ?></p>
                    <h3><?php echo get_field('title_2'); ?></h3>
                </div>
                <div class="col-md-3 kg_stats_left kg_counter_grid2">
                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    <p class="counter"><?php echo get_field('title_3_count'); ?></p>
                    <h3><?php echo get_field('title_3'); ?></h3>
                </div>
                <div class="col-md-3 kg_stats_left kg_counter_grid3">
                    <i class="fa fa-laptop" aria-hidden="true"></i>
                    <p class="counter"><?php echo get_field('title_4_count'); ?></p>
                    <h3><?php echo get_field('title_4'); ?></h3>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //stats -->
    <!-- banner-bottom -->
    <div class="banner-bottom" id="categories">
        <div class="tittle-agileinfo">
            <h3 class="title-kg-agileits"><?php echo get_field('q/a_categories_title'); ?></h3>
        </div>
        <div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
            <a href="javascript:;" data-toggle="modal" data-target="#cs-modal">
	            <div class="agileinfo_banner_bottom_pos">
        			<img src="<?php echo get_field('q/a_categories_1_image'); ?>" class="course">
                <div class="iconandimg">
	                <img class="icon" src="<?php echo get_field('q/a_categories_1_icon'); ?>" alt="icon">
	                <h4><?php echo get_field('q/a_categories_1_content'); ?></h4>
	            </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
            <a href="javascript:;" data-toggle="modal" data-target="#ece-modal">
	            <div class="agileinfo_banner_bottom_pos">
        			<img src="<?php echo get_field('q/a_categories_2_image'); ?>" class="course">
                <div class="iconandimg">
	                <img class="icon" src="<?php echo get_field('q/a_categories_2_icon'); ?>" alt="icon">
	                <h4><?php echo get_field('q/a_categories_2_content'); ?></h4>
	            </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
            <a href="javascript:;" data-toggle="modal" data-target="#it-modal">
	            <div class="agileinfo_banner_bottom_pos">
        			<img src="<?php echo get_field('q/a_categories_3_image'); ?>" class="course">
                <div class="iconandimg">
	                <img class="icon" src="<?php echo get_field('q/a_categories_3_icon'); ?>" alt="icon">
	                <h4><?php echo get_field('q/a_categories_3_content'); ?></h4>
	            </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
            <a href="javascript:;" data-toggle="modal" data-target="#aero-modal">
	            <div class="agileinfo_banner_bottom_pos">
        			<img src="<?php echo get_field('q/a_categories_4_image'); ?>" class="course">
                <div class="iconandimg">
	                <img class="icon" src="<?php echo get_field('q/a_categories_4_icon'); ?>" alt="icon">
	                <h4><?php echo get_field('q/a_categories_4_content'); ?></h4>
	            </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
            <a href="javascript:;" data-toggle="modal" data-target="#civil-modal">
	            <div class="agileinfo_banner_bottom_pos">
        			<img src="<?php echo get_field('q/a_categories_5_image'); ?>" class="course">
                <div class="iconandimg">
	                <img class="icon" src="<?php echo get_field('q/a_categories_5_icon'); ?>" alt="icon">
	                <h4><?php echo get_field('q/a_categories_5_content'); ?></h4>
	            </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
            <a href="javascript:;" data-toggle="modal" data-target="#eee-modal">
	            <div class="agileinfo_banner_bottom_pos">
        			<img src="<?php echo get_field('q/a_categories_6_image'); ?>" class="course">
                <div class="iconandimg">
	                <img class="icon" src="<?php echo get_field('q/a_categories_6_icon'); ?>" alt="icon">
	                <h4><?php echo get_field('q/a_categories_6_content'); ?></h4>
	            </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
            <a href="javascript:;" data-toggle="modal" data-target="#mech-modal">
	            <div class="agileinfo_banner_bottom_pos">
        			<img src="<?php echo get_field('q/a_categories_7_image'); ?>" class="course">
                <div class="iconandimg">
	                <img class="icon" src="<?php echo get_field('q/a_categories_7_icon'); ?>" alt="icon">
	                <h4><?php echo get_field('q/a_categories_7_content'); ?></h4>
	            </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
            <a href="javascript:;" data-toggle="modal" data-target="#auto-modal">
	            <div class="agileinfo_banner_bottom_pos">
        			<img src="<?php echo get_field('q/a_categories_8_image'); ?>" class="course">
                <div class="iconandimg">
	                <img class="icon" src="<?php echo get_field('q/a_categories_8_icon'); ?>" alt="icon">
	                <h4><?php echo get_field('q/a_categories_8_content'); ?></h4>
	            </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
            <a href="javascript:;" data-toggle="modal" data-target="#bio-modal">
	            <div class="agileinfo_banner_bottom_pos">
        			<img src="<?php echo get_field('q/a_categories_9_image'); ?>" class="course">
                <div class="iconandimg">
	                <img class="icon" src="<?php echo get_field('q/a_categories_9_icon'); ?>" alt="icon">
	                <h4><?php echo get_field('q/a_categories_9_content'); ?></h4>
	            </div>
                </div>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>
    <!-- //banner-bottom -->
    <!-- Blog -->
    <div class="blog" id="blog">
        <div class="container">
            <div class="tittle-agileinfo">
                <h3 class="title-kg-agileits"><?php echo get_custom('home_our_blog_title'); ?></h3>
            </div>
            <div class="col-md-6 kgls_banner_bottom_right">
                <section class="slider">
                    <div class="flexslider">
                        <ul class="slides">
                        	<?php 
                    		$blogPosts = get_posts( array( 'post_type' => 'post', 'numberposts' => 3,'post_status' => 'publish', 'orderby' => 'menu_order title', 'order'   => 'DESC') );
                    		//echo "<pre>",print_r($blogPosts);
                    		foreach ( $blogPosts as $blog ) :
                    			$authorId = $blog->post_author; 
                    			$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id( $blog->ID ), 'full' ); 
							    $title = get_the_title( $blog->ID ); 
							    $postUrl = get_permalink( $blog->ID );
							    $postDate = get_the_date( 'j F, Y' );
							    //$commentsNumber = wp_count_comments( $blog->ID );
                    		?>
                            <li>
                                <div class="agileits_kg_banner_bottom_grid">
                                    <img src="<?php echo $backgroundImg[0]; ?>" alt=" " class="img-responsive" />
                                    <div class="blog-info-agileits-kg">
                                        <h5><a href="<?php echo $postUrl; ?>"><?php echo $title ; ?></a></h5>
                                        <ul>
                                            <li><i class="fa fa-user" aria-hidden="true"></i><?php echo get_the_author_meta( 'display_name', $authorId ); ?></li>
                                            <li><i class="fa fa-calendar" aria-hidden="true"></i><?php echo $postDate; ?></li>
                                            <li><i class="fa fa-comments" aria-hidden="true"></i><?php //echo //$commentsNumber; ?> Comments</li>
                                        </ul>
                                        <p class="para-kg"><?php echo wpautop( wp_trim_words( get_the_content(), 40, '...' ) ); ?></p>
                                        <div class="wthree_more wthre( e_more1">
                                            <a href="<?php echo $postUrl; ?>">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        	<?php endforeach; ?>
                        </ul>
                    </div>
                </section>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //Blog -->

    <!-- footer -->

        <!-- stats -->
    <div class="stats contact-us" id="contactus">
        <div class="container">
        	<h3 class="title-kg-agileits">Contact us</h3>
            <div class="inner_kgl_agile_grids">
                <div class="col-md-4 kg_stats_left kg_counter_grid">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <h3>9789359171</h3>
                </div>
                <div class="col-md-4 kg_stats_left kg_counter_grid1">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    <h3>contact@knowledgeseason.com</h3>
                </div>
                <div class="col-md-4 kg_stats_left kg_counter_grid2">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <h3>Chennai</h3>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //stats -->

 <?php get_footer(); ?>