jQuery(document).ready(function() {
    if ($('.cd-stretchy-nav').length > 0) {
        var stretchyNavs = $('.cd-stretchy-nav');

        stretchyNavs.each(function() {
            var stretchyNav = $(this),
                stretchyNavTrigger = stretchyNav.find('.cd-nav-trigger');

            stretchyNavTrigger.on('click', function(event) {
                event.preventDefault();
                stretchyNav.toggleClass('nav-is-visible');
            });
        });

        $(document).on('click', function(event) {
            (!$(event.target).is('.cd-nav-trigger') && !$(event.target).is('.cd-nav-trigger span')) && stretchyNavs.removeClass('nav-is-visible');
        });
    }


    // Reverse stop watch
    function countdown( elementName, minutes, seconds )
	{
	    var element, endTime, hours, mins, msLeft, time;

	    function twoDigits( n )
	    {
	        return (n <= 9 ? "0" + n : n);
	    }

	    function updateTimer()
	    {
	        msLeft = endTime - (+new Date);
	        if ( msLeft < 1000 ) {
	            element.innerHTML = "<p class='end-time blink blink-furiously blink-infinite'>Your time is over</p>";
	            $('.time-remain-wrap').hide();
	        } else {
	            time = new Date( msLeft );
	            hours = time.getUTCHours();
	            mins = time.getUTCMinutes();
	            element.innerHTML = (hours ? hours + ':' + twoDigits( mins ) : mins) + ':' + twoDigits( time.getUTCSeconds() );
	            setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
	        }
	    }

	    element = document.getElementById( elementName );
	    endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
	    updateTimer();
	}

	countdown( "countdown", 30, 00 );
	// countdown( "countdown2", 100, 0 );



    // Stop watch

	//    var m = 0,
	//    s = 0,
	//    startTime = 0,
	//    timerOn;

	// $('.timer').html('0:00:00');
	// $('#pause').css('display', 'none');

	// var timer = {
	//   start : function () {
	//             $('#start').css('display', 'none');
	//             $('#pause').css('display', 'inline');
	//                startTime = 0 + startTime;
	//              //Start interval
	//                timerOn = setInterval(function() {
	//                startTime += 1;
	//                // Add seconds
	//                if (startTime == 100) {
	//                  startTime = 0;
	//                  s += 1;
	//                }  
	//                // Add minutes
	//                if (s == 60) {
	//                  startTime = 0;
	//                  s = 0;
	//                  m += 1;
	//                } 
	//                // Add extra zeroes
	//                (startTime < 10 && s < 10) ? $('.timer').html(m + ':' + '0' + s + ':' + '0' +     startTime) : 
	//                (startTime < 10 && s > 10) ? $('.timer').html(m + ':' + s + ':' + '0' + startTime) :
	//                (s < 10) ? $('.timer').html(m + ':' + '0' + s + ':' + startTime) :
	//                           $('.timer').html(m + ':' + s + ':' + startTime);
	//            }, 10);
	//          },
	//   pause : function () {
	//             $('#start').css('display', 'inline');
	//             $('#pause').css('display', 'none');
	//             setTimeout(function() {
	//               clearInterval(timerOn);
	//             }, 0);
	//           },
	//   reset : function () {
	//             $('#start').css('display', 'inline');
	//             $('#pause').css('display', 'none');
	//             setTimeout(function() {
	//               clearInterval(timerOn);
	//               $('.timer').html('0:00:00');
	//               startTimer = 0;
	//               s = 0;
	//               m = 0;
	//             }, 0);
	//   }
	// }

	// Radio button and check box not checked
	// $(".input[type='radio'], input[type='checkbox']").each(function(){
	// 	if ($(this).not(':checked')) {
	// 		$(".count-down a").addClass('not-visited');
	// 	}
	// });

	// Radio button checked and add class
	$("input[type='radio']").click(function(){
		if ($(this).is(':checked')) {
            $(".count-down a.active").addClass('answered');
        }
	});

	// Checkbox button checked and add class
	$("input[type='checkbox']").change(function(){
		if ($(this).is(':checked')){
			console.log("lenth");
            $(".count-down a.active").addClass('answered');
        }
        else if($(this).filter(':checked').val() >= 1){
        	$(".count-down a.active").addClass('answered');
        }
        else{
        	$(".count-down a.active").removeClass('answered');	
        }
	});

	// Visited
		if($('#qus-wrap li').css("display") == "none"){
			$(".count-down .page").addClass('not-visited');
		}
		else{
			$(".count-down .page").addClass('not-answered');
		}	

	// custom
	$('#save-next').hide();
	$('#hide-question').hide();
	$('#pagination-demo').hide();
	
	$('#start-test').click(function() {
	  $('Empty-qustion').hide().fadeIn();
	  $('#hide-question').show();
	  $('#save-next').show();
	  $('#start-test').hide();
	  $('#empty-qustion').hide();
	  $('#pagination-demo').show().fadeIn(2200);
	  $('.timer').html(timer.start());
	});


	// list get dynamic id

	$('#qus-wrap ul li').each(function(i, e){
	    $(this).attr("id", "qus_" + i);
	});


	$('#save-next').click(function() {
	    new_page = parseInt($('#current_page').val(), 0) + 1;
	    if ($('.active').next('.page').length == true) {
	        go_to_page(new_page);
	        $('#save-next').attr("disabled", true);
	    }
	});  

	// Empty validation

	var emptychecks = $("input[type='checkbox'], input[type='radio']");
	emptychecks.click(function() {
	   if($(this).is(':checked')) {
		$('#save-next').attr("disabled", false); 
	   }
	   else{
	   	$('#save-next').attr("disabled", true);
	   }
	});

	// Get total number of questions
	var getTotalQus = $('#qus-wrap li').length;
	$('#total-question').html(getTotalQus);

	// Total visited questions
	$('#save-next').click(function() {
		$("#total-answer-question").html(function(i, val) {
		 return val*1+1; 
		});
	}); 


	//Start time out
	$('#start-countdown').hide();
	$("#start-test").click(function(){
		setTimeout(function () {
             $('#start-countdown').fadeIn();
         }, 10); 

		setTimeout(function () {
             $('#start-countdown').fadeOut();
         }, 6000); 
	});





});

	// pagination
	jQuery(document).ready(function($) {

	    var show_per_page = 1;
	    var number_of_items = $('#qus-wrap').children('li').size();
	    var number_of_pages = Math.ceil(number_of_items / show_per_page);

	    $('#pagination-demo').append('<div class=controls></div><input id=current_page type=hidden><input id=show_per_page type=hidden>');
	    $('#current_page').val(0);
	    $('#show_per_page').val(show_per_page);

	    var navigation_html = '<span class="prev-pagination" onclick="previous()">Prev</span>';
	    var current_link = 0;
	    while (number_of_pages > current_link) {
	        navigation_html += '<a class="page" onclick="go_to_page(' + current_link + ')" longdesc="' + current_link + '">' + (current_link + 1) + '</a>';
	        current_link++;
	    }
	    navigation_html += '<span class="next-pagination" onclick="next()">Next</span>';

	    $('.controls').html(navigation_html);
	    $('.controls .page:first').addClass('active');

	    $('#qus-wrap').children().css('display', 'none');
	    $('#qus-wrap').children().slice(0, show_per_page).css('display', 'block');

	    // Length
	    $('.count-down a').click(function(index){
	    	$('#total-answer-question').html($(this).index());
	    });
	});



	function go_to_page(page_num) {
	    var show_per_page = parseInt($('#show_per_page').val(), 0);

	    start_from = page_num * show_per_page;

	    end_on = start_from + show_per_page;

	    $('#qus-wrap').children().css('display', 'none').slice(start_from, end_on).css('display', 'block');

	    $('.page[longdesc=' + page_num + ']').addClass('active').siblings('.active').removeClass('active');

	    $('#current_page').val(page_num);

	}

	function previous() {

	    new_page = parseInt($('#current_page').val(), 0) - 1;
	    //if there is an item before the current active link run the function
	    if ($('.active').prev('.page').length == true) {
	        go_to_page(new_page);
	    }

	}
	function next() {
	    new_page = parseInt($('#current_page').val(), 0) + 1;
	    //if there is an item after the current active link run the function
	    if ($('.active').next('.page').length == true) {
	        go_to_page(new_page);
	    }

	}



