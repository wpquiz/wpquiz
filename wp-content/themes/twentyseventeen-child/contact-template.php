<?php
/**
 * Template Name: Contact Template
 *
 */
 ?>
<?php get_header();?>
<?php while ( have_posts() ) : the_post(); ?>

 <!-- contact -->
	<div class="inner_section_kgls">
		<div class="container">
				<div class="agileits_kglayouts_mail">
				<div class="col-md-4 agileits_kglayouts_mail_left">
					<div class="kg_agileits_mail_left">
						<h4><i class="fa fa-home" aria-hidden="true"></i><?php echo get_field('left_block_section_title'); ?></h4>
						<p class="wthree_address"><?php echo get_field('left_block_section_content'); ?></p>
					</div>
				</div>
				<div class="col-md-4 agileits_kglayouts_mail_left">
					<div class="kg_agileits_mail_left">
						<h4><i class="fa fa-envelope" aria-hidden="true"></i><?php echo get_field('center_block_section_title'); ?></h4>
						<div class="kg_agileits_mail_left1">
							<?php echo get_field('center_block_section_content'); ?>
						</div>
					</div>
				</div>
				<div class="col-md-4 agileits_kglayouts_mail_left">
					<div class="kg_agileits_mail_left">
						<h4><i class="fa fa-phone" aria-hidden="true"></i><?php echo get_field('right_block_section_title'); ?></h4>
						<div class="kg_agileits_mail_left1">
							<?php echo get_field('right_block_section_title'); ?>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			
		</div>
	</div>
	<div class="agileits_map">
		<div class="container">
		    <div class="tittle-agileinfo">
			   <h3 class="title-kg-agileits">Find us on map</h3>
			</div>
				
		</div>
		<div class="kg_services_grids">
			<?php the_content(); ?>
		</div>
	</div>
	<div class="services inner_section_kgls">
		<div class="container">
				<h3 class="title-kg-agileits"><?php echo get_field('contact_form_title'); ?></h3>
			<p class="agileits_dummy_para"><?php echo get_field('contact_form_description'); ?></p>
			<div class="agileinfo_mail_grids">
				<?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
			</div>
		</div>
	</div>
<?php endwhile; ?>
<!-- //contact -->
<?php get_footer();?>