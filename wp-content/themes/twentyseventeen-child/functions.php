<?php

/*Add css and js to front-End*/

add_action( 'wp_enqueue_scripts', 'qz_enqueue_style' );

function qz_enqueue_style() {
	$parent_style = 'parent-style';
    wp_enqueue_style( $parent_style, get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri(). '/css/bootstrap.css' );
	wp_enqueue_style( 'flexslider', get_stylesheet_directory_uri(). '/css/flexslider.css' );
	wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri(). '/css/font-awesome.css' );
	wp_enqueue_style( 'lightcase', get_stylesheet_directory_uri(). '/css/lightcase.css' );
	wp_enqueue_style( 'owl-carousel-css', get_stylesheet_directory_uri(). '/css/owl.carousel.css' );
	wp_enqueue_style( 'popuo-box', get_stylesheet_directory_uri(). '/css/popuo-box.css' );
	wp_enqueue_style( 'slider', get_stylesheet_directory_uri(). '/css/slider.css' );
	wp_enqueue_script( 'app', get_stylesheet_directory_uri() . '/js/app.js');
	wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/js/bootstrap.js' );
	wp_enqueue_script( 'classie', get_stylesheet_directory_uri() . '/js/classie.js' );
	wp_enqueue_script( 'easing', get_stylesheet_directory_uri() . '/js/easing.js');
	wp_enqueue_script( 'jquery-countup', get_stylesheet_directory_uri() . '/js/jquery-countup.js' );
	wp_enqueue_script( 'jquery-events-touch', get_stylesheet_directory_uri() . '/js/jquery-events-touch.js' );
	wp_enqueue_script( 'jquery-flexslider', get_stylesheet_directory_uri() . '/js/jquery-flexslider.js' );
	wp_enqueue_script( 'jquery-magnific-popup', get_stylesheet_directory_uri() . '/js/jquery-magnific-popups.js' );
	wp_enqueue_script( 'jquery-waypoints-min', get_stylesheet_directory_uri() . '/js/jquery-waypoints-min.js' );
	wp_enqueue_script( 'jquery-min', get_stylesheet_directory_uri() . '/js/jquery-min.js' );
	wp_enqueue_script( 'lightcase', get_stylesheet_directory_uri() . '/js/lightcase.js' );
	wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/js/main.js' );
	wp_enqueue_script( 'move-top', get_stylesheet_directory_uri() . '/js/move-top.js' );
	wp_enqueue_script( 'owl-carousel-js', get_stylesheet_directory_uri() . '/js/owl-carousel-js.js' );
	wp_enqueue_script( 'sleekslider', get_stylesheet_directory_uri() . '/js/sleekslider.js' );
}

/*Add js to Admin Dash board*/

add_action( 'admin_enqueue_scripts', 'custom_admin_enqueue_scripts' );
function custom_admin_enqueue_scripts() {
   
    wp_enqueue_script( 'my_custom_script', get_stylesheet_directory_uri() . '/js/custom-script.js' );
}

/*Custom Post -Quiz Questions*/

// Register Custom Post Type
add_action( 'init', 'codex_questions_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_questions_init() {
	$labels = array(
		'name'               => _x( 'Questions', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Question', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Questions', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Question', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'question', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Question', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Question', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Question', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Question', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Questions', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Questions', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Questions:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Questions found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Questions found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'questions' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' )
	);

	register_post_type( 'questions', $args );
}

// Register Custom Taxonomy
function custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Question Categories', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Question Category', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Question Categories', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'question_category', array( 'questions' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );

/*Add class to each li in menu Section*/

add_filter ( 'nav_menu_css_class', 'custom_quiz_menu_item_class', 10, 2 );

function custom_quiz_menu_item_class ( $classes, $item){
  $classes[] = 'm_nav_item';
  return $classes;
}

/*Upload svg Images in Media*/

function add_svg_to_upload_mimes( $upload_mimes ) {
	$upload_mimes['svg'] = 'image/svg+xml';
	$upload_mimes['svgz'] = 'image/svg+xml';
	return $upload_mimes;
}
add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );

/*Remove publish metabox in Custom post 'Quiz Question'*/

function quiz_questions_remove_publish_box() {
    remove_meta_box( 'submitdiv', 'quiz_questions', 'side' );
}
add_action( 'admin_menu', 'quiz_questions_remove_publish_box' );
?>