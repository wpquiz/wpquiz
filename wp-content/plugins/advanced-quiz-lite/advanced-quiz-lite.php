<?php
defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );
/**
 * Plugin Name: Advanced Quiz Lite Form
 * Plugin URI: http://iammanikandan.com
 * Description: Custom Quiz form for students in the university/managament websites.
 * Version: 1.0
 * Author: Deeraj
 * Author URI: http://iammanikandan.com
 * License:     GPL2 etc
 * 
 */
 /* Copyright YEAR PLUGIN_AUTHOR_NAME (email : your email address)
(Plugin Name) is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
(Plugin Name) is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with (Plugin Name). If not, see (http://link to your plugin license).
*/
 
 if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
class QuizPlugin {
	
 public function __construct() {
	 
	// Add Javascript and CSS for admin screens
        add_action('admin_enqueue_scripts', array($this,'enqueueAdmin'));

        // Add Javascript and CSS for front-end display
        add_action('wp_enqueue_scripts', array($this,'enqueue'));
 
    }
	
	public function enqueueAdmin() {
    	// Actual enqueues, note the files are in the js and css folders
    	// For scripts, make sure you are including the relevant dependencies (jquery in this case)
    	wp_enqueue_script('adqlf_custom_js', plugins_url('js/adqlf_custom.js', __FILE__), array('jquery'), '1.0', true);
    	wp_enqueue_style('adqlf_custom_css', plugins_url('css/adqlf_custom.css', __FILE__), null, '1.0');
    }
	 public function demoquizmethod(){
		add_action('admin_menu', 'customMenu');
		function customMenu()
		{
			//add_menu_page('Fact Of The Day', 'Fact Of The Day', 'manage_categories', 'factoftheday', 'displayManagementPage');
			add_menu_page('Quiz', 'Quiz', 'manage_options', 'quiz', 'displayQuizPage', '',26);
		}
		/**
 * Display a custom menu page
 */
function displayQuizPage(){ 

$questionCategorys = get_terms( array(
    'taxonomy' => 'question_category',
    'hide_empty' => false,
	'parent' => 0
) );
//echo "<pre>",print_r($questionCategorys);
//foreach($questionCategorys as $questionCategory){
	
//echo "====".$countChildren;
//}

?>
<script type="text/javascript">

   function changeFunc() {
    var questionCategory = document.getElementById("questionCategory");
    var selectedValue = questionCategory.options[questionCategory.selectedIndex].value;
    alert(selectedValue);
   }

  </script>

<h2>Add Quiz Test for Each Category</h2>
            <form name="quizForm" method="post" action="">
                <?php wp_nonce_field('quiz-nonce'); ?>
                <table class="editform form-table" width="100%" cellspacing="2" cellpadding="5">
                    <tr class="form-field term-description-wrap">
                        <th scope="row" valign="top">
                            <label for="quiz">WP Quiz Generation:</label>
                        </th>
                    </tr>
                    <tr class="form-field">
                        <th scope="row" valign="top">
                            <label for="author">Select Question Category:</label>
                        </th>
                        <td>
							<?php
							// create dropdown for Category
							echo'<select name="question-categorys" id="questionCategory" onclick="changeFunc();">'; 
							foreach($questionCategorys as $questionCategory)
							{
									echo'<option value="'.$questionCategory->name.'">'.$questionCategory->name.'</option>'; 
							}    
							echo'</select>';
							?>
                        </td>
                    </tr>
                    <tr>
						<th scope="row" valign="top">
                            <label for="author">Select No.of Questions needed from Each Category:</label>
                        </th>
                    </tr>
                            <?php
							// create dropdown for Category
							foreach($questionCategorys as $questionCategory)
							{
								$questionCategoryschild = get_terms( array(
								'taxonomy' => 'question_category',
								'hide_empty' => false,
								'parent' => $questionCategory->term_id));
								//$countChildren = count (get_term_children( $questionCategory->term_id, 'question_category' ));
								//echo "<pre>",print_r($questionCategory);
								foreach($questionCategoryschild as $questionCategorychild)
							{
								?>
								<tr>
								<td>
								<label><?php echo $questionCategorychild->name;?></label><br>
									<input type="text" name="<?php echo 'count-'.$questionCategorychild->name;?>" size="10"/><br>
									<label><?php echo $questionCategorychild->count;?></label><br>
								</td>
								</tr>
								<?php
							}?>
							<?php
							}
							?>
							<tr>
							 <th scope="row" valign="top"><input type="hidden" name="addQuiz" value="1"/></th>
								<td>
									<input type="submit" name="submit" class="button button-primary" value="Add Quiz Test"/>
								</td>
							</tr>
                    
                </table>
            </form>
            <br/><br/>
<?php 
}
	 }
}

//global $quiz_plugin;
// Create an instance of our class to kick off the whole thing
$quiz_plugin = new QuizPlugin();
$quiz_plugin->demoquizmethod();

add_action('admin_init', 'actionProcess');
function actionProcess()
{
    $action = (isset($_GET['action']) ? $_GET['action'] : null);
    $addQuiz = (isset($_POST['addQuiz']) ? $_POST['addQuiz'] : null);


    if ($addQuiz == 1 && check_admin_referer('quiz-nonce')) {
		echo"<pre>",print_r($_POST);exit;
        //addQuiz(trim($_POST['question-categorys']), $_POST['author']);
    }
}