<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class CGRVC_Document {
    
	public static function form(){
		
		if(isset($_POST) && !empty($_POST)) {
			if($_POST['document_submit']) {
				
				if ( ! function_exists( 'wp_handle_upload' ) ) 
					require_once( ABSPATH . 'wp-admin/includes/file.php' );
				
				$title = $_POST['document_title'];
				$description = $_POST['document_description'];
				$category = $_POST['document_category'];
				$tags = $_POST['document_tag'];
				$current_user = wp_get_current_user();
				$image = self::checkImageFile($_FILES['document_image']);
				$pdf = self::checkPDFFile($_FILES['document_pdf']);
				
				if($pdf == 'valid') {
					
					$post_array = array (
						'comment_status' => 'closed',
						'ping_status' => 'closed',
						'post_title' => $title,
						'post_content' => $description,
						'post_author' => $current_user->ID,
						'post_status' => 'publish',
						'post_type' => 'post',
						'post_category' => array($category),
						'tags_input' => $tags
					);
					
					$insert_post = wp_insert_post($post_array);
					
					if($insert_post > 0) { 
						
						if(isset($_FILES['document_image']) && !empty($_FILES['document_image']['name']) && $_FILES['document_image']['error'] == 0 ) {
							require_once(ABSPATH . "wp-admin" . '/includes/image.php');
							require_once(ABSPATH . "wp-admin" . '/includes/file.php');
							require_once(ABSPATH . "wp-admin" . '/includes/media.php');

							$attachment_id = media_handle_upload('document_image', $insert_post);

							update_post_meta($insert_post, '_thumbnail_id', $attachment_id);

							$attachment_data = array(
							'ID' => $attachment_id
							);

							wp_update_post($attachment_data);
						}
						
						if(isset($_FILES['document_pdf']) && !empty($_FILES['document_pdf']['name']) && $_FILES['document_pdf']['error'] == 0) {
							require_once(ABSPATH . "wp-admin" . '/includes/image.php');
							require_once(ABSPATH . "wp-admin" . '/includes/file.php');
							require_once(ABSPATH . "wp-admin" . '/includes/media.php');

							$attachment_id = media_handle_upload('document_pdf', $insert_post);

							update_post_meta($insert_post, '_document_pdf_upload', $attachment_id);
							update_post_meta($insert_post, '__document_pdf_upload', 'field_583ef6c417d99');

						}
						
						//wp_set_object_terms($insert_post , $tags,'post_tag',true);
						
						echo '<div class="alert alert-success alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							Article Successfully Created.
						</div>';
					
					}
				} else {
					if($image != 'valid') { 
						echo '<div class="alert alert-danger alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Image : </strong>'.$image.'
						</div>';
					} 
					 if($pdf != 'valid') {
						echo '<div class="alert alert-danger alert-dismissible">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>PDF : </strong>'.$pdf.'
						</div>';
					}
				}
				
			}
		}
		$args = array(
			'child_of' => 0,
			'parent' => '',
			'orderby' => 'name',
			'order' => 'ASC',
			'hide_empty' => 0,
			'taxonomy' => 'category',
			'echo' => 0
		);
		$categories = get_categories($args);
		
		$tag_args = array(
			'hide_empty' => 0,
		);
		
		$tags = get_tags($tag_args);
		
		include_once( 'templates/document_form.php' );
	}
	
	public static function checkImageFile($document){
		
		$filetype = wp_check_filetype($document['name']);
		$upload_image_type = array('png','jpg','jpeg','gif');
		
		$msg = '';
		
		if(!empty($document['name']) && $document['name'] == '') {
			
			$msg = "Please Upload your image";
			
		} else if($document['error'] > 0) {
			
			$msg = "Invalid uploaded file";
			
		}else if(!in_array($filetype['ext'], $upload_image_type)) {
		
				$msg = "Invalid uploaded file type";
				
		} else if($document['size'] > '1572864' ) {
			$msg = "Invalid uploaded file size";
			
		} else {
			$msg = 'valid';
		}
		
		return $msg;
	} 
	
	public static function checkPDFFile($document){
		
		$filetype = wp_check_filetype($document['name']);

		$msg = '';
		
		if(!empty($document['name']) && $document['name'] == '') {
			
			$msg = "Please Upload your ". $type;
			
		} else if($document['error'] > 0) {
			
			$msg = "Invalid uploaded file";
			
		} else if($filetype['ext'] != 'pdf') {

			$msg = "Invalid uploaded file type";
				
		}  else {
			
			$msg = 'valid';
			
		}
		
		return $msg;
	}
}
