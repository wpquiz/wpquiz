<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class CGRVC_Shortcodes {
	
	public static function init(){
		add_shortcode('CGRVC_document_form', array('CGRVC_Document', 'form'));
		add_shortcode('CGRVC_event_form', array('CGRVC_Event', 'form'));
	}
}
