	<form class="form-horizontal" name="document_form" id="document_form" role="form" method="POST" enctype="multipart/form-data">
		<h2>Research document upload form</h2>
        <div class="form-group">
			<label for="document_title" class="col-sm-3 control-label">Title</label>
			<div class="col-sm-9">
				<input type="text" id="document_title" name="document_title" placeholder="Title" class="form-control">
				<span class="document-title-error err-msg">* Enter Document Title</span>
			</div>
        </div>
		<div class="form-group research-document-description">
			<label for="document_description" class="col-sm-3 control-label">Description</label>
			<div class="col-sm-9">
				<?php 
					$settings = array(
						'wpautop' => true,
						'media_buttons' => false,
						'textarea_name' => 'document_description',
						'editor_class' => 'document_description',
						'editor_height' => 250,
						'tinymce' => array(
							'theme_advanced_buttons1' => 'bold,italic,underline,blockquote,|,undo,redo,|,fullscreen',
							'theme_advanced_buttons2' => '',
							'theme_advanced_buttons3' => '',
							'theme_advanced_buttons4' => ''
						)
					);
				wp_editor('', 'document_description', $settings );?>
				<span class="document-description-error err-msg">* Enter Document Description</span>
			</div>
		</div>
		<div class="form-group">
			<label for="document_category" class="col-sm-3 control-label">Category</label>
			<div class="col-sm-9">
				<select id="document_category" name="document_category" class="form-control">
					<option value="0">Choose a category</option>
					<?php foreach($categories as $category) { 
						echo '<option value="'.$category->term_id.'">'.$category->name.'</option>';
					} ?>
				</select>
				<span class="document-category-error err-msg">* Select Document Category</span>
			</div>
		 </div> 
		 <div class="form-group">
			<label for="document_tag" class="col-sm-3 control-label">Tags</label>
			<div class="col-sm-9">
				<!--<select id="document_tag" name="document_tag[]" class="form-control" multiple>
					<?php /*foreach($tags as $tags) { 
						echo '<option value="'.$tags->name.'">'.$tags->name.'</option>';
					} */?>
				</select>-->
				<input type="text" id="document_tag" name="document_tag" placeholder="Tag" class="form-control">
				<span class="help-block">Please follow following instruction to add a tag (For Example : Tag1 , Tag2 ,Tag3 .... Tagn)</span>
				<span class="document-tag-error err-msg">* Enter Tag's associated to the Post</span>
			</div>
		 </div> 
		 <div class="form-group">
			<label for="document_image" class="col-sm-3 control-label">Image</label>
			<div class="col-sm-9">
				<input type="file" id="document_image" name="document_image" class="form-control">
				<span class="help-block">Please upload your document image here</span>
				<span class="document-image-error err-msg">* Upload  Document Image(max-size 2mb)</span>
			</div>
			
		 </div>
		 <div class="form-group">
			<label for="document_pdf" class="col-sm-3 control-label">PDF</label>
			<div class="col-sm-9">
				<input type="file" id="document_pdf" name="document_pdf" class="form-control">
				<span class="help-block">Please upload your PDF document here</span>
				<span class="document-pdf-error err-msg">* Upload  Document PDF (max-size 2mb)</span>
			</div>
			
		 </div>
		<div class="form-group">
			<div class="col-sm-12">
				<input type="hidden" name="action" value="new_article_update">
				<input type="submit" class="submit btn btn-primary btn-block document_submit" name="document_submit" id="document_submit" value="Submit">
			</div>
		</div>
	</form> 

