-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2017 at 05:11 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wpquiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-09-24 10:05:24', '2017-09-24 10:05:24', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_custom_options_plus`
--

CREATE TABLE `wp_custom_options_plus` (
  `id` int(5) NOT NULL,
  `label` varchar(100) NOT NULL,
  `name` varchar(80) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_custom_options_plus`
--

INSERT INTO `wp_custom_options_plus` (`id`, `label`, `name`, `value`) VALUES
(1, 'Home Our Blog Title', 'home_our_blog_title', 'Our Blog');

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/wpquiz', 'yes'),
(2, 'home', 'http://localhost/wpquiz', 'yes'),
(3, 'blogname', 'WpQuiz', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'deeraj16@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:121:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:42:\"quiz_questions/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:52:\"quiz_questions/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:72:\"quiz_questions/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"quiz_questions/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"quiz_questions/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:48:\"quiz_questions/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:31:\"quiz_questions/([^/]+)/embed/?$\";s:47:\"index.php?quiz_questions=$matches[1]&embed=true\";s:35:\"quiz_questions/([^/]+)/trackback/?$\";s:41:\"index.php?quiz_questions=$matches[1]&tb=1\";s:43:\"quiz_questions/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?quiz_questions=$matches[1]&paged=$matches[2]\";s:50:\"quiz_questions/([^/]+)/comment-page-([0-9]{1,})/?$\";s:54:\"index.php?quiz_questions=$matches[1]&cpage=$matches[2]\";s:39:\"quiz_questions/([^/]+)(?:/([0-9]+))?/?$\";s:53:\"index.php?quiz_questions=$matches[1]&page=$matches[2]\";s:31:\"quiz_questions/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\"quiz_questions/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\"quiz_questions/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"quiz_questions/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"quiz_questions/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\"quiz_questions/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:40:\"wck-meta-box/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"wck-meta-box/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"wck-meta-box/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"wck-meta-box/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"wck-meta-box/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"wck-meta-box/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:29:\"wck-meta-box/([^/]+)/embed/?$\";s:45:\"index.php?wck-meta-box=$matches[1]&embed=true\";s:33:\"wck-meta-box/([^/]+)/trackback/?$\";s:39:\"index.php?wck-meta-box=$matches[1]&tb=1\";s:41:\"wck-meta-box/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?wck-meta-box=$matches[1]&paged=$matches[2]\";s:48:\"wck-meta-box/([^/]+)/comment-page-([0-9]{1,})/?$\";s:52:\"index.php?wck-meta-box=$matches[1]&cpage=$matches[2]\";s:37:\"wck-meta-box/([^/]+)(?:/([0-9]+))?/?$\";s:51:\"index.php?wck-meta-box=$matches[1]&page=$matches[2]\";s:29:\"wck-meta-box/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:39:\"wck-meta-box/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:59:\"wck-meta-box/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"wck-meta-box/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"wck-meta-box/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:35:\"wck-meta-box/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=45&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:43:\"custom-options-plus/custom-options-plus.php\";i:3;s:55:\"wck-custom-fields-and-custom-post-types-creator/wck.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'twentyseventeen', 'yes'),
(41, 'stylesheet', 'twentyseventeen-child', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '45', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:5:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'cron', 'a:5:{i:1506679525;a:2:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1506679526;a:1:{s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1506679588;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1506701297;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(109, 'nonce_key', '6;h%a5W!!uK:E2`t=1W]x#(rK#z`QCaa`;aiW`YP^a2C~QsS#?%]zqM/d6;aq^yN', 'no'),
(110, 'nonce_salt', '}EVa{=:JT7XA]_ `j0CebSCnlCl%b8V>q}o:/jqD;N<lmTqUul&+h-ZJg+ uAY(w', 'no'),
(111, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1506262461;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(115, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.8.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.8.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.8.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.8.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.8.2\";s:7:\"version\";s:5:\"4.8.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1506649324;s:15:\"version_checked\";s:5:\"4.8.2\";s:12:\"translations\";a:0:{}}', 'no'),
(120, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1506649326;s:7:\"checked\";a:2:{s:21:\"twentyseventeen-child\";s:5:\"1.0.0\";s:15:\"twentyseventeen\";s:3:\"1.3\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(121, 'auth_key', 'Ca_%[~j6KA6rcRRuSzNnA/ +0O1j:uISP S@+qoN >VI1b#(_3_,T<yI2SM4GX@E', 'no'),
(122, 'auth_salt', 'Z2VAR@{M>)b!h!_jbG*~p`L~h+tcrVg9U3!yfG`}{^o-,I=%Uik1[qj3)>0>`R*]', 'no'),
(123, 'logged_in_key', 'c8(%QgKs`vNqeqr<O)DS$>z;e(@%+XWW/7Q=(RX<#P/g_TF^|xIf~z8T/rl&UCnj', 'no'),
(124, 'logged_in_salt', '~7jT<wUEi_dTazU1A+;q.y_$G)nET~~[u]`PbU^q.VN|i ;}OIY3MxC%FwP*! z7', 'no'),
(126, '_site_transient_timeout_browser_130a06df177c97a2e2b12b5a17719ce1', '1506852345', 'no'),
(127, '_site_transient_browser_130a06df177c97a2e2b12b5a17719ce1', 'a:9:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:4:\"56.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:24:\"https://www.firefox.com/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"16\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(128, 'can_compress_scripts', '1', 'no'),
(153, '_transient_timeout_plugin_slugs', '1506396024', 'no'),
(154, '_transient_plugin_slugs', 'a:4:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:43:\"custom-options-plus/custom-options-plus.php\";i:3;s:55:\"wck-custom-fields-and-custom-post-types-creator/wck.php\";}', 'no'),
(155, 'recently_activated', 'a:0:{}', 'yes'),
(156, 'acf_version', '4.4.12', 'yes'),
(157, 'wpcf7', 'a:2:{s:7:\"version\";s:3:\"4.9\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1506262411;s:7:\"version\";s:3:\"4.9\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(158, 'cop_version', '1.8.0', 'yes'),
(159, 'wck_meta_boxes_ids', 'a:0:{}', 'yes'),
(160, 'wck_update_to_unserialized', 'no', 'yes'),
(163, 'current_theme', 'Twenty Seventeen Child', 'yes'),
(164, 'theme_mods_twentyseventeen-child', 'a:3:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:1:{s:3:\"top\";i:2;}}', 'yes'),
(165, 'theme_switched', '', 'yes'),
(170, '_site_transient_timeout_available_translations', '1506280499', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(171, '_site_transient_available_translations', 'a:109:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-04 12:02:13\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-29 08:49:40\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.6\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.7.6/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-30 18:40:55\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.8.2/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-09 09:24:45\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:22:\"Продължение\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-04 16:58:43\";s:12:\"english_name\";s:7:\"Bengali\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-05 09:44:12\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-04 20:20:28\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-08 21:01:45\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 08:46:26\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:12:\"Čeština‎\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-19 16:27:32\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-23 01:58:28\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Forts&#230;t\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-24 11:46:57\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-09 11:53:31\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:72:\"http://downloads.wordpress.org/translation/core/4.8.2/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-04 15:29:24\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:70:\"http://downloads.wordpress.org/translation/core/4.8.2/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-09 11:51:58\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 10:51:51\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-25 10:03:08\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-25 17:31:04\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-02 03:57:05\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-25 19:47:01\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"4.7.6\";s:7:\"updated\";s:19:\"2017-01-26 15:53:43\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.6/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-26 10:38:53\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-30 16:09:17\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/es_VE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-01 04:48:11\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/es_AR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-31 15:12:02\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/es_MX.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"4.7.6\";s:7:\"updated\";s:19:\"2017-01-26 15:54:37\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.6/es_GT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-28 20:09:49\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/es_CL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-18 14:39:36\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/es_ES.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"es\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-13 17:00:30\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/es_CO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 16:37:11\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-19 12:08:05\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-09 15:50:45\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.8/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 11:00:29\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 12:37:07\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-20 10:02:42\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 09:14:18\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-26 12:45:35\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-31 06:54:10\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-12 21:37:24\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"להמשיך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 10:29:26\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-22 14:47:25\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:48:39\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Tovább\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 12:45:08\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:5:\"4.7.6\";s:7:\"updated\";s:19:\"2017-04-13 13:55:54\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.6/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-10 18:53:47\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-03 23:23:50\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-12 09:20:11\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.8/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-22 15:33:00\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.8.2/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-07 02:07:59\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 10:48:16\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:48:25\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.7.2/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"ຕໍ່\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 11:02:15\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"4.7.6\";s:7:\"updated\";s:19:\"2017-03-17 20:40:40\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.6/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"4.7.6\";s:7:\"updated\";s:19:\"2017-01-26 15:54:41\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.6/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-05 19:40:47\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:58:\"http://downloads.wordpress.org/translation/core/4.8/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.7.6\";s:7:\"updated\";s:19:\"2017-03-05 09:45:10\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.6/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.19\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:64:\"http://downloads.wordpress.org/translation/core/4.1.19/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ေဆာင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-02 21:02:39\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-31 08:47:10\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"जारीराख्नु \";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-30 07:58:32\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:70:\"http://downloads.wordpress.org/translation/core/4.8.2/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-27 16:44:39\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-17 11:00:54\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-01 07:32:10\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.8.2/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-20 08:02:58\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.19\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.1.19/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"دوام\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-26 21:35:20\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-05 18:31:50\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:68:\"http://downloads.wordpress.org/translation/core/4.8.2/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-10 19:12:13\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-19 13:05:58\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-23 11:17:01\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-09 13:26:18\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-12 12:51:50\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Nadaljujte\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"4.7.6\";s:7:\"updated\";s:19:\"2017-04-24 08:35:30\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.6/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 12:07:44\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-19 08:58:31\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:48:43\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-31 11:38:12\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:61:\"http://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-05 09:23:39\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:9:\"Uyƣurqə\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.7.2/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-19 19:56:39\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-07-28 14:27:29\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-11 16:54:43\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-13 04:19:14\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:60:\"http://downloads.wordpress.org/translation/core/4.8.2/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-12 11:35:05\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-09-23 09:44:04\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"4.8.2\";s:7:\"updated\";s:19:\"2017-08-04 07:53:05\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:63:\"http://downloads.wordpress.org/translation/core/4.8.2/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}', 'no'),
(178, '_site_transient_timeout_browser_78781494ed3df6d95ed4cc377276b386', '1506914253', 'no'),
(179, '_site_transient_browser_78781494ed3df6d95ed4cc377276b386', 'a:9:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:4:\"55.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:24:\"https://www.firefox.com/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"16\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(192, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(194, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1506649325;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"4.4.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip\";}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:3:\"4.9\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-7.4.9.zip\";}s:43:\"custom-options-plus/custom-options-plus.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:33:\"w.org/plugins/custom-options-plus\";s:4:\"slug\";s:19:\"custom-options-plus\";s:6:\"plugin\";s:43:\"custom-options-plus/custom-options-plus.php\";s:11:\"new_version\";s:5:\"1.8.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-options-plus/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/custom-options-plus.zip\";}s:55:\"wck-custom-fields-and-custom-post-types-creator/wck.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:61:\"w.org/plugins/wck-custom-fields-and-custom-post-types-creator\";s:4:\"slug\";s:47:\"wck-custom-fields-and-custom-post-types-creator\";s:6:\"plugin\";s:55:\"wck-custom-fields-and-custom-post-types-creator/wck.php\";s:11:\"new_version\";s:5:\"2.1.2\";s:3:\"url\";s:78:\"https://wordpress.org/plugins/wck-custom-fields-and-custom-post-types-creator/\";s:7:\"package\";s:96:\"https://downloads.wordpress.org/plugin/wck-custom-fields-and-custom-post-types-creator.2.1.2.zip\";}}}', 'no'),
(201, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1506328546', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(202, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4377;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2506;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2376;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:2312;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1834;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1604;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1593;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1440;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1357;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1353;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1345;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1279;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1273;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1138;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1059;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1050;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:998;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:946;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:818;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:815;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:813;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:774;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:772;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:670;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:669;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:664;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:655;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:647;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:645;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:638;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:625;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:611;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:596;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:594;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:589;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:584;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:577;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:576;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:561;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:558;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:543;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:535;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:525;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:519;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:505;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:502;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:496;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:488;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:477;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:475;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:474;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:470;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:450;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:448;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:442;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:440;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:440;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:435;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:422;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:417;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:410;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:407;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:405;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:405;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:402;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:396;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:386;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:383;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:381;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:370;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:353;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:344;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:340;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:332;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:332;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:331;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:330;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:330;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:327;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:326;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:323;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:321;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:321;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:314;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:303;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:300;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:300;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:293;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:293;}s:3:\"tag\";a:3:{s:4:\"name\";s:3:\"tag\";s:4:\"slug\";s:3:\"tag\";s:5:\"count\";i:292;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:287;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:286;}s:7:\"adsense\";a:3:{s:4:\"name\";s:7:\"adsense\";s:4:\"slug\";s:7:\"adsense\";s:5:\"count\";i:284;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:283;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:279;}s:8:\"lightbox\";a:3:{s:4:\"name\";s:8:\"lightbox\";s:4:\"slug\";s:8:\"lightbox\";s:5:\"count\";i:278;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:274;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:274;}s:7:\"tinymce\";a:3:{s:4:\"name\";s:7:\"tinyMCE\";s:4:\"slug\";s:7:\"tinymce\";s:5:\"count\";i:273;}s:7:\"captcha\";a:3:{s:4:\"name\";s:7:\"captcha\";s:4:\"slug\";s:7:\"captcha\";s:5:\"count\";i:271;}}', 'no'),
(214, 'wck_cptc', 'a:1:{i:0;a:39:{s:9:\"post-type\";s:14:\"quiz_questions\";s:11:\"description\";s:0:\"\";s:14:\"singular-label\";s:13:\"Quiz Question\";s:12:\"plural-label\";s:14:\"Quiz Questions\";s:12:\"hierarchical\";s:5:\"false\";s:11:\"has-archive\";s:5:\"false\";s:8:\"supports\";s:5:\"title\";s:7:\"add-new\";s:0:\"\";s:12:\"add-new-item\";s:0:\"\";s:9:\"edit-item\";s:0:\"\";s:8:\"new-item\";s:0:\"\";s:9:\"all-items\";s:0:\"\";s:10:\"view-items\";s:0:\"\";s:12:\"search-items\";s:0:\"\";s:9:\"not-found\";s:0:\"\";s:18:\"not-found-in-trash\";s:0:\"\";s:17:\"parent-item-colon\";s:0:\"\";s:9:\"menu-name\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:7:\"show-ui\";s:4:\"true\";s:17:\"show-in-nav-menus\";s:4:\"true\";s:12:\"show-in-menu\";s:4:\"true\";s:13:\"menu-position\";s:0:\"\";s:9:\"menu-icon\";s:0:\"\";s:15:\"capability-type\";s:4:\"post\";s:10:\"taxonomies\";s:8:\"category\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite-slug\";s:0:\"\";s:12:\"show-in-rest\";s:5:\"false\";}}', 'yes'),
(219, 'category_children', 'a:0:{}', 'yes'),
(222, '_site_transient_timeout_theme_roots', '1506651126', 'no'),
(223, '_site_transient_theme_roots', 'a:2:{s:21:\"twentyseventeen-child\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";}', 'no'),
(224, '_transient_timeout_dash_v2_88ae138922fe95674369b1cb3d215a2b', '1506692923', 'no'),
(225, '_transient_dash_v2_88ae138922fe95674369b1cb3d215a2b', '<div class=\"rss-widget\"><p><strong>RSS Error:</strong> WP HTTP Error: cURL error 6: Could not resolve host: wordpress.org</p></div><div class=\"rss-widget\"><p><strong>RSS Error:</strong> WP HTTP Error: cURL error 6: Could not resolve host: planet.wordpress.org</p></div>', 'no'),
(232, '_transient_is_multi_author', '0', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_form', '<span class=\"input input--chisato\">\n	[text* your-name class:input__field class:input__field--chisato]\n<label class=\"input__label input__label--chisato\" for=\"input-13\">\n	<span class=\"input__label-content input__label-content--chisato\" data-content=\"Name\">Name</span>\n</label>\n</span>\n\n<span class=\"input input--chisato\">\n	[email* your-email class:input__field class:input__field--chisato]\n<label class=\"input__label input__label--chisato\" for=\"input-14\">\n	<span class=\"input__label-content input__label-content--chisato\" data-content=\"Email\">Email</span>\n</label>\n</span>\n\n<span class=\"input input--chisato\">\n	[text* your-subject class:input__field class:input__field--chisato]\n<label class=\"input__label input__label--chisato\" for=\"input-15\">\n	<span class=\"input__label-content input__label-content--chisato\" data-content=\"Subject\">Subject</span>\n</label>\n</span>\n\n[textarea* your-message placeholder \"Your comment here...\"]\n[submit \"Submit\"]'),
(3, 4, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:23:\"WpQuiz \"[your-subject]\"\";s:6:\"sender\";s:32:\"[your-name] <deeraj16@gmail.com>\";s:9:\"recipient\";s:18:\"deeraj16@gmail.com\";s:4:\"body\";s:168:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on WpQuiz (http://localhost/wpquiz)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(4, 4, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:23:\"WpQuiz \"[your-subject]\"\";s:6:\"sender\";s:27:\"WpQuiz <deeraj16@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:110:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on WpQuiz (http://localhost/wpquiz)\";s:18:\"additional_headers\";s:28:\"Reply-To: deeraj16@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(5, 4, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(6, 4, '_additional_settings', ''),
(7, 4, '_locale', 'en_US'),
(8, 5, '_wp_attached_file', '2017/09/g6.jpg'),
(9, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:620;s:6:\"height\";i:400;s:4:\"file\";s:14:\"2017/09/g6.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g6-300x194.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:194;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g6-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(10, 6, '_wp_attached_file', '2017/09/g9.jpg'),
(11, 6, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:520;s:4:\"file\";s:14:\"2017/09/g9.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g9-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g9-231x300.jpg\";s:5:\"width\";i:231;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g9-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(12, 7, '_wp_attached_file', '2017/09/move-top.png'),
(13, 7, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:32;s:6:\"height\";i:32;s:4:\"file\";s:20:\"2017/09/move-top.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(14, 8, '_wp_attached_file', '2017/09/s1.jpg'),
(15, 8, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:220;s:4:\"file\";s:14:\"2017/09/s1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"s1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(16, 9, '_wp_attached_file', '2017/09/s2.jpg'),
(17, 9, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:220;s:4:\"file\";s:14:\"2017/09/s2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"s2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(18, 10, '_wp_attached_file', '2017/09/s3.jpg'),
(19, 10, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:220;s:4:\"file\";s:14:\"2017/09/s3.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"s3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(20, 11, '_wp_attached_file', '2017/09/s4.jpg'),
(21, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:220;s:4:\"file\";s:14:\"2017/09/s4.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"s4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"9\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:12:\"Canon EOS 5D\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:2:\"32\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:4:\"0.01\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(22, 12, '_wp_attached_file', '2017/09/score-board.png'),
(23, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:183;s:6:\"height\";i:175;s:4:\"file\";s:23:\"2017/09/score-board.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"score-board-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:23:\"score-board-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(24, 13, '_wp_attached_file', '2017/09/wallhaven-3178.jpg'),
(25, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1680;s:6:\"height\";i:850;s:4:\"file\";s:26:\"2017/09/wallhaven-3178.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"wallhaven-3178-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"wallhaven-3178-300x152.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:152;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"wallhaven-3178-768x389.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:389;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"wallhaven-3178-1024x518.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:518;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:26:\"wallhaven-3178-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(26, 14, '_wp_attached_file', '2017/09/wallhaven-3178-thumb.jpg'),
(27, 14, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:112;s:4:\"file\";s:32:\"2017/09/wallhaven-3178-thumb.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"wallhaven-3178-thumb-150x112.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:112;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:32:\"wallhaven-3178-thumb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(28, 15, '_wp_attached_file', '2017/09/wallhaven-10742.jpg'),
(29, 15, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:850;s:4:\"file\";s:27:\"2017/09/wallhaven-10742.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"wallhaven-10742-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"wallhaven-10742-300x128.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:128;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"wallhaven-10742-768x326.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:326;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"wallhaven-10742-1024x435.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:435;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:28:\"wallhaven-10742-2000x850.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:850;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:27:\"wallhaven-10742-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(30, 16, '_wp_attached_file', '2017/09/wallhaven-10742-thumb.jpg'),
(31, 16, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:112;s:4:\"file\";s:33:\"2017/09/wallhaven-10742-thumb.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"wallhaven-10742-thumb-150x112.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:112;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:33:\"wallhaven-10742-thumb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(32, 17, '_wp_attached_file', '2017/09/wallhaven-12018.jpg'),
(33, 17, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1680;s:6:\"height\";i:850;s:4:\"file\";s:27:\"2017/09/wallhaven-12018.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"wallhaven-12018-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"wallhaven-12018-300x152.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:152;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"wallhaven-12018-768x389.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:389;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"wallhaven-12018-1024x518.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:518;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:27:\"wallhaven-12018-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(34, 18, '_wp_attached_file', '2017/09/wallhaven-12018-thumbnail.jpg'),
(35, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:112;s:4:\"file\";s:37:\"2017/09/wallhaven-12018-thumbnail.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"wallhaven-12018-thumbnail-150x112.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:112;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:37:\"wallhaven-12018-thumbnail-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"8\";s:6:\"credit\";s:32:\"Łukasz Popardowski | lukasz@pop\";s:6:\"camera\";s:22:\"Canon EOS 400D DIGITAL\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1456082322\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:2:\"40\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:4:\"0.25\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(36, 19, '_wp_attached_file', '2017/09/wallhaven-16270.jpg'),
(37, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:850;s:4:\"file\";s:27:\"2017/09/wallhaven-16270.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"wallhaven-16270-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"wallhaven-16270-300x128.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:128;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"wallhaven-16270-768x326.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:326;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"wallhaven-16270-1024x435.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:435;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:28:\"wallhaven-16270-2000x850.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:850;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:27:\"wallhaven-16270-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(38, 20, '_wp_attached_file', '2017/09/wallhaven-16270-thumbnail.jpg'),
(39, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:112;s:4:\"file\";s:37:\"2017/09/wallhaven-16270-thumbnail.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"wallhaven-16270-thumbnail-150x112.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:112;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:37:\"wallhaven-16270-thumbnail-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(40, 21, '_wp_attached_file', '2017/09/wallhaven-27263.jpg'),
(41, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1680;s:6:\"height\";i:850;s:4:\"file\";s:27:\"2017/09/wallhaven-27263.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"wallhaven-27263-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"wallhaven-27263-300x152.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:152;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"wallhaven-27263-768x389.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:389;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"wallhaven-27263-1024x518.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:518;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:27:\"wallhaven-27263-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(42, 22, '_wp_attached_file', '2017/09/wallhaven-27263-thumbnail.jpg'),
(43, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:112;s:4:\"file\";s:37:\"2017/09/wallhaven-27263-thumbnail.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"wallhaven-27263-thumbnail-150x112.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:112;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:37:\"wallhaven-27263-thumbnail-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"5.6\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:19:\"Canon EOS REBEL T3i\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1425044364\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:2:\"20\";s:3:\"iso\";s:3:\"125\";s:13:\"shutter_speed\";s:4:\"0.04\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(44, 23, '_wp_attached_file', '2017/09/ab.jpg'),
(45, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:620;s:6:\"height\";i:460;s:4:\"file\";s:14:\"2017/09/ab.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"ab-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"ab-300x223.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:223;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"ab-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(46, 24, '_wp_attached_file', '2017/09/ab1.jpg'),
(47, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:620;s:6:\"height\";i:400;s:4:\"file\";s:15:\"2017/09/ab1.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"ab1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"ab1-300x194.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:194;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:15:\"ab1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(48, 25, '_wp_attached_file', '2017/09/arrows.png'),
(49, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:76;s:6:\"height\";i:53;s:4:\"file\";s:18:\"2017/09/arrows.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(50, 26, '_wp_attached_file', '2017/09/contact-bg.jpg'),
(51, 26, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1680;s:6:\"height\";i:500;s:4:\"file\";s:22:\"2017/09/contact-bg.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"contact-bg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"contact-bg-300x89.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:89;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"contact-bg-768x229.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:229;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"contact-bg-1024x305.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:22:\"contact-bg-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(52, 27, '_wp_attached_file', '2017/09/exam.jpg'),
(53, 27, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:5264;s:6:\"height\";i:3860;s:4:\"file\";s:16:\"2017/09/exam.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"exam-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"exam-300x220.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"exam-768x563.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:563;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"exam-1024x751.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:751;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:18:\"exam-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:16:\"exam-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(54, 28, '_wp_attached_file', '2017/09/g1.jpg'),
(55, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:520;s:4:\"file\";s:14:\"2017/09/g1.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g1-231x300.jpg\";s:5:\"width\";i:231;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(56, 29, '_wp_attached_file', '2017/09/g2.jpg'),
(57, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:620;s:6:\"height\";i:400;s:4:\"file\";s:14:\"2017/09/g2.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g2-300x194.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:194;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(58, 30, '_wp_attached_file', '2017/09/g3.jpg'),
(59, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:620;s:6:\"height\";i:400;s:4:\"file\";s:14:\"2017/09/g3.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g3-300x194.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:194;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(60, 31, '_wp_attached_file', '2017/09/g4.jpg'),
(61, 31, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:620;s:6:\"height\";i:400;s:4:\"file\";s:14:\"2017/09/g4.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g4-300x194.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:194;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(62, 32, '_wp_attached_file', '2017/09/g5.jpg'),
(63, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:520;s:4:\"file\";s:14:\"2017/09/g5.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g5-231x300.jpg\";s:5:\"width\";i:231;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g5-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(64, 33, '_wp_attached_file', '2017/09/01.jpg'),
(65, 33, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2316;s:6:\"height\";i:1542;s:4:\"file\";s:14:\"2017/09/01.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"01-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"01-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"01-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"01-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"01-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"01-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(66, 34, '_wp_attached_file', '2017/09/02.jpg'),
(67, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2316;s:6:\"height\";i:1542;s:4:\"file\";s:14:\"2017/09/02.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"02-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"02-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"02-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"02-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"02-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"02-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(68, 35, '_wp_attached_file', '2017/09/03.jpg'),
(69, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2316;s:6:\"height\";i:1542;s:4:\"file\";s:14:\"2017/09/03.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"03-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"03-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"03-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"03-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"03-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"03-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(70, 36, '_wp_attached_file', '2017/09/04.jpg'),
(71, 36, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2316;s:6:\"height\";i:1542;s:4:\"file\";s:14:\"2017/09/04.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"04-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"04-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"04-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"04-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"04-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"04-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(72, 37, '_wp_attached_file', '2017/09/05.jpg'),
(73, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2316;s:6:\"height\";i:1542;s:4:\"file\";s:14:\"2017/09/05.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"05-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"05-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"05-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"05-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"05-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"05-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(74, 38, '_wp_attached_file', '2017/09/06.jpg'),
(75, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2316;s:6:\"height\";i:1542;s:4:\"file\";s:14:\"2017/09/06.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"06-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"06-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"06-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"06-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"06-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"06-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(76, 39, '_wp_attached_file', '2017/09/07.jpg'),
(77, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2316;s:6:\"height\";i:1542;s:4:\"file\";s:14:\"2017/09/07.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"07-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"07-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"07-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"07-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"07-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"07-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(78, 40, '_wp_attached_file', '2017/09/08.jpg'),
(79, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2316;s:6:\"height\";i:1542;s:4:\"file\";s:14:\"2017/09/08.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"08-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"08-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"08-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"08-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"08-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"08-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(80, 41, '_wp_attached_file', '2017/09/09.jpg'),
(81, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2316;s:6:\"height\";i:1542;s:4:\"file\";s:14:\"2017/09/09.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"09-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"09-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"09-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"09-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"09-2000x1200.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"09-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(82, 45, '_edit_last', '1'),
(83, 45, '_edit_lock', '1506488896:1'),
(84, 45, '_wp_page_template', 'default'),
(85, 48, '_edit_last', '1'),
(86, 48, '_wp_page_template', 'default'),
(87, 48, '_edit_lock', '1506651446:1'),
(88, 49, '_edit_last', '1'),
(89, 49, '_edit_lock', '1506654414:1'),
(90, 49, '_wp_page_template', 'default'),
(91, 50, '_edit_last', '1'),
(92, 50, '_wp_page_template', 'default'),
(93, 50, '_edit_lock', '1506310026:1'),
(94, 51, '_edit_last', '1'),
(95, 51, '_wp_page_template', 'default'),
(96, 51, '_edit_lock', '1506310027:1'),
(97, 52, '_edit_last', '1'),
(98, 52, '_wp_page_template', 'default'),
(99, 52, '_edit_lock', '1506310031:1'),
(100, 53, '_edit_last', '1'),
(101, 53, '_wp_page_template', 'contact-template.php'),
(102, 53, '_edit_lock', '1506653611:1'),
(103, 60, '_menu_item_type', 'post_type'),
(104, 60, '_menu_item_menu_item_parent', '0'),
(105, 60, '_menu_item_object_id', '53'),
(106, 60, '_menu_item_object', 'page'),
(107, 60, '_menu_item_target', ''),
(108, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(109, 60, '_menu_item_xfn', ''),
(110, 60, '_menu_item_url', ''),
(112, 61, '_menu_item_type', 'post_type'),
(113, 61, '_menu_item_menu_item_parent', '0'),
(114, 61, '_menu_item_object_id', '52'),
(115, 61, '_menu_item_object', 'page'),
(116, 61, '_menu_item_target', ''),
(117, 61, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(118, 61, '_menu_item_xfn', ''),
(119, 61, '_menu_item_url', ''),
(121, 62, '_menu_item_type', 'post_type'),
(122, 62, '_menu_item_menu_item_parent', '0'),
(123, 62, '_menu_item_object_id', '51'),
(124, 62, '_menu_item_object', 'page'),
(125, 62, '_menu_item_target', ''),
(126, 62, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(127, 62, '_menu_item_xfn', ''),
(128, 62, '_menu_item_url', ''),
(130, 63, '_menu_item_type', 'post_type'),
(131, 63, '_menu_item_menu_item_parent', '0'),
(132, 63, '_menu_item_object_id', '50'),
(133, 63, '_menu_item_object', 'page'),
(134, 63, '_menu_item_target', ''),
(135, 63, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(136, 63, '_menu_item_xfn', ''),
(137, 63, '_menu_item_url', ''),
(139, 64, '_menu_item_type', 'post_type'),
(140, 64, '_menu_item_menu_item_parent', '0'),
(141, 64, '_menu_item_object_id', '49'),
(142, 64, '_menu_item_object', 'page'),
(143, 64, '_menu_item_target', ''),
(144, 64, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(145, 64, '_menu_item_xfn', ''),
(146, 64, '_menu_item_url', ''),
(148, 65, '_menu_item_type', 'post_type'),
(149, 65, '_menu_item_menu_item_parent', '0'),
(150, 65, '_menu_item_object_id', '48'),
(151, 65, '_menu_item_object', 'page'),
(152, 65, '_menu_item_target', ''),
(153, 65, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(154, 65, '_menu_item_xfn', ''),
(155, 65, '_menu_item_url', ''),
(157, 66, '_menu_item_type', 'post_type'),
(158, 66, '_menu_item_menu_item_parent', '0'),
(159, 66, '_menu_item_object_id', '45'),
(160, 66, '_menu_item_object', 'page'),
(161, 66, '_menu_item_target', ''),
(162, 66, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(163, 66, '_menu_item_xfn', ''),
(164, 66, '_menu_item_url', ''),
(166, 67, '_edit_last', '1'),
(168, 67, 'position', 'normal'),
(169, 67, 'layout', 'no_box'),
(170, 67, 'hide_on_screen', ''),
(171, 67, '_edit_lock', '1506321804:1'),
(172, 67, 'field_59c881289ec0a', 'a:8:{s:3:\"key\";s:19:\"field_59c881289ec0a\";s:5:\"label\";s:6:\"Slider\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(173, 67, 'field_59c881489ec0b', 'a:14:{s:3:\"key\";s:19:\"field_59c881489ec0b\";s:5:\"label\";s:15:\"Slide Content 1\";s:4:\"name\";s:15:\"slide_content_1\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(174, 67, 'field_59c881879ec0c', 'a:14:{s:3:\"key\";s:19:\"field_59c881879ec0c\";s:5:\"label\";s:15:\"Slide Content 2\";s:4:\"name\";s:15:\"slide_content_2\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(175, 67, 'field_59c8818f9ec0d', 'a:14:{s:3:\"key\";s:19:\"field_59c8818f9ec0d\";s:5:\"label\";s:15:\"Slide Content 3\";s:4:\"name\";s:15:\"slide_content_3\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(176, 67, 'field_59c881a49ec0e', 'a:14:{s:3:\"key\";s:19:\"field_59c881a49ec0e\";s:5:\"label\";s:15:\"Slide Content 4\";s:4:\"name\";s:15:\"slide_content_4\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(177, 67, 'field_59c881ab9ec0f', 'a:14:{s:3:\"key\";s:19:\"field_59c881ab9ec0f\";s:5:\"label\";s:15:\"Slide Content 5\";s:4:\"name\";s:15:\"slide_content_5\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}'),
(179, 68, 'slide_content_1', 'instead of building walls we can help build bridges...'),
(180, 68, '_slide_content_1', 'field_59c881489ec0b'),
(181, 68, 'slide_content_2', 'if you don\'t give up you still have a chance giving up is the greatest failure...'),
(182, 68, '_slide_content_2', 'field_59c881879ec0c'),
(183, 68, 'slide_content_3', 'Imagine with all your mind believe...'),
(184, 68, '_slide_content_3', 'field_59c8818f9ec0d'),
(185, 68, 'slide_content_4', 'Work hard dream big...'),
(186, 68, '_slide_content_4', 'field_59c881a49ec0e'),
(187, 68, 'slide_content_5', 'Your Time is limited...'),
(188, 68, '_slide_content_5', 'field_59c881ab9ec0f'),
(189, 45, 'slide_content_1', 'instead of building walls we can help build bridges...'),
(190, 45, '_slide_content_1', 'field_59c881489ec0b'),
(191, 45, 'slide_content_2', 'if you don\'t give up you still have a chance giving up is the greatest failure...'),
(192, 45, '_slide_content_2', 'field_59c881879ec0c'),
(193, 45, 'slide_content_3', 'Imagine with all your mind believe...'),
(194, 45, '_slide_content_3', 'field_59c8818f9ec0d'),
(195, 45, 'slide_content_4', 'Work hard dream big...'),
(196, 45, '_slide_content_4', 'field_59c881a49ec0e'),
(197, 45, 'slide_content_5', 'Your Time is limited...'),
(198, 45, '_slide_content_5', 'field_59c881ab9ec0f'),
(201, 67, 'field_59c8834b95fa5', 'a:8:{s:3:\"key\";s:19:\"field_59c8834b95fa5\";s:5:\"label\";s:8:\"About Us\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}'),
(202, 67, 'field_59c8836295fa6', 'a:14:{s:3:\"key\";s:19:\"field_59c8836295fa6\";s:5:\"label\";s:14:\"About Us Title\";s:4:\"name\";s:14:\"about_us_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}'),
(203, 67, 'field_59c8836e95fa7', 'a:14:{s:3:\"key\";s:19:\"field_59c8836e95fa7\";s:5:\"label\";s:21:\"About Us Caption Text\";s:4:\"name\";s:21:\"about_us_caption_text\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:8;}'),
(204, 67, 'field_59c8838595fa8', 'a:11:{s:3:\"key\";s:19:\"field_59c8838595fa8\";s:5:\"label\";s:16:\"About Us Content\";s:4:\"name\";s:16:\"about_us_content\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:9;}'),
(206, 69, 'slide_content_1', 'instead of building walls we can help build bridges...'),
(207, 69, '_slide_content_1', 'field_59c881489ec0b'),
(208, 69, 'slide_content_2', 'if you don\'t give up you still have a chance giving up is the greatest failure...'),
(209, 69, '_slide_content_2', 'field_59c881879ec0c'),
(210, 69, 'slide_content_3', 'Imagine with all your mind believe...'),
(211, 69, '_slide_content_3', 'field_59c8818f9ec0d'),
(212, 69, 'slide_content_4', 'Work hard dream big...'),
(213, 69, '_slide_content_4', 'field_59c881a49ec0e'),
(214, 69, 'slide_content_5', 'Your Time is limited...'),
(215, 69, '_slide_content_5', 'field_59c881ab9ec0f'),
(216, 69, 'about_us_title', 'About Us'),
(217, 69, '_about_us_title', 'field_59c8836295fa6'),
(218, 69, 'about_us_caption_text', 'Practice & Get Placed Dream job through knowledge season'),
(219, 69, '_about_us_caption_text', 'field_59c8836e95fa7'),
(220, 69, 'about_us_content', '1. Our motto is If you can dream it, you can do it.\r\n\r\n2. This knowledge season created for the main purpose is every students can do find the\r\nour future dream job work flow before complete your graduation.'),
(221, 69, '_about_us_content', 'field_59c8838595fa8'),
(222, 45, 'about_us_title', 'About Us'),
(223, 45, '_about_us_title', 'field_59c8836295fa6'),
(224, 45, 'about_us_caption_text', 'Practice & Get Placed Dream job through knowledge season'),
(225, 45, '_about_us_caption_text', 'field_59c8836e95fa7'),
(226, 45, 'about_us_content', '1. Our motto is If you can dream it, you can do it.\r\n\r\n2. This knowledge season created for the main purpose is every students can do find the\r\nour future dream job work flow before complete your graduation.'),
(227, 45, '_about_us_content', 'field_59c8838595fa8'),
(228, 67, 'field_59c8857a6d4cb', 'a:8:{s:3:\"key\";s:19:\"field_59c8857a6d4cb\";s:5:\"label\";s:4:\"Exam\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:10;}'),
(229, 67, 'field_59c885986d4cc', 'a:11:{s:3:\"key\";s:19:\"field_59c885986d4cc\";s:5:\"label\";s:17:\"ONLINE EXAM Image\";s:4:\"name\";s:17:\"online_exam_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:11;}'),
(230, 67, 'field_59c885b26d4cd', 'a:14:{s:3:\"key\";s:19:\"field_59c885b26d4cd\";s:5:\"label\";s:17:\"ONLINE EXAM Title\";s:4:\"name\";s:17:\"online_exam_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:12;}'),
(231, 67, 'field_59c885c56d4ce', 'a:11:{s:3:\"key\";s:19:\"field_59c885c56d4ce\";s:5:\"label\";s:19:\"ONLINE EXAM Content\";s:4:\"name\";s:19:\"online_exam_content\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:13;}'),
(233, 70, '_wp_attached_file', '2017/09/ab-1.jpg'),
(234, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:620;s:6:\"height\";i:460;s:4:\"file\";s:16:\"2017/09/ab-1.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"ab-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"ab-1-300x223.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:223;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:16:\"ab-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(236, 71, 'slide_content_1', 'instead of building walls we can help build bridges...'),
(237, 71, '_slide_content_1', 'field_59c881489ec0b'),
(238, 71, 'slide_content_2', 'if you don\'t give up you still have a chance giving up is the greatest failure...'),
(239, 71, '_slide_content_2', 'field_59c881879ec0c'),
(240, 71, 'slide_content_3', 'Imagine with all your mind believe...'),
(241, 71, '_slide_content_3', 'field_59c8818f9ec0d'),
(242, 71, 'slide_content_4', 'Work hard dream big...'),
(243, 71, '_slide_content_4', 'field_59c881a49ec0e'),
(244, 71, 'slide_content_5', 'Your Time is limited...'),
(245, 71, '_slide_content_5', 'field_59c881ab9ec0f'),
(246, 71, 'about_us_title', 'About Us'),
(247, 71, '_about_us_title', 'field_59c8836295fa6'),
(248, 71, 'about_us_caption_text', 'Practice & Get Placed Dream job through knowledge season'),
(249, 71, '_about_us_caption_text', 'field_59c8836e95fa7'),
(250, 71, 'about_us_content', '1. Our motto is If you can dream it, you can do it.\r\n\r\n2. This knowledge season created for the main purpose is every students can do find the\r\nour future dream job work flow before complete your graduation.'),
(251, 71, '_about_us_content', 'field_59c8838595fa8'),
(252, 71, 'online_exam_image', '23'),
(253, 71, '_online_exam_image', 'field_59c885986d4cc'),
(254, 71, 'online_exam_title', 'Welcome To Online Exam'),
(255, 71, '_online_exam_title', 'field_59c885b26d4cd'),
(256, 71, 'online_exam_content', ' <p>Prepare and get placed in the easiest way possible.</p>                         <p>Its really easy to get your knowledge score and you can find your area of interset.</p>'),
(257, 71, '_online_exam_content', 'field_59c885c56d4ce'),
(258, 45, 'online_exam_image', '23'),
(259, 45, '_online_exam_image', 'field_59c885986d4cc'),
(260, 45, 'online_exam_title', 'Welcome To Online Exam'),
(261, 45, '_online_exam_title', 'field_59c885b26d4cd'),
(262, 45, 'online_exam_content', 'Prepare and get placed in the easiest way possible.\r\n\r\nIts really easy to get your knowledge score and you can find your area of interset.'),
(263, 45, '_online_exam_content', 'field_59c885c56d4ce'),
(264, 67, 'field_59c887d980fc5', 'a:14:{s:3:\"key\";s:19:\"field_59c887d980fc5\";s:5:\"label\";s:16:\"Exam Steps Title\";s:4:\"name\";s:16:\"exam_steps_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:14;}'),
(265, 67, 'field_59c887ea80fc6', 'a:14:{s:3:\"key\";s:19:\"field_59c887ea80fc6\";s:5:\"label\";s:22:\"Exam Question Number 1\";s:4:\"name\";s:22:\"exam_question_number_1\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:15;}'),
(266, 67, 'field_59c887f080fc7', 'a:14:{s:3:\"key\";s:19:\"field_59c887f080fc7\";s:5:\"label\";s:28:\"Exam Question Number 1 Title\";s:4:\"name\";s:28:\"exam_question_number_1_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:16;}'),
(267, 67, 'field_59c887fb80fc8', 'a:14:{s:3:\"key\";s:19:\"field_59c887fb80fc8\";s:5:\"label\";s:22:\"Exam Question Number 2\";s:4:\"name\";s:22:\"exam_question_number_2\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:17;}'),
(268, 67, 'field_59c8880480fc9', 'a:14:{s:3:\"key\";s:19:\"field_59c8880480fc9\";s:5:\"label\";s:28:\"Exam Question Number 2 Title\";s:4:\"name\";s:28:\"exam_question_number_2_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:18;}'),
(269, 67, 'field_59c8880e80fca', 'a:14:{s:3:\"key\";s:19:\"field_59c8880e80fca\";s:5:\"label\";s:22:\"Exam Question Number 3\";s:4:\"name\";s:22:\"exam_question_number_3\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:19;}'),
(270, 67, 'field_59c8881580fcb', 'a:14:{s:3:\"key\";s:19:\"field_59c8881580fcb\";s:5:\"label\";s:28:\"Exam Question Number 3 Title\";s:4:\"name\";s:28:\"exam_question_number_3_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:20;}'),
(272, 72, 'slide_content_1', 'instead of building walls we can help build bridges...'),
(273, 72, '_slide_content_1', 'field_59c881489ec0b'),
(274, 72, 'slide_content_2', 'if you don\'t give up you still have a chance giving up is the greatest failure...'),
(275, 72, '_slide_content_2', 'field_59c881879ec0c'),
(276, 72, 'slide_content_3', 'Imagine with all your mind believe...'),
(277, 72, '_slide_content_3', 'field_59c8818f9ec0d'),
(278, 72, 'slide_content_4', 'Work hard dream big...'),
(279, 72, '_slide_content_4', 'field_59c881a49ec0e'),
(280, 72, 'slide_content_5', 'Your Time is limited...'),
(281, 72, '_slide_content_5', 'field_59c881ab9ec0f'),
(282, 72, 'about_us_title', 'About Us'),
(283, 72, '_about_us_title', 'field_59c8836295fa6'),
(284, 72, 'about_us_caption_text', 'Practice & Get Placed Dream job through knowledge season'),
(285, 72, '_about_us_caption_text', 'field_59c8836e95fa7'),
(286, 72, 'about_us_content', '1. Our motto is If you can dream it, you can do it.\r\n\r\n2. This knowledge season created for the main purpose is every students can do find the\r\nour future dream job work flow before complete your graduation.'),
(287, 72, '_about_us_content', 'field_59c8838595fa8'),
(288, 72, 'online_exam_image', '23'),
(289, 72, '_online_exam_image', 'field_59c885986d4cc'),
(290, 72, 'online_exam_title', 'Welcome To Online Exam'),
(291, 72, '_online_exam_title', 'field_59c885b26d4cd'),
(292, 72, 'online_exam_content', 'Prepare and get placed in the easiest way possible.\r\n\r\nIts really easy to get your knowledge score and you can find your area of interset.'),
(293, 72, '_online_exam_content', 'field_59c885c56d4ce'),
(294, 72, 'exam_steps_title', 'Follow test For Easy Steps'),
(295, 72, '_exam_steps_title', 'field_59c887d980fc5'),
(296, 72, 'exam_question_number_1', '01'),
(297, 72, '_exam_question_number_1', 'field_59c887ea80fc6'),
(298, 72, 'exam_question_number_1_title', 'Login'),
(299, 72, '_exam_question_number_1_title', 'field_59c887f080fc7'),
(300, 72, 'exam_question_number_2', '02'),
(301, 72, '_exam_question_number_2', 'field_59c887fb80fc8'),
(302, 72, 'exam_question_number_2_title', 'Choose categories'),
(303, 72, '_exam_question_number_2_title', 'field_59c8880480fc9'),
(304, 72, 'exam_question_number_3', '03'),
(305, 72, '_exam_question_number_3', 'field_59c8880e80fca'),
(306, 72, 'exam_question_number_3_title', 'Take test'),
(307, 72, '_exam_question_number_3_title', 'field_59c8881580fcb'),
(308, 45, 'exam_steps_title', 'Follow test For Easy Steps'),
(309, 45, '_exam_steps_title', 'field_59c887d980fc5'),
(310, 45, 'exam_question_number_1', '01'),
(311, 45, '_exam_question_number_1', 'field_59c887ea80fc6'),
(312, 45, 'exam_question_number_1_title', 'Login'),
(313, 45, '_exam_question_number_1_title', 'field_59c887f080fc7'),
(314, 45, 'exam_question_number_2', '02'),
(315, 45, '_exam_question_number_2', 'field_59c887fb80fc8'),
(316, 45, 'exam_question_number_2_title', 'Choose categories'),
(317, 45, '_exam_question_number_2_title', 'field_59c8880480fc9'),
(318, 45, 'exam_question_number_3', '03'),
(319, 45, '_exam_question_number_3', 'field_59c8880e80fca'),
(320, 45, 'exam_question_number_3_title', 'Take test'),
(321, 45, '_exam_question_number_3_title', 'field_59c8881580fcb'),
(322, 67, 'field_59c8895ee774a', 'a:8:{s:3:\"key\";s:19:\"field_59c8895ee774a\";s:5:\"label\";s:4:\"INFO\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:21;}'),
(323, 67, 'field_59c889a4e774b', 'a:14:{s:3:\"key\";s:19:\"field_59c889a4e774b\";s:5:\"label\";s:7:\"Title 1\";s:4:\"name\";s:7:\"title_1\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:22;}'),
(324, 67, 'field_59c889d3e774c', 'a:14:{s:3:\"key\";s:19:\"field_59c889d3e774c\";s:5:\"label\";s:13:\"Title 1 Count\";s:4:\"name\";s:13:\"title_1_count\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:23;}'),
(325, 67, 'field_59c889ede774d', 'a:14:{s:3:\"key\";s:19:\"field_59c889ede774d\";s:5:\"label\";s:7:\"Title 2\";s:4:\"name\";s:7:\"title_2\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:24;}'),
(326, 67, 'field_59c889f4e774e', 'a:14:{s:3:\"key\";s:19:\"field_59c889f4e774e\";s:5:\"label\";s:13:\"Title 2 Count\";s:4:\"name\";s:13:\"title_2_count\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:25;}'),
(327, 67, 'field_59c889fbe774f', 'a:14:{s:3:\"key\";s:19:\"field_59c889fbe774f\";s:5:\"label\";s:7:\"Title 3\";s:4:\"name\";s:7:\"title_3\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:26;}'),
(328, 67, 'field_59c88a03e7750', 'a:14:{s:3:\"key\";s:19:\"field_59c88a03e7750\";s:5:\"label\";s:13:\"Title 3 Count\";s:4:\"name\";s:13:\"title_3_count\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:27;}'),
(329, 67, 'field_59c88a0ae7751', 'a:14:{s:3:\"key\";s:19:\"field_59c88a0ae7751\";s:5:\"label\";s:7:\"Title 4\";s:4:\"name\";s:7:\"title_4\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:28;}'),
(330, 67, 'field_59c88a24e7752', 'a:14:{s:3:\"key\";s:19:\"field_59c88a24e7752\";s:5:\"label\";s:13:\"Title 4 Count\";s:4:\"name\";s:13:\"title_4_count\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:29;}'),
(332, 73, 'slide_content_1', 'instead of building walls we can help build bridges...'),
(333, 73, '_slide_content_1', 'field_59c881489ec0b'),
(334, 73, 'slide_content_2', 'if you don\'t give up you still have a chance giving up is the greatest failure...'),
(335, 73, '_slide_content_2', 'field_59c881879ec0c'),
(336, 73, 'slide_content_3', 'Imagine with all your mind believe...'),
(337, 73, '_slide_content_3', 'field_59c8818f9ec0d'),
(338, 73, 'slide_content_4', 'Work hard dream big...'),
(339, 73, '_slide_content_4', 'field_59c881a49ec0e'),
(340, 73, 'slide_content_5', 'Your Time is limited...'),
(341, 73, '_slide_content_5', 'field_59c881ab9ec0f'),
(342, 73, 'about_us_title', 'About Us'),
(343, 73, '_about_us_title', 'field_59c8836295fa6'),
(344, 73, 'about_us_caption_text', 'Practice & Get Placed Dream job through knowledge season'),
(345, 73, '_about_us_caption_text', 'field_59c8836e95fa7'),
(346, 73, 'about_us_content', '1. Our motto is If you can dream it, you can do it.\r\n\r\n2. This knowledge season created for the main purpose is every students can do find the\r\nour future dream job work flow before complete your graduation.'),
(347, 73, '_about_us_content', 'field_59c8838595fa8'),
(348, 73, 'online_exam_image', '23'),
(349, 73, '_online_exam_image', 'field_59c885986d4cc'),
(350, 73, 'online_exam_title', 'Welcome To Online Exam'),
(351, 73, '_online_exam_title', 'field_59c885b26d4cd'),
(352, 73, 'online_exam_content', 'Prepare and get placed in the easiest way possible.\r\n\r\nIts really easy to get your knowledge score and you can find your area of interset.'),
(353, 73, '_online_exam_content', 'field_59c885c56d4ce'),
(354, 73, 'exam_steps_title', 'Follow test For Easy Steps'),
(355, 73, '_exam_steps_title', 'field_59c887d980fc5'),
(356, 73, 'exam_question_number_1', '01'),
(357, 73, '_exam_question_number_1', 'field_59c887ea80fc6'),
(358, 73, 'exam_question_number_1_title', 'Login'),
(359, 73, '_exam_question_number_1_title', 'field_59c887f080fc7'),
(360, 73, 'exam_question_number_2', '02'),
(361, 73, '_exam_question_number_2', 'field_59c887fb80fc8'),
(362, 73, 'exam_question_number_2_title', 'Choose categories'),
(363, 73, '_exam_question_number_2_title', 'field_59c8880480fc9'),
(364, 73, 'exam_question_number_3', '03'),
(365, 73, '_exam_question_number_3', 'field_59c8880e80fca'),
(366, 73, 'exam_question_number_3_title', 'Take test'),
(367, 73, '_exam_question_number_3_title', 'field_59c8881580fcb'),
(368, 73, 'title_1', 'Collages'),
(369, 73, '_title_1', 'field_59c889a4e774b'),
(370, 73, 'title_1_count', '45'),
(371, 73, '_title_1_count', 'field_59c889d3e774c'),
(372, 73, 'title_2', 'companies'),
(373, 73, '_title_2', 'field_59c889ede774d'),
(374, 73, 'title_2_count', '165'),
(375, 73, '_title_2_count', 'field_59c889f4e774e'),
(376, 73, 'title_3', 'Students'),
(377, 73, '_title_3', 'field_59c889fbe774f'),
(378, 73, 'title_3_count', '563'),
(379, 73, '_title_3_count', 'field_59c88a03e7750'),
(380, 73, 'title_4', 'Placements'),
(381, 73, '_title_4', 'field_59c88a0ae7751'),
(382, 73, 'title_4_count', '245'),
(383, 73, '_title_4_count', 'field_59c88a24e7752'),
(384, 45, 'title_1', 'Collages'),
(385, 45, '_title_1', 'field_59c889a4e774b'),
(386, 45, 'title_1_count', '45'),
(387, 45, '_title_1_count', 'field_59c889d3e774c'),
(388, 45, 'title_2', 'companies'),
(389, 45, '_title_2', 'field_59c889ede774d'),
(390, 45, 'title_2_count', '165'),
(391, 45, '_title_2_count', 'field_59c889f4e774e'),
(392, 45, 'title_3', 'Students'),
(393, 45, '_title_3', 'field_59c889fbe774f'),
(394, 45, 'title_3_count', '563'),
(395, 45, '_title_3_count', 'field_59c88a03e7750'),
(396, 45, 'title_4', 'Placements'),
(397, 45, '_title_4', 'field_59c88a0ae7751'),
(398, 45, 'title_4_count', '245'),
(399, 45, '_title_4_count', 'field_59c88a24e7752'),
(400, 67, 'field_59c88eec92921', 'a:8:{s:3:\"key\";s:19:\"field_59c88eec92921\";s:5:\"label\";s:14:\"Q/A Categories\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:30;}'),
(401, 67, 'field_59c88f1592922', 'a:14:{s:3:\"key\";s:19:\"field_59c88f1592922\";s:5:\"label\";s:20:\"Q/A Categories Title\";s:4:\"name\";s:20:\"q/a_categories_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:31;}'),
(402, 67, 'field_59c88f3f92923', 'a:11:{s:3:\"key\";s:19:\"field_59c88f3f92923\";s:5:\"label\";s:22:\"Q/A Categories 1 image\";s:4:\"name\";s:22:\"q/a_categories_1_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:32;}'),
(403, 67, 'field_59c88f5392924', 'a:11:{s:3:\"key\";s:19:\"field_59c88f5392924\";s:5:\"label\";s:21:\"Q/A Categories 1 Icon\";s:4:\"name\";s:21:\"q/a_categories_1_icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:33;}'),
(404, 67, 'field_59c88f6392925', 'a:14:{s:3:\"key\";s:19:\"field_59c88f6392925\";s:5:\"label\";s:24:\"Q/A Categories 1 Content\";s:4:\"name\";s:24:\"q/a_categories_1_content\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:34;}'),
(405, 67, 'field_59c88f8092926', 'a:11:{s:3:\"key\";s:19:\"field_59c88f8092926\";s:5:\"label\";s:22:\"Q/A Categories 2 Image\";s:4:\"name\";s:22:\"q/a_categories_2_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:35;}'),
(406, 67, 'field_59c88f9b92927', 'a:11:{s:3:\"key\";s:19:\"field_59c88f9b92927\";s:5:\"label\";s:21:\"Q/A Categories 2 Icon\";s:4:\"name\";s:21:\"q/a_categories_2_icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:36;}'),
(407, 67, 'field_59c88fa992928', 'a:14:{s:3:\"key\";s:19:\"field_59c88fa992928\";s:5:\"label\";s:24:\"Q/A Categories 2 Content\";s:4:\"name\";s:24:\"q/a_categories_2_content\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:37;}'),
(408, 67, 'field_59c88fbd92929', 'a:11:{s:3:\"key\";s:19:\"field_59c88fbd92929\";s:5:\"label\";s:22:\"Q/A Categories 3 Image\";s:4:\"name\";s:22:\"q/a_categories_3_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:38;}'),
(409, 67, 'field_59c88ff89292a', 'a:11:{s:3:\"key\";s:19:\"field_59c88ff89292a\";s:5:\"label\";s:21:\"Q/A Categories 3 Icon\";s:4:\"name\";s:21:\"q/a_categories_3_icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:39;}'),
(410, 67, 'field_59c8900b9292b', 'a:14:{s:3:\"key\";s:19:\"field_59c8900b9292b\";s:5:\"label\";s:24:\"Q/A Categories 3 Content\";s:4:\"name\";s:24:\"q/a_categories_3_content\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:40;}'),
(411, 67, 'field_59c890189292c', 'a:11:{s:3:\"key\";s:19:\"field_59c890189292c\";s:5:\"label\";s:22:\"Q/A Categories 4 Image\";s:4:\"name\";s:22:\"q/a_categories_4_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:41;}'),
(412, 67, 'field_59c890269292d', 'a:11:{s:3:\"key\";s:19:\"field_59c890269292d\";s:5:\"label\";s:21:\"Q/A Categories 4 icon\";s:4:\"name\";s:21:\"q/a_categories_4_icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:42;}'),
(413, 67, 'field_59c8903b9292e', 'a:14:{s:3:\"key\";s:19:\"field_59c8903b9292e\";s:5:\"label\";s:24:\"Q/A Categories 4 Content\";s:4:\"name\";s:24:\"q/a_categories_4_content\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:43;}'),
(414, 67, 'field_59c8905f9292f', 'a:11:{s:3:\"key\";s:19:\"field_59c8905f9292f\";s:5:\"label\";s:22:\"Q/A Categories 5 Image\";s:4:\"name\";s:22:\"q/a_categories_5_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:44;}'),
(415, 67, 'field_59c8906f92930', 'a:11:{s:3:\"key\";s:19:\"field_59c8906f92930\";s:5:\"label\";s:21:\"Q/A Categories 5 Icon\";s:4:\"name\";s:21:\"q/a_categories_5_icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:45;}'),
(416, 67, 'field_59c8908592931', 'a:14:{s:3:\"key\";s:19:\"field_59c8908592931\";s:5:\"label\";s:24:\"Q/A Categories 5 Content\";s:4:\"name\";s:24:\"q/a_categories_5_content\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:46;}'),
(417, 67, 'field_59c8908e92932', 'a:11:{s:3:\"key\";s:19:\"field_59c8908e92932\";s:5:\"label\";s:22:\"Q/A Categories 6 Image\";s:4:\"name\";s:22:\"q/a_categories_6_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:47;}'),
(418, 67, 'field_59c8909f92933', 'a:11:{s:3:\"key\";s:19:\"field_59c8909f92933\";s:5:\"label\";s:21:\"Q/A Categories 6 Icon\";s:4:\"name\";s:21:\"q/a_categories_6_icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:48;}'),
(419, 67, 'field_59c890f192934', 'a:14:{s:3:\"key\";s:19:\"field_59c890f192934\";s:5:\"label\";s:24:\"Q/A Categories 6 Content\";s:4:\"name\";s:24:\"q/a_categories_6_content\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:49;}'),
(420, 67, 'field_59c8911392935', 'a:11:{s:3:\"key\";s:19:\"field_59c8911392935\";s:5:\"label\";s:22:\"Q/A Categories 7 Image\";s:4:\"name\";s:22:\"q/a_categories_7_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:50;}'),
(421, 67, 'field_59c8915492936', 'a:11:{s:3:\"key\";s:19:\"field_59c8915492936\";s:5:\"label\";s:21:\"Q/A Categories 7 Icon\";s:4:\"name\";s:21:\"q/a_categories_7_icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:51;}'),
(422, 67, 'field_59c8919c92937', 'a:14:{s:3:\"key\";s:19:\"field_59c8919c92937\";s:5:\"label\";s:22:\"Q/A Categories Content\";s:4:\"name\";s:22:\"q/a_categories_content\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:52;}'),
(423, 67, 'field_59c891b592938', 'a:11:{s:3:\"key\";s:19:\"field_59c891b592938\";s:5:\"label\";s:22:\"Q/A Categories 8 Image\";s:4:\"name\";s:22:\"q/a_categories_8_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:53;}'),
(424, 67, 'field_59c8925a92939', 'a:11:{s:3:\"key\";s:19:\"field_59c8925a92939\";s:5:\"label\";s:21:\"Q/A Categories 8 Icon\";s:4:\"name\";s:21:\"q/a_categories_8_icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:54;}'),
(425, 67, 'field_59c8926b9293a', 'a:14:{s:3:\"key\";s:19:\"field_59c8926b9293a\";s:5:\"label\";s:24:\"Q/A Categories 8 Content\";s:4:\"name\";s:24:\"q/a_categories_8_content\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:55;}'),
(426, 67, 'field_59c8927d9293b', 'a:11:{s:3:\"key\";s:19:\"field_59c8927d9293b\";s:5:\"label\";s:22:\"Q/A Categories 9 Image\";s:4:\"name\";s:22:\"q/a_categories_9_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:56;}'),
(427, 67, 'field_59c892ec9293c', 'a:11:{s:3:\"key\";s:19:\"field_59c892ec9293c\";s:5:\"label\";s:21:\"Q/A Categories 9 Icon\";s:4:\"name\";s:21:\"q/a_categories_9_icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:57;}'),
(428, 67, 'field_59c893129293d', 'a:14:{s:3:\"key\";s:19:\"field_59c893129293d\";s:5:\"label\";s:24:\"Q/A Categories 9 Content\";s:4:\"name\";s:24:\"q/a_categories_9_content\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:58;}'),
(431, 74, 'slide_content_1', 'instead of building walls we can help build bridges...'),
(432, 74, '_slide_content_1', 'field_59c881489ec0b'),
(433, 74, 'slide_content_2', 'if you don\'t give up you still have a chance giving up is the greatest failure...'),
(434, 74, '_slide_content_2', 'field_59c881879ec0c'),
(435, 74, 'slide_content_3', 'Imagine with all your mind believe...'),
(436, 74, '_slide_content_3', 'field_59c8818f9ec0d'),
(437, 74, 'slide_content_4', 'Work hard dream big...'),
(438, 74, '_slide_content_4', 'field_59c881a49ec0e'),
(439, 74, 'slide_content_5', 'Your Time is limited...'),
(440, 74, '_slide_content_5', 'field_59c881ab9ec0f'),
(441, 74, 'about_us_title', 'About Us'),
(442, 74, '_about_us_title', 'field_59c8836295fa6'),
(443, 74, 'about_us_caption_text', 'Practice & Get Placed Dream job through knowledge season'),
(444, 74, '_about_us_caption_text', 'field_59c8836e95fa7'),
(445, 74, 'about_us_content', '1. Our motto is If you can dream it, you can do it.\r\n\r\n2. This knowledge season created for the main purpose is every students can do find the\r\nour future dream job work flow before complete your graduation.'),
(446, 74, '_about_us_content', 'field_59c8838595fa8'),
(447, 74, 'online_exam_image', '23'),
(448, 74, '_online_exam_image', 'field_59c885986d4cc'),
(449, 74, 'online_exam_title', 'Welcome To Online Exam'),
(450, 74, '_online_exam_title', 'field_59c885b26d4cd'),
(451, 74, 'online_exam_content', 'Prepare and get placed in the easiest way possible.\r\n\r\nIts really easy to get your knowledge score and you can find your area of interset.'),
(452, 74, '_online_exam_content', 'field_59c885c56d4ce'),
(453, 74, 'exam_steps_title', 'Follow test For Easy Steps'),
(454, 74, '_exam_steps_title', 'field_59c887d980fc5'),
(455, 74, 'exam_question_number_1', '01'),
(456, 74, '_exam_question_number_1', 'field_59c887ea80fc6'),
(457, 74, 'exam_question_number_1_title', 'Login'),
(458, 74, '_exam_question_number_1_title', 'field_59c887f080fc7'),
(459, 74, 'exam_question_number_2', '02'),
(460, 74, '_exam_question_number_2', 'field_59c887fb80fc8'),
(461, 74, 'exam_question_number_2_title', 'Choose categories'),
(462, 74, '_exam_question_number_2_title', 'field_59c8880480fc9');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(463, 74, 'exam_question_number_3', '03'),
(464, 74, '_exam_question_number_3', 'field_59c8880e80fca'),
(465, 74, 'exam_question_number_3_title', 'Take test'),
(466, 74, '_exam_question_number_3_title', 'field_59c8881580fcb'),
(467, 74, 'title_1', 'Collages'),
(468, 74, '_title_1', 'field_59c889a4e774b'),
(469, 74, 'title_1_count', '45'),
(470, 74, '_title_1_count', 'field_59c889d3e774c'),
(471, 74, 'title_2', 'companies'),
(472, 74, '_title_2', 'field_59c889ede774d'),
(473, 74, 'title_2_count', '165'),
(474, 74, '_title_2_count', 'field_59c889f4e774e'),
(475, 74, 'title_3', 'Students'),
(476, 74, '_title_3', 'field_59c889fbe774f'),
(477, 74, 'title_3_count', '563'),
(478, 74, '_title_3_count', 'field_59c88a03e7750'),
(479, 74, 'title_4', 'Placements'),
(480, 74, '_title_4', 'field_59c88a0ae7751'),
(481, 74, 'title_4_count', '245'),
(482, 74, '_title_4_count', 'field_59c88a24e7752'),
(483, 74, 'q/a_categories_title', 'Our Question Categories'),
(484, 74, '_q/a_categories_title', 'field_59c88f1592922'),
(485, 74, 'q/a_categories_1_image', '33'),
(486, 74, '_q/a_categories_1_image', 'field_59c88f3f92923'),
(487, 74, 'q/a_categories_1_icon', ''),
(488, 74, '_q/a_categories_1_icon', 'field_59c88f5392924'),
(489, 74, 'q/a_categories_1_content', ''),
(490, 74, '_q/a_categories_1_content', 'field_59c88f6392925'),
(491, 74, 'q/a_categories_2_image', ''),
(492, 74, '_q/a_categories_2_image', 'field_59c88f8092926'),
(493, 74, 'q/a_categories_2_icon', ''),
(494, 74, '_q/a_categories_2_icon', 'field_59c88f9b92927'),
(495, 74, 'q/a_categories_2_content', ''),
(496, 74, '_q/a_categories_2_content', 'field_59c88fa992928'),
(497, 74, 'q/a_categories_3_image', ''),
(498, 74, '_q/a_categories_3_image', 'field_59c88fbd92929'),
(499, 74, 'q/a_categories_3_icon', ''),
(500, 74, '_q/a_categories_3_icon', 'field_59c88ff89292a'),
(501, 74, 'q/a_categories_3_content', ''),
(502, 74, '_q/a_categories_3_content', 'field_59c8900b9292b'),
(503, 74, 'q/a_categories_4_image', ''),
(504, 74, '_q/a_categories_4_image', 'field_59c890189292c'),
(505, 74, 'q/a_categories_4_icon', ''),
(506, 74, '_q/a_categories_4_icon', 'field_59c890269292d'),
(507, 74, 'q/a_categories_4_content', ''),
(508, 74, '_q/a_categories_4_content', 'field_59c8903b9292e'),
(509, 74, 'q/a_categories_5_image', ''),
(510, 74, '_q/a_categories_5_image', 'field_59c8905f9292f'),
(511, 74, 'q/a_categories_5_icon', ''),
(512, 74, '_q/a_categories_5_icon', 'field_59c8906f92930'),
(513, 74, 'q/a_categories_5_content', ''),
(514, 74, '_q/a_categories_5_content', 'field_59c8908592931'),
(515, 74, 'q/a_categories_6_image', ''),
(516, 74, '_q/a_categories_6_image', 'field_59c8908e92932'),
(517, 74, 'q/a_categories_6_icon', ''),
(518, 74, '_q/a_categories_6_icon', 'field_59c8909f92933'),
(519, 74, 'q/a_categories_6_content', ''),
(520, 74, '_q/a_categories_6_content', 'field_59c890f192934'),
(521, 74, 'q/a_categories_7_image', ''),
(522, 74, '_q/a_categories_7_image', 'field_59c8911392935'),
(523, 74, 'q/a_categories_7_icon', ''),
(524, 74, '_q/a_categories_7_icon', 'field_59c8915492936'),
(525, 74, 'q/a_categories_content', ''),
(526, 74, '_q/a_categories_content', 'field_59c8919c92937'),
(527, 74, 'q/a_categories_8_image', ''),
(528, 74, '_q/a_categories_8_image', 'field_59c891b592938'),
(529, 74, 'q/a_categories_8_icon', ''),
(530, 74, '_q/a_categories_8_icon', 'field_59c8925a92939'),
(531, 74, 'q/a_categories_8_content', ''),
(532, 74, '_q/a_categories_8_content', 'field_59c8926b9293a'),
(533, 74, 'q/a_categories_9_image', ''),
(534, 74, '_q/a_categories_9_image', 'field_59c8927d9293b'),
(535, 74, 'q/a_categories_9_icon', ''),
(536, 74, '_q/a_categories_9_icon', 'field_59c892ec9293c'),
(537, 74, 'q/a_categories_9_content', ''),
(538, 74, '_q/a_categories_9_content', 'field_59c893129293d'),
(539, 45, 'q/a_categories_title', 'Our Question Categories'),
(540, 45, '_q/a_categories_title', 'field_59c88f1592922'),
(541, 45, 'q/a_categories_1_image', '33'),
(542, 45, '_q/a_categories_1_image', 'field_59c88f3f92923'),
(543, 45, 'q/a_categories_1_icon', '75'),
(544, 45, '_q/a_categories_1_icon', 'field_59c88f5392924'),
(545, 45, 'q/a_categories_1_content', 'Computer Science engineering'),
(546, 45, '_q/a_categories_1_content', 'field_59c88f6392925'),
(547, 45, 'q/a_categories_2_image', '34'),
(548, 45, '_q/a_categories_2_image', 'field_59c88f8092926'),
(549, 45, 'q/a_categories_2_icon', '77'),
(550, 45, '_q/a_categories_2_icon', 'field_59c88f9b92927'),
(551, 45, 'q/a_categories_2_content', 'electronic communication engineering'),
(552, 45, '_q/a_categories_2_content', 'field_59c88fa992928'),
(553, 45, 'q/a_categories_3_image', '35'),
(554, 45, '_q/a_categories_3_image', 'field_59c88fbd92929'),
(555, 45, 'q/a_categories_3_icon', '78'),
(556, 45, '_q/a_categories_3_icon', 'field_59c88ff89292a'),
(557, 45, 'q/a_categories_3_content', 'information technology'),
(558, 45, '_q/a_categories_3_content', 'field_59c8900b9292b'),
(559, 45, 'q/a_categories_4_image', '36'),
(560, 45, '_q/a_categories_4_image', 'field_59c890189292c'),
(561, 45, 'q/a_categories_4_icon', '79'),
(562, 45, '_q/a_categories_4_icon', 'field_59c890269292d'),
(563, 45, 'q/a_categories_4_content', 'aeronautical engineering'),
(564, 45, '_q/a_categories_4_content', 'field_59c8903b9292e'),
(565, 45, 'q/a_categories_5_image', '37'),
(566, 45, '_q/a_categories_5_image', 'field_59c8905f9292f'),
(567, 45, 'q/a_categories_5_icon', '80'),
(568, 45, '_q/a_categories_5_icon', 'field_59c8906f92930'),
(569, 45, 'q/a_categories_5_content', 'civil engineering'),
(570, 45, '_q/a_categories_5_content', 'field_59c8908592931'),
(571, 45, 'q/a_categories_6_image', '38'),
(572, 45, '_q/a_categories_6_image', 'field_59c8908e92932'),
(573, 45, 'q/a_categories_6_icon', '81'),
(574, 45, '_q/a_categories_6_icon', 'field_59c8909f92933'),
(575, 45, 'q/a_categories_6_content', 'electrical and electronics engineering'),
(576, 45, '_q/a_categories_6_content', 'field_59c890f192934'),
(577, 45, 'q/a_categories_7_image', '39'),
(578, 45, '_q/a_categories_7_image', 'field_59c8911392935'),
(579, 45, 'q/a_categories_7_icon', '82'),
(580, 45, '_q/a_categories_7_icon', 'field_59c8915492936'),
(581, 45, 'q/a_categories_content', 'mechanical engineering'),
(582, 45, '_q/a_categories_content', 'field_59c8919c92937'),
(583, 45, 'q/a_categories_8_image', '40'),
(584, 45, '_q/a_categories_8_image', 'field_59c891b592938'),
(585, 45, 'q/a_categories_8_icon', '83'),
(586, 45, '_q/a_categories_8_icon', 'field_59c8925a92939'),
(587, 45, 'q/a_categories_8_content', 'automobile engineering'),
(588, 45, '_q/a_categories_8_content', 'field_59c8926b9293a'),
(589, 45, 'q/a_categories_9_image', '41'),
(590, 45, '_q/a_categories_9_image', 'field_59c8927d9293b'),
(591, 45, 'q/a_categories_9_icon', '84'),
(592, 45, '_q/a_categories_9_icon', 'field_59c892ec9293c'),
(593, 45, 'q/a_categories_9_content', 'biomedical engineering'),
(594, 45, '_q/a_categories_9_content', 'field_59c893129293d'),
(595, 75, '_wp_attached_file', '2017/09/icon-cs.svg'),
(596, 76, 'slide_content_1', 'instead of building walls we can help build bridges...'),
(597, 76, '_slide_content_1', 'field_59c881489ec0b'),
(598, 76, 'slide_content_2', 'if you don\'t give up you still have a chance giving up is the greatest failure...'),
(599, 76, '_slide_content_2', 'field_59c881879ec0c'),
(600, 76, 'slide_content_3', 'Imagine with all your mind believe...'),
(601, 76, '_slide_content_3', 'field_59c8818f9ec0d'),
(602, 76, 'slide_content_4', 'Work hard dream big...'),
(603, 76, '_slide_content_4', 'field_59c881a49ec0e'),
(604, 76, 'slide_content_5', 'Your Time is limited...'),
(605, 76, '_slide_content_5', 'field_59c881ab9ec0f'),
(606, 76, 'about_us_title', 'About Us'),
(607, 76, '_about_us_title', 'field_59c8836295fa6'),
(608, 76, 'about_us_caption_text', 'Practice & Get Placed Dream job through knowledge season'),
(609, 76, '_about_us_caption_text', 'field_59c8836e95fa7'),
(610, 76, 'about_us_content', '1. Our motto is If you can dream it, you can do it.\r\n\r\n2. This knowledge season created for the main purpose is every students can do find the\r\nour future dream job work flow before complete your graduation.'),
(611, 76, '_about_us_content', 'field_59c8838595fa8'),
(612, 76, 'online_exam_image', '23'),
(613, 76, '_online_exam_image', 'field_59c885986d4cc'),
(614, 76, 'online_exam_title', 'Welcome To Online Exam'),
(615, 76, '_online_exam_title', 'field_59c885b26d4cd'),
(616, 76, 'online_exam_content', 'Prepare and get placed in the easiest way possible.\r\n\r\nIts really easy to get your knowledge score and you can find your area of interset.'),
(617, 76, '_online_exam_content', 'field_59c885c56d4ce'),
(618, 76, 'exam_steps_title', 'Follow test For Easy Steps'),
(619, 76, '_exam_steps_title', 'field_59c887d980fc5'),
(620, 76, 'exam_question_number_1', '01'),
(621, 76, '_exam_question_number_1', 'field_59c887ea80fc6'),
(622, 76, 'exam_question_number_1_title', 'Login'),
(623, 76, '_exam_question_number_1_title', 'field_59c887f080fc7'),
(624, 76, 'exam_question_number_2', '02'),
(625, 76, '_exam_question_number_2', 'field_59c887fb80fc8'),
(626, 76, 'exam_question_number_2_title', 'Choose categories'),
(627, 76, '_exam_question_number_2_title', 'field_59c8880480fc9'),
(628, 76, 'exam_question_number_3', '03'),
(629, 76, '_exam_question_number_3', 'field_59c8880e80fca'),
(630, 76, 'exam_question_number_3_title', 'Take test'),
(631, 76, '_exam_question_number_3_title', 'field_59c8881580fcb'),
(632, 76, 'title_1', 'Collages'),
(633, 76, '_title_1', 'field_59c889a4e774b'),
(634, 76, 'title_1_count', '45'),
(635, 76, '_title_1_count', 'field_59c889d3e774c'),
(636, 76, 'title_2', 'companies'),
(637, 76, '_title_2', 'field_59c889ede774d'),
(638, 76, 'title_2_count', '165'),
(639, 76, '_title_2_count', 'field_59c889f4e774e'),
(640, 76, 'title_3', 'Students'),
(641, 76, '_title_3', 'field_59c889fbe774f'),
(642, 76, 'title_3_count', '563'),
(643, 76, '_title_3_count', 'field_59c88a03e7750'),
(644, 76, 'title_4', 'Placements'),
(645, 76, '_title_4', 'field_59c88a0ae7751'),
(646, 76, 'title_4_count', '245'),
(647, 76, '_title_4_count', 'field_59c88a24e7752'),
(648, 76, 'q/a_categories_title', 'Our Question Categories'),
(649, 76, '_q/a_categories_title', 'field_59c88f1592922'),
(650, 76, 'q/a_categories_1_image', '33'),
(651, 76, '_q/a_categories_1_image', 'field_59c88f3f92923'),
(652, 76, 'q/a_categories_1_icon', '75'),
(653, 76, '_q/a_categories_1_icon', 'field_59c88f5392924'),
(654, 76, 'q/a_categories_1_content', ''),
(655, 76, '_q/a_categories_1_content', 'field_59c88f6392925'),
(656, 76, 'q/a_categories_2_image', ''),
(657, 76, '_q/a_categories_2_image', 'field_59c88f8092926'),
(658, 76, 'q/a_categories_2_icon', ''),
(659, 76, '_q/a_categories_2_icon', 'field_59c88f9b92927'),
(660, 76, 'q/a_categories_2_content', ''),
(661, 76, '_q/a_categories_2_content', 'field_59c88fa992928'),
(662, 76, 'q/a_categories_3_image', ''),
(663, 76, '_q/a_categories_3_image', 'field_59c88fbd92929'),
(664, 76, 'q/a_categories_3_icon', ''),
(665, 76, '_q/a_categories_3_icon', 'field_59c88ff89292a'),
(666, 76, 'q/a_categories_3_content', ''),
(667, 76, '_q/a_categories_3_content', 'field_59c8900b9292b'),
(668, 76, 'q/a_categories_4_image', ''),
(669, 76, '_q/a_categories_4_image', 'field_59c890189292c'),
(670, 76, 'q/a_categories_4_icon', ''),
(671, 76, '_q/a_categories_4_icon', 'field_59c890269292d'),
(672, 76, 'q/a_categories_4_content', ''),
(673, 76, '_q/a_categories_4_content', 'field_59c8903b9292e'),
(674, 76, 'q/a_categories_5_image', ''),
(675, 76, '_q/a_categories_5_image', 'field_59c8905f9292f'),
(676, 76, 'q/a_categories_5_icon', ''),
(677, 76, '_q/a_categories_5_icon', 'field_59c8906f92930'),
(678, 76, 'q/a_categories_5_content', ''),
(679, 76, '_q/a_categories_5_content', 'field_59c8908592931'),
(680, 76, 'q/a_categories_6_image', ''),
(681, 76, '_q/a_categories_6_image', 'field_59c8908e92932'),
(682, 76, 'q/a_categories_6_icon', ''),
(683, 76, '_q/a_categories_6_icon', 'field_59c8909f92933'),
(684, 76, 'q/a_categories_6_content', ''),
(685, 76, '_q/a_categories_6_content', 'field_59c890f192934'),
(686, 76, 'q/a_categories_7_image', ''),
(687, 76, '_q/a_categories_7_image', 'field_59c8911392935'),
(688, 76, 'q/a_categories_7_icon', ''),
(689, 76, '_q/a_categories_7_icon', 'field_59c8915492936'),
(690, 76, 'q/a_categories_content', ''),
(691, 76, '_q/a_categories_content', 'field_59c8919c92937'),
(692, 76, 'q/a_categories_8_image', ''),
(693, 76, '_q/a_categories_8_image', 'field_59c891b592938'),
(694, 76, 'q/a_categories_8_icon', ''),
(695, 76, '_q/a_categories_8_icon', 'field_59c8925a92939'),
(696, 76, 'q/a_categories_8_content', ''),
(697, 76, '_q/a_categories_8_content', 'field_59c8926b9293a'),
(698, 76, 'q/a_categories_9_image', ''),
(699, 76, '_q/a_categories_9_image', 'field_59c8927d9293b'),
(700, 76, 'q/a_categories_9_icon', ''),
(701, 76, '_q/a_categories_9_icon', 'field_59c892ec9293c'),
(702, 76, 'q/a_categories_9_content', ''),
(703, 76, '_q/a_categories_9_content', 'field_59c893129293d'),
(704, 77, '_wp_attached_file', '2017/09/icon-ece.svg'),
(705, 67, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"45\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(706, 78, '_wp_attached_file', '2017/09/icon-it.svg'),
(707, 79, '_wp_attached_file', '2017/09/icon-aero.svg'),
(708, 80, '_wp_attached_file', '2017/09/icon-civil.svg'),
(709, 81, '_wp_attached_file', '2017/09/icon-eee.svg'),
(710, 82, '_wp_attached_file', '2017/09/icon-mechnical.svg'),
(711, 83, '_wp_attached_file', '2017/09/icon-automobile.svg'),
(712, 84, '_wp_attached_file', '2017/09/icon-biomedical.svg'),
(713, 85, 'slide_content_1', 'instead of building walls we can help build bridges...'),
(714, 85, '_slide_content_1', 'field_59c881489ec0b'),
(715, 85, 'slide_content_2', 'if you don\'t give up you still have a chance giving up is the greatest failure...'),
(716, 85, '_slide_content_2', 'field_59c881879ec0c'),
(717, 85, 'slide_content_3', 'Imagine with all your mind believe...'),
(718, 85, '_slide_content_3', 'field_59c8818f9ec0d'),
(719, 85, 'slide_content_4', 'Work hard dream big...'),
(720, 85, '_slide_content_4', 'field_59c881a49ec0e'),
(721, 85, 'slide_content_5', 'Your Time is limited...'),
(722, 85, '_slide_content_5', 'field_59c881ab9ec0f'),
(723, 85, 'about_us_title', 'About Us'),
(724, 85, '_about_us_title', 'field_59c8836295fa6'),
(725, 85, 'about_us_caption_text', 'Practice & Get Placed Dream job through knowledge season'),
(726, 85, '_about_us_caption_text', 'field_59c8836e95fa7'),
(727, 85, 'about_us_content', '1. Our motto is If you can dream it, you can do it.\r\n\r\n2. This knowledge season created for the main purpose is every students can do find the\r\nour future dream job work flow before complete your graduation.'),
(728, 85, '_about_us_content', 'field_59c8838595fa8'),
(729, 85, 'online_exam_image', '23'),
(730, 85, '_online_exam_image', 'field_59c885986d4cc'),
(731, 85, 'online_exam_title', 'Welcome To Online Exam'),
(732, 85, '_online_exam_title', 'field_59c885b26d4cd'),
(733, 85, 'online_exam_content', 'Prepare and get placed in the easiest way possible.\r\n\r\nIts really easy to get your knowledge score and you can find your area of interset.'),
(734, 85, '_online_exam_content', 'field_59c885c56d4ce'),
(735, 85, 'exam_steps_title', 'Follow test For Easy Steps'),
(736, 85, '_exam_steps_title', 'field_59c887d980fc5'),
(737, 85, 'exam_question_number_1', '01'),
(738, 85, '_exam_question_number_1', 'field_59c887ea80fc6'),
(739, 85, 'exam_question_number_1_title', 'Login'),
(740, 85, '_exam_question_number_1_title', 'field_59c887f080fc7'),
(741, 85, 'exam_question_number_2', '02'),
(742, 85, '_exam_question_number_2', 'field_59c887fb80fc8'),
(743, 85, 'exam_question_number_2_title', 'Choose categories'),
(744, 85, '_exam_question_number_2_title', 'field_59c8880480fc9'),
(745, 85, 'exam_question_number_3', '03'),
(746, 85, '_exam_question_number_3', 'field_59c8880e80fca'),
(747, 85, 'exam_question_number_3_title', 'Take test'),
(748, 85, '_exam_question_number_3_title', 'field_59c8881580fcb'),
(749, 85, 'title_1', 'Collages'),
(750, 85, '_title_1', 'field_59c889a4e774b'),
(751, 85, 'title_1_count', '45'),
(752, 85, '_title_1_count', 'field_59c889d3e774c'),
(753, 85, 'title_2', 'companies'),
(754, 85, '_title_2', 'field_59c889ede774d'),
(755, 85, 'title_2_count', '165'),
(756, 85, '_title_2_count', 'field_59c889f4e774e'),
(757, 85, 'title_3', 'Students'),
(758, 85, '_title_3', 'field_59c889fbe774f'),
(759, 85, 'title_3_count', '563'),
(760, 85, '_title_3_count', 'field_59c88a03e7750'),
(761, 85, 'title_4', 'Placements'),
(762, 85, '_title_4', 'field_59c88a0ae7751'),
(763, 85, 'title_4_count', '245'),
(764, 85, '_title_4_count', 'field_59c88a24e7752'),
(765, 85, 'q/a_categories_title', 'Our Question Categories'),
(766, 85, '_q/a_categories_title', 'field_59c88f1592922'),
(767, 85, 'q/a_categories_1_image', '33'),
(768, 85, '_q/a_categories_1_image', 'field_59c88f3f92923'),
(769, 85, 'q/a_categories_1_icon', '75'),
(770, 85, '_q/a_categories_1_icon', 'field_59c88f5392924'),
(771, 85, 'q/a_categories_1_content', 'Computer Science engineering'),
(772, 85, '_q/a_categories_1_content', 'field_59c88f6392925'),
(773, 85, 'q/a_categories_2_image', '34'),
(774, 85, '_q/a_categories_2_image', 'field_59c88f8092926'),
(775, 85, 'q/a_categories_2_icon', '77'),
(776, 85, '_q/a_categories_2_icon', 'field_59c88f9b92927'),
(777, 85, 'q/a_categories_2_content', 'electronic communication engineering'),
(778, 85, '_q/a_categories_2_content', 'field_59c88fa992928'),
(779, 85, 'q/a_categories_3_image', '35'),
(780, 85, '_q/a_categories_3_image', 'field_59c88fbd92929'),
(781, 85, 'q/a_categories_3_icon', '78'),
(782, 85, '_q/a_categories_3_icon', 'field_59c88ff89292a'),
(783, 85, 'q/a_categories_3_content', 'information technology'),
(784, 85, '_q/a_categories_3_content', 'field_59c8900b9292b'),
(785, 85, 'q/a_categories_4_image', '36'),
(786, 85, '_q/a_categories_4_image', 'field_59c890189292c'),
(787, 85, 'q/a_categories_4_icon', '79'),
(788, 85, '_q/a_categories_4_icon', 'field_59c890269292d'),
(789, 85, 'q/a_categories_4_content', 'aeronautical engineering'),
(790, 85, '_q/a_categories_4_content', 'field_59c8903b9292e'),
(791, 85, 'q/a_categories_5_image', '37'),
(792, 85, '_q/a_categories_5_image', 'field_59c8905f9292f'),
(793, 85, 'q/a_categories_5_icon', '80'),
(794, 85, '_q/a_categories_5_icon', 'field_59c8906f92930'),
(795, 85, 'q/a_categories_5_content', 'civil engineering'),
(796, 85, '_q/a_categories_5_content', 'field_59c8908592931'),
(797, 85, 'q/a_categories_6_image', '38'),
(798, 85, '_q/a_categories_6_image', 'field_59c8908e92932'),
(799, 85, 'q/a_categories_6_icon', '81'),
(800, 85, '_q/a_categories_6_icon', 'field_59c8909f92933'),
(801, 85, 'q/a_categories_6_content', 'electrical and electronics engineering'),
(802, 85, '_q/a_categories_6_content', 'field_59c890f192934'),
(803, 85, 'q/a_categories_7_image', '39'),
(804, 85, '_q/a_categories_7_image', 'field_59c8911392935'),
(805, 85, 'q/a_categories_7_icon', '82'),
(806, 85, '_q/a_categories_7_icon', 'field_59c8915492936'),
(807, 85, 'q/a_categories_content', 'mechanical engineering'),
(808, 85, '_q/a_categories_content', 'field_59c8919c92937'),
(809, 85, 'q/a_categories_8_image', '40'),
(810, 85, '_q/a_categories_8_image', 'field_59c891b592938'),
(811, 85, 'q/a_categories_8_icon', '83'),
(812, 85, '_q/a_categories_8_icon', 'field_59c8925a92939'),
(813, 85, 'q/a_categories_8_content', 'automobile engineering'),
(814, 85, '_q/a_categories_8_content', 'field_59c8926b9293a'),
(815, 85, 'q/a_categories_9_image', '41'),
(816, 85, '_q/a_categories_9_image', 'field_59c8927d9293b'),
(817, 85, 'q/a_categories_9_icon', '84'),
(818, 85, '_q/a_categories_9_icon', 'field_59c892ec9293c'),
(819, 85, 'q/a_categories_9_content', 'biomedical engineering'),
(820, 85, '_q/a_categories_9_content', 'field_59c893129293d'),
(821, 86, '_edit_last', '1'),
(822, 86, '_edit_lock', '1506318682:1'),
(823, 87, '_edit_last', '1'),
(824, 87, '_edit_lock', '1506318685:1'),
(825, 88, '_edit_last', '1'),
(826, 88, '_edit_lock', '1506318689:1'),
(827, 86, '_thumbnail_id', '30'),
(829, 87, '_thumbnail_id', '29'),
(833, 88, '_thumbnail_id', '23'),
(834, 93, '_edit_last', '1'),
(835, 93, '_edit_lock', '1506493934:1'),
(836, 94, '_edit_last', '1'),
(837, 94, 'field_59cb33a515fc9', 'a:8:{s:3:\"key\";s:19:\"field_59cb33a515fc9\";s:5:\"label\";s:24:\"Check Box Type Questions\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:19:\"field_59cb35862e019\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"1\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(838, 94, 'field_59cb33c215fca', 'a:8:{s:3:\"key\";s:19:\"field_59cb33c215fca\";s:5:\"label\";s:27:\"Radio Button Type Questions\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}'),
(839, 94, 'field_59cb33db15fcb', 'a:8:{s:3:\"key\";s:19:\"field_59cb33db15fcb\";s:5:\"label\";s:22:\"Boolean Type Questions\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:19:\"field_59cb35862e019\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"1\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:9;}'),
(841, 94, 'position', 'acf_after_title'),
(842, 94, 'layout', 'no_box'),
(843, 94, 'hide_on_screen', ''),
(844, 94, '_edit_lock', '1506493934:1'),
(846, 94, 'field_59cb346d4640d', 'a:11:{s:3:\"key\";s:19:\"field_59cb346d4640d\";s:5:\"label\";s:19:\"Checkbox Question 1\";s:4:\"name\";s:19:\"checkbox_question_1\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:2:\"no\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(847, 94, 'field_59cb34e44640e', 'a:11:{s:3:\"key\";s:19:\"field_59cb34e44640e\";s:5:\"label\";s:19:\"Checkbox Question 2\";s:4:\"name\";s:19:\"checkbox_question_2\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:2:\"no\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(848, 94, 'field_59cb34f34640f', 'a:11:{s:3:\"key\";s:19:\"field_59cb34f34640f\";s:5:\"label\";s:19:\"Checkbox Question 3\";s:4:\"name\";s:19:\"checkbox_question_3\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:2:\"no\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(849, 94, 'field_59cb34ff46410', 'a:11:{s:3:\"key\";s:19:\"field_59cb34ff46410\";s:5:\"label\";s:19:\"Checkbox Question 4\";s:4:\"name\";s:19:\"checkbox_question_4\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:2:\"no\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(850, 94, 'field_59cb350c46411', 'a:14:{s:3:\"key\";s:19:\"field_59cb350c46411\";s:5:\"label\";s:23:\"Radio Button Question 1\";s:4:\"name\";s:23:\"radio_button_question_1\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}'),
(863, 94, 'field_59cb3884d20f8', 'a:11:{s:3:\"key\";s:19:\"field_59cb3884d20f8\";s:5:\"label\";s:16:\"Boolean Question\";s:4:\"name\";s:16:\"boolean_question\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:19:\"field_59cb35862e019\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"1\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:10;}'),
(868, 95, '_edit_last', '1'),
(869, 95, 'field_59cb3ac00c005', 'a:12:{s:3:\"key\";s:19:\"field_59cb3ac00c005\";s:5:\"label\";s:23:\"Checkbox Correct Answer\";s:4:\"name\";s:23:\"checkbox_correct_answer\";s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:7:\"choices\";a:4:{i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";}s:13:\"default_value\";s:1:\"1\";s:10:\"allow_null\";s:1:\"0\";s:8:\"multiple\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(871, 95, 'position', 'side'),
(872, 95, 'layout', 'default'),
(873, 95, 'hide_on_screen', ''),
(874, 95, '_edit_lock', '1506493935:1'),
(878, 95, 'field_59cb3d6babeaf', 'a:12:{s:3:\"key\";s:19:\"field_59cb3d6babeaf\";s:5:\"label\";s:27:\"Radio Button Correct Answer\";s:4:\"name\";s:27:\"radio_button_correct_answer\";s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:7:\"choices\";a:2:{i:1;s:1:\"1\";i:2;s:1:\"2\";}s:13:\"default_value\";s:1:\"1\";s:10:\"allow_null\";s:1:\"0\";s:8:\"multiple\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:19:\"field_59cb3ac00c005\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"1\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(879, 95, 'field_59cb3d80abeb0', 'a:13:{s:3:\"key\";s:19:\"field_59cb3d80abeb0\";s:5:\"label\";s:22:\"Boolean Correct Answer\";s:4:\"name\";s:22:\"boolean_correct_answer\";s:4:\"type\";s:5:\"radio\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:7:\"choices\";a:2:{s:4:\"true\";s:4:\"True\";s:5:\"false\";s:5:\"False\";}s:12:\"other_choice\";s:1:\"0\";s:17:\"save_other_choice\";s:1:\"0\";s:13:\"default_value\";s:4:\"true\";s:6:\"layout\";s:8:\"vertical\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:19:\"field_59cb3ac00c005\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"1\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(880, 95, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"quiz_questions\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(884, 94, 'field_59cb425ad70d4', 'a:14:{s:3:\"key\";s:19:\"field_59cb425ad70d4\";s:5:\"label\";s:21:\"Radio Button Answer 1\";s:4:\"name\";s:21:\"radio_button_answer_1\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:19:\"field_59cb35862e019\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"1\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}'),
(885, 94, 'field_59cb4281d70d5', 'a:14:{s:3:\"key\";s:19:\"field_59cb4281d70d5\";s:5:\"label\";s:21:\"Radio Button Answer 2\";s:4:\"name\";s:21:\"radio_button_answer_2\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:19:\"field_59cb35862e019\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"1\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:8;}'),
(888, 94, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"quiz_questions\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(889, 117, '_edit_last', '1'),
(890, 117, 'field_59cdadd56453d', 'a:8:{s:3:\"key\";s:19:\"field_59cdadd56453d\";s:5:\"label\";s:18:\"Left Block Section\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(891, 117, 'field_59cdae0e6453e', 'a:14:{s:3:\"key\";s:19:\"field_59cdae0e6453e\";s:5:\"label\";s:24:\"Left Block Section Title\";s:4:\"name\";s:24:\"left_block_section_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(892, 117, 'field_59cdae276453f', 'a:11:{s:3:\"key\";s:19:\"field_59cdae276453f\";s:5:\"label\";s:26:\"Left Block Section Content\";s:4:\"name\";s:26:\"left_block_section_content\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(893, 117, 'field_59cdae3664540', 'a:8:{s:3:\"key\";s:19:\"field_59cdae3664540\";s:5:\"label\";s:20:\"Center Block Section\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(894, 117, 'field_59cdae5064541', 'a:14:{s:3:\"key\";s:19:\"field_59cdae5064541\";s:5:\"label\";s:26:\"Center Block Section Title\";s:4:\"name\";s:26:\"center_block_section_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(895, 117, 'field_59cdae6264542', 'a:11:{s:3:\"key\";s:19:\"field_59cdae6264542\";s:5:\"label\";s:28:\"Center Block Section Content\";s:4:\"name\";s:28:\"center_block_section_content\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}'),
(896, 117, 'field_59cdae7764543', 'a:8:{s:3:\"key\";s:19:\"field_59cdae7764543\";s:5:\"label\";s:19:\"Right Block Section\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}'),
(897, 117, 'field_59cdae9864544', 'a:14:{s:3:\"key\";s:19:\"field_59cdae9864544\";s:5:\"label\";s:25:\"Right Block Section Title\";s:4:\"name\";s:25:\"right_block_section_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}'),
(898, 117, 'field_59cdaea664545', 'a:11:{s:3:\"key\";s:19:\"field_59cdaea664545\";s:5:\"label\";s:27:\"Right Block Section Content\";s:4:\"name\";s:27:\"right_block_section_content\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:8;}'),
(900, 117, 'position', 'normal'),
(901, 117, 'layout', 'no_box'),
(902, 117, 'hide_on_screen', ''),
(903, 117, '_edit_lock', '1506653450:1'),
(904, 118, 'left_block_section_title', 'Address'),
(905, 118, '_left_block_section_title', 'field_59cdae0e6453e'),
(906, 118, 'left_block_section_content', '<p style=\"text-align: left;\">3481 Melrose Place, 2K32 Street , Beverly Hills,</p>\r\n<p style=\"text-align: left;\">New York City 90210.</p>'),
(907, 118, '_left_block_section_content', 'field_59cdae276453f'),
(908, 118, 'center_block_section_title', 'Email'),
(909, 118, '_center_block_section_title', 'field_59cdae5064541'),
(910, 118, 'center_block_section_content', '<a href=\"mailto:info@example.com\">info@example1.com</a>\r\n\r\n<a href=\"mailto:info@example.com\">info@example2.com</a>'),
(911, 118, '_center_block_section_content', 'field_59cdae6264542'),
(912, 118, 'right_block_section_title', 'Call Us'),
(913, 118, '_right_block_section_title', 'field_59cdae9864544'),
(914, 118, 'right_block_section_content', '(+000) 123 345 456\r\n\r\n(+010) 123 345 458'),
(915, 118, '_right_block_section_content', 'field_59cdaea664545'),
(916, 53, 'left_block_section_title', 'Address'),
(917, 53, '_left_block_section_title', 'field_59cdae0e6453e'),
(918, 53, 'left_block_section_content', '<p style=\"text-align: left;\">3481 Melrose Place, 2K32 Street , Beverly Hills,</p>\r\n<p style=\"text-align: left;\">New York City 90210.</p>'),
(919, 53, '_left_block_section_content', 'field_59cdae276453f'),
(920, 53, 'center_block_section_title', 'Email'),
(921, 53, '_center_block_section_title', 'field_59cdae5064541'),
(922, 53, 'center_block_section_content', '<a href=\"mailto:info@example.com\">info@example1.com</a>\r\n\r\n<a href=\"mailto:info@example.com\">info@example2.com</a>'),
(923, 53, '_center_block_section_content', 'field_59cdae6264542'),
(924, 53, 'right_block_section_title', 'Call Us'),
(925, 53, '_right_block_section_title', 'field_59cdae9864544'),
(926, 53, 'right_block_section_content', '(+000) 123 345 456\r\n\r\n(+010) 123 345 458'),
(927, 53, '_right_block_section_content', 'field_59cdaea664545'),
(928, 119, 'left_block_section_title', 'Address'),
(929, 119, '_left_block_section_title', 'field_59cdae0e6453e'),
(930, 119, 'left_block_section_content', '<p style=\"text-align: left;\">3481 Melrose Place, 2K32 Street , Beverly Hills,</p>\r\n<p style=\"text-align: left;\">New York City 90210.</p>'),
(931, 119, '_left_block_section_content', 'field_59cdae276453f'),
(932, 119, 'center_block_section_title', 'Email'),
(933, 119, '_center_block_section_title', 'field_59cdae5064541'),
(934, 119, 'center_block_section_content', '<a href=\"mailto:info@example.com\">info@example1.com</a>\r\n\r\n<a href=\"mailto:info@example.com\">info@example2.com</a>'),
(935, 119, '_center_block_section_content', 'field_59cdae6264542'),
(936, 119, 'right_block_section_title', 'Call Us'),
(937, 119, '_right_block_section_title', 'field_59cdae9864544'),
(938, 119, 'right_block_section_content', '(+000) 123 345 456\r\n\r\n(+010) 123 345 458'),
(939, 119, '_right_block_section_content', 'field_59cdaea664545'),
(940, 117, 'field_59cdb4a455a30', 'a:8:{s:3:\"key\";s:19:\"field_59cdb4a455a30\";s:5:\"label\";s:20:\"Contact Form Section\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:9;}'),
(941, 117, 'field_59cdb4c155a31', 'a:14:{s:3:\"key\";s:19:\"field_59cdb4c155a31\";s:5:\"label\";s:18:\"Contact form Title\";s:4:\"name\";s:18:\"contact_form_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:10;}'),
(942, 117, 'field_59cdb4dd55a32', 'a:11:{s:3:\"key\";s:19:\"field_59cdb4dd55a32\";s:5:\"label\";s:24:\"Contact Form Description\";s:4:\"name\";s:24:\"contact_form_description\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:11;}'),
(943, 117, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"53\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(944, 120, 'left_block_section_title', 'Address'),
(945, 120, '_left_block_section_title', 'field_59cdae0e6453e'),
(946, 120, 'left_block_section_content', '<p style=\"text-align: left;\">3481 Melrose Place, 2K32 Street , Beverly Hills,</p>\r\n<p style=\"text-align: left;\">New York City 90210.</p>'),
(947, 120, '_left_block_section_content', 'field_59cdae276453f'),
(948, 120, 'center_block_section_title', 'Email'),
(949, 120, '_center_block_section_title', 'field_59cdae5064541'),
(950, 120, 'center_block_section_content', '<a href=\"mailto:info@example.com\">info@example1.com</a>\r\n\r\n<a href=\"mailto:info@example.com\">info@example2.com</a>'),
(951, 120, '_center_block_section_content', 'field_59cdae6264542'),
(952, 120, 'right_block_section_title', 'Call Us'),
(953, 120, '_right_block_section_title', 'field_59cdae9864544'),
(954, 120, 'right_block_section_content', '(+000) 123 345 456\r\n\r\n(+010) 123 345 458'),
(955, 120, '_right_block_section_content', 'field_59cdaea664545'),
(956, 120, 'contact_form_title', 'Send us message'),
(957, 120, '_contact_form_title', 'field_59cdb4c155a31'),
(958, 120, 'contact_form_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac tellus in quam congue facilisis non ut nibh. Fusce ut arcu id massa vestibulum gravida et vitae est. Nullam ligula orci, laoreet eget nunc ac, dictum convallis erat. Integer bibendum egestas imperdiet. Nullam lobortis consectetur massa vitae fermentum. Mauris malesuada mauris a ante maximus convallis. Quisque vitae ipsum non eros cursus eleifend.'),
(959, 120, '_contact_form_description', 'field_59cdb4dd55a32'),
(960, 53, 'contact_form_title', 'Send us message'),
(961, 53, '_contact_form_title', 'field_59cdb4c155a31'),
(962, 53, 'contact_form_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac tellus in quam congue facilisis non ut nibh. Fusce ut arcu id massa vestibulum gravida et vitae est. Nullam ligula orci, laoreet eget nunc ac, dictum convallis erat. Integer bibendum egestas imperdiet. Nullam lobortis consectetur massa vitae fermentum. Mauris malesuada mauris a ante maximus convallis. Quisque vitae ipsum non eros cursus eleifend.'),
(963, 53, '_contact_form_description', 'field_59cdb4dd55a32'),
(964, 121, 'left_block_section_title', 'Address'),
(965, 121, '_left_block_section_title', 'field_59cdae0e6453e'),
(966, 121, 'left_block_section_content', '<p style=\"text-align: left;\">3481 Melrose Place, 2K32 Street , Beverly Hills,</p>\r\n<p style=\"text-align: left;\">New York City 90210.</p>'),
(967, 121, '_left_block_section_content', 'field_59cdae276453f'),
(968, 121, 'center_block_section_title', 'Email'),
(969, 121, '_center_block_section_title', 'field_59cdae5064541'),
(970, 121, 'center_block_section_content', '<a href=\"mailto:info@example.com\">info@example1.com</a>\r\n\r\n<a href=\"mailto:info@example.com\">info@example2.com</a>'),
(971, 121, '_center_block_section_content', 'field_59cdae6264542'),
(972, 121, 'right_block_section_title', 'Call Us'),
(973, 121, '_right_block_section_title', 'field_59cdae9864544'),
(974, 121, 'right_block_section_content', '(+000) 123 345 456\r\n\r\n(+010) 123 345 458'),
(975, 121, '_right_block_section_content', 'field_59cdaea664545'),
(976, 121, 'contact_form_title', 'Send us message'),
(977, 121, '_contact_form_title', 'field_59cdb4c155a31'),
(978, 121, 'contact_form_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac tellus in quam congue facilisis non ut nibh. Fusce ut arcu id massa vestibulum gravida et vitae est. Nullam ligula orci, laoreet eget nunc ac, dictum convallis erat. Integer bibendum egestas imperdiet. Nullam lobortis consectetur massa vitae fermentum. Mauris malesuada mauris a ante maximus convallis. Quisque vitae ipsum non eros cursus eleifend.'),
(979, 121, '_contact_form_description', 'field_59cdb4dd55a32');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-09-24 10:05:24', '2017-09-24 10:05:24', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2017-09-24 10:05:24', '2017-09-24 10:05:24', '', 0, 'http://localhost/wpquiz/?p=1', 0, 'post', '', 1),
(2, 1, '2017-09-24 10:05:24', '2017-09-24 10:05:24', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://localhost/wpquiz/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2017-09-24 10:05:24', '2017-09-24 10:05:24', '', 0, 'http://localhost/wpquiz/?page_id=2', 0, 'page', '', 0),
(3, 1, '2017-09-24 10:05:46', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2017-09-24 10:05:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?p=3', 0, 'post', '', 0),
(4, 1, '2017-09-24 14:13:29', '2017-09-24 14:13:29', '<span class=\"input input--chisato\">\r\n	[text* your-name class:input__field class:input__field--chisato]\r\n<label class=\"input__label input__label--chisato\" for=\"input-13\">\r\n	<span class=\"input__label-content input__label-content--chisato\" data-content=\"Name\">Name</span>\r\n</label>\r\n</span>\r\n\r\n<span class=\"input input--chisato\">\r\n	[email* your-email class:input__field class:input__field--chisato]\r\n<label class=\"input__label input__label--chisato\" for=\"input-14\">\r\n	<span class=\"input__label-content input__label-content--chisato\" data-content=\"Email\">Email</span>\r\n</label>\r\n</span>\r\n\r\n<span class=\"input input--chisato\">\r\n	[text* your-subject class:input__field class:input__field--chisato]\r\n<label class=\"input__label input__label--chisato\" for=\"input-15\">\r\n	<span class=\"input__label-content input__label-content--chisato\" data-content=\"Subject\">Subject</span>\r\n</label>\r\n</span>\r\n\r\n[textarea* your-message placeholder \"Your comment here...\"]\r\n[submit \"Submit\"]\n1\nWpQuiz \"[your-subject]\"\n[your-name] <deeraj16@gmail.com>\ndeeraj16@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on WpQuiz (http://localhost/wpquiz)\nReply-To: [your-email]\n\n\n\n\nWpQuiz \"[your-subject]\"\nWpQuiz <deeraj16@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on WpQuiz (http://localhost/wpquiz)\nReply-To: deeraj16@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2017-09-29 03:00:15', '2017-09-29 03:00:15', '', 0, 'http://localhost/wpquiz/?post_type=wpcf7_contact_form&#038;p=4', 0, 'wpcf7_contact_form', '', 0),
(5, 1, '2017-09-24 15:13:01', '2017-09-24 15:13:01', '', 'g6', '', 'inherit', 'open', 'closed', '', 'g6', '', '', '2017-09-24 15:13:01', '2017-09-24 15:13:01', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/g6.jpg', 0, 'attachment', 'image/jpeg', 0),
(6, 1, '2017-09-24 15:13:15', '2017-09-24 15:13:15', '', 'g9', '', 'inherit', 'open', 'closed', '', 'g9', '', '', '2017-09-24 15:13:15', '2017-09-24 15:13:15', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/g9.jpg', 0, 'attachment', 'image/jpeg', 0),
(7, 1, '2017-09-24 15:13:26', '2017-09-24 15:13:26', '', 'move-top', '', 'inherit', 'open', 'closed', '', 'move-top', '', '', '2017-09-24 15:13:26', '2017-09-24 15:13:26', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/move-top.png', 0, 'attachment', 'image/png', 0),
(8, 1, '2017-09-24 15:13:30', '2017-09-24 15:13:30', '', 's1', '', 'inherit', 'open', 'closed', '', 's1', '', '', '2017-09-24 15:13:30', '2017-09-24 15:13:30', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/s1.jpg', 0, 'attachment', 'image/jpeg', 0),
(9, 1, '2017-09-24 15:13:39', '2017-09-24 15:13:39', '', 's2', '', 'inherit', 'open', 'closed', '', 's2', '', '', '2017-09-24 15:13:39', '2017-09-24 15:13:39', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/s2.jpg', 0, 'attachment', 'image/jpeg', 0),
(10, 1, '2017-09-24 15:13:43', '2017-09-24 15:13:43', '', 's3', '', 'inherit', 'open', 'closed', '', 's3', '', '', '2017-09-24 15:13:43', '2017-09-24 15:13:43', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/s3.jpg', 0, 'attachment', 'image/jpeg', 0),
(11, 1, '2017-09-24 15:13:45', '2017-09-24 15:13:45', '', 's4', '', 'inherit', 'open', 'closed', '', 's4', '', '', '2017-09-24 15:13:45', '2017-09-24 15:13:45', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/s4.jpg', 0, 'attachment', 'image/jpeg', 0),
(12, 1, '2017-09-24 15:13:46', '2017-09-24 15:13:46', '', 'score-board', '', 'inherit', 'open', 'closed', '', 'score-board', '', '', '2017-09-24 15:13:46', '2017-09-24 15:13:46', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/score-board.png', 0, 'attachment', 'image/png', 0),
(13, 1, '2017-09-24 15:13:47', '2017-09-24 15:13:47', '', 'wallhaven-3178', '', 'inherit', 'open', 'closed', '', 'wallhaven-3178', '', '', '2017-09-24 15:13:47', '2017-09-24 15:13:47', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-3178.jpg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2017-09-24 15:13:50', '2017-09-24 15:13:50', '', 'wallhaven-3178-thumb', '', 'inherit', 'open', 'closed', '', 'wallhaven-3178-thumb', '', '', '2017-09-24 15:13:50', '2017-09-24 15:13:50', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-3178-thumb.jpg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2017-09-24 15:13:51', '2017-09-24 15:13:51', '', 'wallhaven-10742', '', 'inherit', 'open', 'closed', '', 'wallhaven-10742', '', '', '2017-09-24 15:13:51', '2017-09-24 15:13:51', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-10742.jpg', 0, 'attachment', 'image/jpeg', 0),
(16, 1, '2017-09-24 15:13:53', '2017-09-24 15:13:53', '', 'wallhaven-10742-thumb', '', 'inherit', 'open', 'closed', '', 'wallhaven-10742-thumb', '', '', '2017-09-24 15:13:53', '2017-09-24 15:13:53', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-10742-thumb.jpg', 0, 'attachment', 'image/jpeg', 0),
(17, 1, '2017-09-24 15:13:55', '2017-09-24 15:13:55', '', 'wallhaven-12018', '', 'inherit', 'open', 'closed', '', 'wallhaven-12018', '', '', '2017-09-24 15:13:55', '2017-09-24 15:13:55', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-12018.jpg', 0, 'attachment', 'image/jpeg', 0),
(18, 1, '2017-09-24 15:13:57', '2017-09-24 15:13:57', '', 'wallhaven-12018-thumbnail', '', 'inherit', 'open', 'closed', '', 'wallhaven-12018-thumbnail', '', '', '2017-09-24 15:13:57', '2017-09-24 15:13:57', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-12018-thumbnail.jpg', 0, 'attachment', 'image/jpeg', 0),
(19, 1, '2017-09-24 15:13:58', '2017-09-24 15:13:58', '', 'wallhaven-16270', '', 'inherit', 'open', 'closed', '', 'wallhaven-16270', '', '', '2017-09-24 15:13:58', '2017-09-24 15:13:58', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-16270.jpg', 0, 'attachment', 'image/jpeg', 0),
(20, 1, '2017-09-24 15:14:00', '2017-09-24 15:14:00', '', 'wallhaven-16270-thumbnail', '', 'inherit', 'open', 'closed', '', 'wallhaven-16270-thumbnail', '', '', '2017-09-24 15:14:00', '2017-09-24 15:14:00', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-16270-thumbnail.jpg', 0, 'attachment', 'image/jpeg', 0),
(21, 1, '2017-09-24 15:14:01', '2017-09-24 15:14:01', '', 'wallhaven-27263', '', 'inherit', 'open', 'closed', '', 'wallhaven-27263', '', '', '2017-09-24 15:14:01', '2017-09-24 15:14:01', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-27263.jpg', 0, 'attachment', 'image/jpeg', 0),
(22, 1, '2017-09-24 15:14:03', '2017-09-24 15:14:03', '', 'wallhaven-27263-thumbnail', '', 'inherit', 'open', 'closed', '', 'wallhaven-27263-thumbnail', '', '', '2017-09-24 15:14:03', '2017-09-24 15:14:03', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/wallhaven-27263-thumbnail.jpg', 0, 'attachment', 'image/jpeg', 0),
(23, 1, '2017-09-24 15:14:05', '2017-09-24 15:14:05', '', 'ab', '', 'inherit', 'open', 'closed', '', 'ab', '', '', '2017-09-24 15:14:05', '2017-09-24 15:14:05', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/ab.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2017-09-24 15:14:06', '2017-09-24 15:14:06', '', 'ab1', '', 'inherit', 'open', 'closed', '', 'ab1', '', '', '2017-09-24 15:14:06', '2017-09-24 15:14:06', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/ab1.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2017-09-24 15:14:07', '2017-09-24 15:14:07', '', 'arrows', '', 'inherit', 'open', 'closed', '', 'arrows', '', '', '2017-09-24 15:14:07', '2017-09-24 15:14:07', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/arrows.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2017-09-24 15:14:10', '2017-09-24 15:14:10', '', 'contact-bg', '', 'inherit', 'open', 'closed', '', 'contact-bg', '', '', '2017-09-24 15:14:10', '2017-09-24 15:14:10', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/contact-bg.jpg', 0, 'attachment', 'image/jpeg', 0),
(27, 1, '2017-09-24 15:14:12', '2017-09-24 15:14:12', '', 'exam', '', 'inherit', 'open', 'closed', '', 'exam', '', '', '2017-09-24 15:14:12', '2017-09-24 15:14:12', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/exam.jpg', 0, 'attachment', 'image/jpeg', 0),
(28, 1, '2017-09-24 15:14:30', '2017-09-24 15:14:30', '', 'g1', '', 'inherit', 'open', 'closed', '', 'g1', '', '', '2017-09-24 15:14:30', '2017-09-24 15:14:30', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/g1.jpg', 0, 'attachment', 'image/jpeg', 0),
(29, 1, '2017-09-24 15:14:33', '2017-09-24 15:14:33', '', 'g2', '', 'inherit', 'open', 'closed', '', 'g2', '', '', '2017-09-24 15:14:33', '2017-09-24 15:14:33', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/g2.jpg', 0, 'attachment', 'image/jpeg', 0),
(30, 1, '2017-09-24 15:14:35', '2017-09-24 15:14:35', '', 'g3', '', 'inherit', 'open', 'closed', '', 'g3', '', '', '2017-09-24 15:14:35', '2017-09-24 15:14:35', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/g3.jpg', 0, 'attachment', 'image/jpeg', 0),
(31, 1, '2017-09-24 15:14:38', '2017-09-24 15:14:38', '', 'g4', '', 'inherit', 'open', 'closed', '', 'g4', '', '', '2017-09-24 15:14:38', '2017-09-24 15:14:38', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/g4.jpg', 0, 'attachment', 'image/jpeg', 0),
(32, 1, '2017-09-24 15:14:40', '2017-09-24 15:14:40', '', 'g5', '', 'inherit', 'open', 'closed', '', 'g5', '', '', '2017-09-24 15:14:40', '2017-09-24 15:14:40', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/g5.jpg', 0, 'attachment', 'image/jpeg', 0),
(33, 1, '2017-09-24 15:17:48', '2017-09-24 15:17:48', '', '01', '', 'inherit', 'open', 'closed', '', '01', '', '', '2017-09-24 15:17:48', '2017-09-24 15:17:48', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/01.jpg', 0, 'attachment', 'image/jpeg', 0),
(34, 1, '2017-09-24 15:17:52', '2017-09-24 15:17:52', '', '02', '', 'inherit', 'open', 'closed', '', '02', '', '', '2017-09-24 15:17:52', '2017-09-24 15:17:52', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/02.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 1, '2017-09-24 15:17:57', '2017-09-24 15:17:57', '', '03', '', 'inherit', 'open', 'closed', '', '03', '', '', '2017-09-24 15:17:57', '2017-09-24 15:17:57', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/03.jpg', 0, 'attachment', 'image/jpeg', 0),
(36, 1, '2017-09-24 15:18:02', '2017-09-24 15:18:02', '', '04', '', 'inherit', 'open', 'closed', '', '04', '', '', '2017-09-24 15:18:02', '2017-09-24 15:18:02', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/04.jpg', 0, 'attachment', 'image/jpeg', 0),
(37, 1, '2017-09-24 15:18:07', '2017-09-24 15:18:07', '', '05', '', 'inherit', 'open', 'closed', '', '05', '', '', '2017-09-24 15:18:07', '2017-09-24 15:18:07', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/05.jpg', 0, 'attachment', 'image/jpeg', 0),
(38, 1, '2017-09-24 15:18:12', '2017-09-24 15:18:12', '', '06', '', 'inherit', 'open', 'closed', '', '06', '', '', '2017-09-24 15:18:12', '2017-09-24 15:18:12', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/06.jpg', 0, 'attachment', 'image/jpeg', 0),
(39, 1, '2017-09-24 15:18:16', '2017-09-24 15:18:16', '', '07', '', 'inherit', 'open', 'closed', '', '07', '', '', '2017-09-24 15:18:16', '2017-09-24 15:18:16', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/07.jpg', 0, 'attachment', 'image/jpeg', 0),
(40, 1, '2017-09-24 15:18:20', '2017-09-24 15:18:20', '', '08', '', 'inherit', 'open', 'closed', '', '08', '', '', '2017-09-24 15:18:20', '2017-09-24 15:18:20', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/08.jpg', 0, 'attachment', 'image/jpeg', 0),
(41, 1, '2017-09-24 15:18:25', '2017-09-24 15:18:25', '', '09', '', 'inherit', 'open', 'closed', '', '09', '', '', '2017-09-24 15:18:25', '2017-09-24 15:18:25', '', 0, 'http://localhost/wpquiz/wp-content/uploads/2017/09/09.jpg', 0, 'attachment', 'image/jpeg', 0),
(42, 1, '2017-09-24 16:08:18', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-24 16:08:18', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?page_id=42', 0, 'page', '', 0),
(43, 1, '2017-09-24 16:09:42', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-24 16:09:42', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?page_id=43', 0, 'page', '', 0),
(44, 1, '2017-09-24 16:10:15', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-24 16:10:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?page_id=44', 0, 'page', '', 0),
(45, 1, '2017-09-25 03:20:19', '2017-09-25 03:20:19', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2017-09-25 05:58:01', '2017-09-25 05:58:01', '', 0, 'http://localhost/wpquiz/?page_id=45', 0, 'page', '', 0),
(46, 1, '2017-09-24 16:15:25', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-24 16:15:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?page_id=46', 0, 'page', '', 0),
(47, 1, '2017-09-25 03:20:19', '2017-09-25 03:20:19', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-09-25 03:20:19', '2017-09-25 03:20:19', '', 45, 'http://localhost/wpquiz/45-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2017-09-25 03:27:59', '2017-09-25 03:27:59', '\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac tellus in quam congue facilisis non ut nibh. Fusce ut arcu id massa vestibulum gravida et vitae est. Nullam ligula orci, laoreet eget nunc ac, dictum convallis erat. Integer bibendum egestas imperdiet. Nullam lobortis consectetur massa vitae fermentum. Mauris malesuada mauris a ante maximus convallis. Quisque vitae ipsum non eros cursus eleifend.\r\n\r\nSuspendisse ut molestie odio, ut porttitor ipsum. Nunc blandit velit erat, at tempor elit molestie non. Vestibulum sed mollis lacus, eget accumsan nisl. Maecenas turpis leo, fringilla ut pharetra in, imperdiet at nisi. Proin rhoncus eu ligula suscipit finibus. Donec tempor purus nibh, non vestibulum lorem posuere et. Donec tristique, mi non egestas iaculis, tellus erat interdum leo, nec molestie ligula lectus tristique nibh. Etiam mattis sem sed dolor cursus pharetra. Aenean ut libero ac nunc condimentum viverra vel eu mauris.\r\n\r\nDonec ut augue eu quam iaculis euismod a in enim. Cras viverra luctus velit, sed lobortis libero aliquam tempus. Mauris id massa odio. Praesent ac enim quis mi laoreet cursus ut ac turpis. Sed luctus semper iaculis. Morbi tincidunt pulvinar velit quis tempus. Duis ut elit sodales, iaculis metus id, scelerisque dui. Sed sed felis ac neque convallis varius in a metus. Praesent sed tristique libero. Donec sollicitudin est est, sed ultricies nibh auctor non. Quisque ut velit sit amet dolor iaculis vestibulum eu sed ligula.\r\n\r\nNam sollicitudin leo ac tortor feugiat, hendrerit rhoncus leo ornare. Vestibulum at leo nibh. Mauris eget rutrum sem, consequat pulvinar enim. Aenean vel dui odio. Donec consequat congue diam, ut luctus massa. Nam vel sagittis lorem, nec imperdiet justo. Nunc efficitur dui fermentum, lobortis massa a, rutrum odio.\r\n\r\nMauris eleifend, magna vitae iaculis scelerisque, nunc sem interdum tellus, lacinia accumsan mauris purus in sem. Curabitur tempus maximus odio, eu venenatis libero blandit eget. Sed dui est, suscipit et condimentum nec, consectetur nec felis. Integer molestie malesuada leo et semper. Donec porta felis vel mauris vulputate, vitae bibendum risus interdum. Pellentesque consectetur turpis vitae sapien volutpat, in finibus lectus dignissim. Integer gravida ante quam, quis tincidunt justo convallis id. Nullam viverra ligula feugiat arcu euismod congue at ut metus. Quisque varius nunc dui, gravida lobortis nisi condimentum id. ', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2017-09-29 01:51:23', '2017-09-29 01:51:23', '', 0, 'http://localhost/wpquiz/?page_id=48', 0, 'page', '', 0),
(49, 1, '2017-09-25 03:28:21', '2017-09-25 03:28:21', '\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac tellus in quam congue facilisis non ut nibh. Fusce ut arcu id massa vestibulum gravida et vitae est. Nullam ligula orci, laoreet eget nunc ac, dictum convallis erat. Integer bibendum egestas imperdiet. Nullam lobortis consectetur massa vitae fermentum. Mauris malesuada mauris a ante maximus convallis. Quisque vitae ipsum non eros cursus eleifend.\r\n\r\nSuspendisse ut molestie odio, ut porttitor ipsum. Nunc blandit velit erat, at tempor elit molestie non. Vestibulum sed mollis lacus, eget accumsan nisl. Maecenas turpis leo, fringilla ut pharetra in, imperdiet at nisi. Proin rhoncus eu ligula suscipit finibus. Donec tempor purus nibh, non vestibulum lorem posuere et. Donec tristique, mi non egestas iaculis, tellus erat interdum leo, nec molestie ligula lectus tristique nibh. Etiam mattis sem sed dolor cursus pharetra. Aenean ut libero ac nunc condimentum viverra vel eu mauris.\r\n\r\nDonec ut augue eu quam iaculis euismod a in enim. Cras viverra luctus velit, sed lobortis libero aliquam tempus. Mauris id massa odio. Praesent ac enim quis mi laoreet cursus ut ac turpis. Sed luctus semper iaculis. Morbi tincidunt pulvinar velit quis tempus. Duis ut elit sodales, iaculis metus id, scelerisque dui. Sed sed felis ac neque convallis varius in a metus. Praesent sed tristique libero. Donec sollicitudin est est, sed ultricies nibh auctor non. Quisque ut velit sit amet dolor iaculis vestibulum eu sed ligula.\r\n\r\nNam sollicitudin leo ac tortor feugiat, hendrerit rhoncus leo ornare. Vestibulum at leo nibh. Mauris eget rutrum sem, consequat pulvinar enim. Aenean vel dui odio. Donec consequat congue diam, ut luctus massa. Nam vel sagittis lorem, nec imperdiet justo. Nunc efficitur dui fermentum, lobortis massa a, rutrum odio.\r\n\r\nMauris eleifend, magna vitae iaculis scelerisque, nunc sem interdum tellus, lacinia accumsan mauris purus in sem. Curabitur tempus maximus odio, eu venenatis libero blandit eget. Sed dui est, suscipit et condimentum nec, consectetur nec felis. Integer molestie malesuada leo et semper. Donec porta felis vel mauris vulputate, vitae bibendum risus interdum. Pellentesque consectetur turpis vitae sapien volutpat, in finibus lectus dignissim. Integer gravida ante quam, quis tincidunt justo convallis id. Nullam viverra ligula feugiat arcu euismod congue at ut metus. Quisque varius nunc dui, gravida lobortis nisi condimentum id.\r\n', 'Categories', '', 'publish', 'closed', 'closed', '', 'categories', '', '', '2017-09-29 03:09:08', '2017-09-29 03:09:08', '', 0, 'http://localhost/wpquiz/?page_id=49', 0, 'page', '', 0),
(50, 1, '2017-09-25 03:28:40', '2017-09-25 03:28:40', '', 'login', '', 'publish', 'closed', 'closed', '', 'login', '', '', '2017-09-25 03:28:40', '2017-09-25 03:28:40', '', 0, 'http://localhost/wpquiz/?page_id=50', 0, 'page', '', 0),
(51, 1, '2017-09-25 03:28:55', '2017-09-25 03:28:55', '', 'Student', '', 'publish', 'closed', 'closed', '', 'student', '', '', '2017-09-25 03:28:55', '2017-09-25 03:28:55', '', 0, 'http://localhost/wpquiz/?page_id=51', 0, 'page', '', 0),
(52, 1, '2017-09-25 03:29:07', '2017-09-25 03:29:07', '', 'Faq', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2017-09-25 03:29:07', '2017-09-25 03:29:07', '', 0, 'http://localhost/wpquiz/?page_id=52', 0, 'page', '', 0),
(53, 1, '2017-09-25 03:29:20', '2017-09-25 03:29:20', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387142.84010033106!2d-74.25819252532891!3d40.70583163828471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1475140387172\" style=\"border:0\"></iframe>', 'Contact us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2017-09-29 02:53:29', '2017-09-29 02:53:29', '', 0, 'http://localhost/wpquiz/?page_id=53', 0, 'page', '', 0),
(54, 1, '2017-09-25 03:27:59', '2017-09-25 03:27:59', '', 'About Us', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2017-09-25 03:27:59', '2017-09-25 03:27:59', '', 48, 'http://localhost/wpquiz/48-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2017-09-25 03:28:21', '2017-09-25 03:28:21', '', 'Categories', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2017-09-25 03:28:21', '2017-09-25 03:28:21', '', 49, 'http://localhost/wpquiz/49-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2017-09-25 03:28:40', '2017-09-25 03:28:40', '', 'login', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2017-09-25 03:28:40', '2017-09-25 03:28:40', '', 50, 'http://localhost/wpquiz/50-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2017-09-25 03:28:55', '2017-09-25 03:28:55', '', 'Student', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2017-09-25 03:28:55', '2017-09-25 03:28:55', '', 51, 'http://localhost/wpquiz/51-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2017-09-25 03:29:07', '2017-09-25 03:29:07', '', 'Faq', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2017-09-25 03:29:07', '2017-09-25 03:29:07', '', 52, 'http://localhost/wpquiz/52-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2017-09-25 03:29:20', '2017-09-25 03:29:20', '', 'Contact us', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2017-09-25 03:29:20', '2017-09-25 03:29:20', '', 53, 'http://localhost/wpquiz/53-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2017-09-25 03:30:39', '2017-09-25 03:30:39', ' ', '', '', 'publish', 'closed', 'closed', '', '60', '', '', '2017-09-25 03:39:59', '2017-09-25 03:39:59', '', 0, 'http://localhost/wpquiz/?p=60', 7, 'nav_menu_item', '', 0),
(61, 1, '2017-09-25 03:30:39', '2017-09-25 03:30:39', ' ', '', '', 'publish', 'closed', 'closed', '', '61', '', '', '2017-09-25 03:39:59', '2017-09-25 03:39:59', '', 0, 'http://localhost/wpquiz/?p=61', 6, 'nav_menu_item', '', 0),
(62, 1, '2017-09-25 03:30:39', '2017-09-25 03:30:39', ' ', '', '', 'publish', 'closed', 'closed', '', '62', '', '', '2017-09-25 03:39:59', '2017-09-25 03:39:59', '', 0, 'http://localhost/wpquiz/?p=62', 5, 'nav_menu_item', '', 0),
(63, 1, '2017-09-25 03:30:38', '2017-09-25 03:30:38', ' ', '', '', 'publish', 'closed', 'closed', '', '63', '', '', '2017-09-25 03:39:59', '2017-09-25 03:39:59', '', 0, 'http://localhost/wpquiz/?p=63', 4, 'nav_menu_item', '', 0),
(64, 1, '2017-09-25 03:30:37', '2017-09-25 03:30:37', ' ', '', '', 'publish', 'closed', 'closed', '', '64', '', '', '2017-09-25 03:39:59', '2017-09-25 03:39:59', '', 0, 'http://localhost/wpquiz/?p=64', 3, 'nav_menu_item', '', 0),
(65, 1, '2017-09-25 03:30:37', '2017-09-25 03:30:37', ' ', '', '', 'publish', 'closed', 'closed', '', '65', '', '', '2017-09-25 03:39:59', '2017-09-25 03:39:59', '', 0, 'http://localhost/wpquiz/?p=65', 2, 'nav_menu_item', '', 0),
(66, 1, '2017-09-25 03:30:36', '2017-09-25 03:30:36', ' ', '', '', 'publish', 'closed', 'closed', '', '66', '', '', '2017-09-25 03:39:59', '2017-09-25 03:39:59', '', 0, 'http://localhost/wpquiz/?p=66', 1, 'nav_menu_item', '', 0),
(67, 1, '2017-09-25 04:07:33', '2017-09-25 04:07:33', '', 'Home Page fields', '', 'publish', 'closed', 'closed', '', 'acf_home-page-fields', '', '', '2017-09-25 05:41:30', '2017-09-25 05:41:30', '', 0, 'http://localhost/wpquiz/?post_type=acf&#038;p=67', 0, 'acf', '', 0),
(68, 1, '2017-09-25 04:11:56', '2017-09-25 04:11:56', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-09-25 04:11:56', '2017-09-25 04:11:56', '', 45, 'http://localhost/wpquiz/45-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2017-09-25 04:20:05', '2017-09-25 04:20:05', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-09-25 04:20:05', '2017-09-25 04:20:05', '', 45, 'http://localhost/wpquiz/45-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2017-09-25 04:32:14', '2017-09-25 04:32:14', '', 'ab', '', 'inherit', 'open', 'closed', '', 'ab-2', '', '', '2017-09-25 04:32:14', '2017-09-25 04:32:14', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/ab-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(71, 1, '2017-09-25 04:33:12', '2017-09-25 04:33:12', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-09-25 04:33:12', '2017-09-25 04:33:12', '', 45, 'http://localhost/wpquiz/45-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2017-09-25 04:38:59', '2017-09-25 04:38:59', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-09-25 04:38:59', '2017-09-25 04:38:59', '', 45, 'http://localhost/wpquiz/45-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2017-09-25 04:48:56', '2017-09-25 04:48:56', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-09-25 04:48:56', '2017-09-25 04:48:56', '', 45, 'http://localhost/wpquiz/45-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2017-09-25 05:37:36', '2017-09-25 05:37:36', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-09-25 05:37:36', '2017-09-25 05:37:36', '', 45, 'http://localhost/wpquiz/45-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2017-09-25 05:38:26', '2017-09-25 05:38:26', '', 'icon-cs', '', 'inherit', 'open', 'closed', '', 'icon-cs', '', '', '2017-09-25 05:38:26', '2017-09-25 05:38:26', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/icon-cs.svg', 0, 'attachment', 'image/svg+xml', 0),
(76, 1, '2017-09-25 05:38:39', '2017-09-25 05:38:39', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-09-25 05:38:39', '2017-09-25 05:38:39', '', 45, 'http://localhost/wpquiz/45-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2017-09-25 05:40:43', '2017-09-25 05:40:43', '', 'icon-ece', '', 'inherit', 'open', 'closed', '', 'icon-ece', '', '', '2017-09-25 05:40:43', '2017-09-25 05:40:43', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/icon-ece.svg', 0, 'attachment', 'image/svg+xml', 0),
(78, 1, '2017-09-25 05:42:07', '2017-09-25 05:42:07', '', 'icon-it', '', 'inherit', 'open', 'closed', '', 'icon-it', '', '', '2017-09-25 05:42:07', '2017-09-25 05:42:07', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/icon-it.svg', 0, 'attachment', 'image/svg+xml', 0),
(79, 1, '2017-09-25 05:43:00', '2017-09-25 05:43:00', '', 'icon-aero', '', 'inherit', 'open', 'closed', '', 'icon-aero', '', '', '2017-09-25 05:43:00', '2017-09-25 05:43:00', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/icon-aero.svg', 0, 'attachment', 'image/svg+xml', 0),
(80, 1, '2017-09-25 05:43:36', '2017-09-25 05:43:36', '', 'icon-civil', '', 'inherit', 'open', 'closed', '', 'icon-civil', '', '', '2017-09-25 05:43:36', '2017-09-25 05:43:36', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/icon-civil.svg', 0, 'attachment', 'image/svg+xml', 0),
(81, 1, '2017-09-25 05:44:18', '2017-09-25 05:44:18', '', 'icon-eee', '', 'inherit', 'open', 'closed', '', 'icon-eee', '', '', '2017-09-25 05:44:18', '2017-09-25 05:44:18', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/icon-eee.svg', 0, 'attachment', 'image/svg+xml', 0),
(82, 1, '2017-09-25 05:44:58', '2017-09-25 05:44:58', '', 'icon-mechnical', '', 'inherit', 'open', 'closed', '', 'icon-mechnical', '', '', '2017-09-25 05:44:58', '2017-09-25 05:44:58', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/icon-mechnical.svg', 0, 'attachment', 'image/svg+xml', 0),
(83, 1, '2017-09-25 05:45:33', '2017-09-25 05:45:33', '', 'icon-automobile', '', 'inherit', 'open', 'closed', '', 'icon-automobile', '', '', '2017-09-25 05:45:33', '2017-09-25 05:45:33', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/icon-automobile.svg', 0, 'attachment', 'image/svg+xml', 0),
(84, 1, '2017-09-25 05:46:04', '2017-09-25 05:46:04', '', 'icon-biomedical', '', 'inherit', 'open', 'closed', '', 'icon-biomedical', '', '', '2017-09-25 05:46:04', '2017-09-25 05:46:04', '', 45, 'http://localhost/wpquiz/wp-content/uploads/2017/09/icon-biomedical.svg', 0, 'attachment', 'image/svg+xml', 0),
(85, 1, '2017-09-25 05:46:16', '2017-09-25 05:46:16', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-09-25 05:46:16', '2017-09-25 05:46:16', '', 45, 'http://localhost/wpquiz/45-revision-v1/', 0, 'revision', '', 0),
(86, 1, '2017-09-25 05:52:39', '2017-09-25 05:52:39', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel consequat est. Curabitur commodo, velit aliquet sollicitudin ultricies, lorem neque aliquet urna, vitae aliquet orci diam vel risus. Nullam suscipit nisl in orci facilisis, a hendrerit velit tempor. Duis id turpis interdum, pellentesque nulla at, sagittis velit. Mauris venenatis ex quis nibh dapibus, quis porttitor orci fermentum. Cras iaculis vel sem ut malesuada. Nulla in ante ultricies neque elementum placerat. Sed nec lorem a libero ultricies ornare ac nec lorem. Nullam cursus venenatis felis. Fusce at velit sed eros pulvinar congue. Suspendisse dolor enim, varius ut iaculis ut, dignissim sed metus. Morbi eu gravida mi, et euismod tortor.\r\n\r\nSed eu justo ullamcorper, consequat tortor sed, pulvinar mi. Integer congue risus in nibh rutrum, quis ultrices leo mattis. Mauris in mi orci. Vestibulum varius non elit quis tincidunt. Quisque eleifend egestas consequat. Vestibulum blandit tellus ac cursus luctus. Nam at tristique sem. Mauris at turpis quam. Donec arcu magna, varius vehicula diam non, dapibus pharetra elit.\r\n\r\nDuis sed dolor id augue consectetur laoreet. Sed fermentum posuere ex, eu gravida urna facilisis a. Pellentesque fermentum in nisl blandit laoreet. Vestibulum congue, erat et molestie ullamcorper, erat massa vulputate nunc, a sagittis orci ipsum id eros. Mauris ac lorem rhoncus, lacinia neque vel, rhoncus dui. Ut eget nibh ut lectus finibus porttitor at non ligula. Aenean felis metus, pellentesque et varius ac, venenatis at velit. Sed posuere orci vel arcu egestas viverra. Aenean malesuada tincidunt augue pretium pellentesque. Maecenas sit amet mauris bibendum, facilisis massa a, accumsan ex.', 'Nullam tristique est a elementum pellentesque', '', 'publish', 'open', 'open', '', 'nullam-tristique-est-a-elementum-pellentesque', '', '', '2017-09-25 05:52:39', '2017-09-25 05:52:39', '', 0, 'http://localhost/wpquiz/?p=86', 0, 'post', '', 0),
(87, 1, '2017-09-25 05:53:06', '2017-09-25 05:53:06', 'Duis sed dolor id augue consectetur laoreet. Sed fermentum posuere ex, eu gravida urna facilisis a. Pellentesque fermentum in nisl blandit laoreet. Vestibulum congue, erat et molestie ullamcorper, erat massa vulputate nunc, a sagittis orci ipsum id eros. Mauris ac lorem rhoncus, lacinia neque vel, rhoncus dui. Ut eget nibh ut lectus finibus porttitor at non ligula. Aenean felis metus, pellentesque et varius ac, venenatis at velit. Sed posuere orci vel arcu egestas viverra. Aenean malesuada tincidunt augue pretium pellentesque. Maecenas sit amet mauris bibendum, facilisis massa a, accumsan ex.\r\n\r\nNullam est eros, placerat sit amet fermentum ac, lacinia vel lectus. Duis justo enim, dapibus eget blandit tempor, ultrices nec justo. Ut at feugiat lectus. Nullam sit amet mi aliquam dui fringilla scelerisque. Aliquam a enim augue. Morbi vitae magna ipsum. Suspendisse blandit non dui condimentum lobortis. Phasellus in arcu venenatis, tempor metus vitae, ultricies augue. Pellentesque sollicitudin augue eget gravida tristique. Aliquam porta sodales enim ac commodo. Nam ultrices efficitur est luctus scelerisque. Etiam ut est purus. Vivamus condimentum, nibh a pellentesque volutpat, augue justo sagittis nisi, ut scelerisque massa tortor at sapien. Maecenas eget sodales orci, ac accumsan dui. Donec dignissim euismod justo eu faucibus. Morbi euismod commodo lectus venenatis blandit.\r\n\r\nSuspendisse pretium commodo hendrerit. Integer mattis vitae ante non varius. Cras tincidunt venenatis arcu, vel bibendum turpis. Integer vitae nibh facilisis, fringilla metus elementum, ultricies nibh. Ut luctus a nisi non fermentum. Donec lobortis, est non rutrum rutrum, ex tortor commodo arcu, imperdiet laoreet quam lectus et massa. Aenean suscipit mi odio, sit amet maximus erat dignissim ac.', 'Quisque vel eros vitae massa finibus efficitur', '', 'publish', 'open', 'open', '', 'quisque-vel-eros-vitae-massa-finibus-efficitur', '', '', '2017-09-25 05:53:06', '2017-09-25 05:53:06', '', 0, 'http://localhost/wpquiz/?p=87', 0, 'post', '', 0),
(88, 1, '2017-09-25 05:53:40', '2017-09-25 05:53:40', 'Sed eu justo ullamcorper, consequat tortor sed, pulvinar mi. Integer congue risus in nibh rutrum, quis ultrices leo mattis. Mauris in mi orci. Vestibulum varius non elit quis tincidunt. Quisque eleifend egestas consequat. Vestibulum blandit tellus ac cursus luctus. Nam at tristique sem. Mauris at turpis quam. Donec arcu magna, varius vehicula diam non, dapibus pharetra elit.\r\n\r\nDuis sed dolor id augue consectetur laoreet. Sed fermentum posuere ex, eu gravida urna facilisis a. Pellentesque fermentum in nisl blandit laoreet. Vestibulum congue, erat et molestie ullamcorper, erat massa vulputate nunc, a sagittis orci ipsum id eros. Mauris ac lorem rhoncus, lacinia neque vel, rhoncus dui. Ut eget nibh ut lectus finibus porttitor at non ligula. Aenean felis metus, pellentesque et varius ac, venenatis at velit. Sed posuere orci vel arcu egestas viverra. Aenean malesuada tincidunt augue pretium pellentesque. Maecenas sit amet mauris bibendum, facilisis massa a, accumsan ex.\r\n\r\nNullam est eros, placerat sit amet fermentum ac, lacinia vel lectus. Duis justo enim, dapibus eget blandit tempor, ultrices nec justo. Ut at feugiat lectus. Nullam sit amet mi aliquam dui fringilla scelerisque. Aliquam a enim augue. Morbi vitae magna ipsum. Suspendisse blandit non dui condimentum lobortis. Phasellus in arcu venenatis, tempor metus vitae, ultricies augue. Pellentesque sollicitudin augue eget gravida tristique. Aliquam porta sodales enim ac commodo. Nam ultrices efficitur est luctus scelerisque. Etiam ut est purus. Vivamus condimentum, nibh a pellentesque volutpat, augue justo sagittis nisi, ut scelerisque massa tortor at sapien. Maecenas eget sodales orci, ac accumsan dui. Donec dignissim euismod justo eu faucibus. Morbi euismod commodo lectus venenatis blandit.', 'Neque porro quisquam est qui dolorem ipsum', '', 'publish', 'open', 'open', '', 'neque-porro-quisquam-est-qui-dolorem-ipsum', '', '', '2017-09-25 05:53:40', '2017-09-25 05:53:40', '', 0, 'http://localhost/wpquiz/?p=88', 0, 'post', '', 0),
(89, 1, '2017-09-25 05:52:39', '2017-09-25 05:52:39', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel consequat est. Curabitur commodo, velit aliquet sollicitudin ultricies, lorem neque aliquet urna, vitae aliquet orci diam vel risus. Nullam suscipit nisl in orci facilisis, a hendrerit velit tempor. Duis id turpis interdum, pellentesque nulla at, sagittis velit. Mauris venenatis ex quis nibh dapibus, quis porttitor orci fermentum. Cras iaculis vel sem ut malesuada. Nulla in ante ultricies neque elementum placerat. Sed nec lorem a libero ultricies ornare ac nec lorem. Nullam cursus venenatis felis. Fusce at velit sed eros pulvinar congue. Suspendisse dolor enim, varius ut iaculis ut, dignissim sed metus. Morbi eu gravida mi, et euismod tortor.\r\n\r\nSed eu justo ullamcorper, consequat tortor sed, pulvinar mi. Integer congue risus in nibh rutrum, quis ultrices leo mattis. Mauris in mi orci. Vestibulum varius non elit quis tincidunt. Quisque eleifend egestas consequat. Vestibulum blandit tellus ac cursus luctus. Nam at tristique sem. Mauris at turpis quam. Donec arcu magna, varius vehicula diam non, dapibus pharetra elit.\r\n\r\nDuis sed dolor id augue consectetur laoreet. Sed fermentum posuere ex, eu gravida urna facilisis a. Pellentesque fermentum in nisl blandit laoreet. Vestibulum congue, erat et molestie ullamcorper, erat massa vulputate nunc, a sagittis orci ipsum id eros. Mauris ac lorem rhoncus, lacinia neque vel, rhoncus dui. Ut eget nibh ut lectus finibus porttitor at non ligula. Aenean felis metus, pellentesque et varius ac, venenatis at velit. Sed posuere orci vel arcu egestas viverra. Aenean malesuada tincidunt augue pretium pellentesque. Maecenas sit amet mauris bibendum, facilisis massa a, accumsan ex.', 'Nullam tristique est a elementum pellentesque', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2017-09-25 05:52:39', '2017-09-25 05:52:39', '', 86, 'http://localhost/wpquiz/86-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2017-09-25 05:53:06', '2017-09-25 05:53:06', 'Duis sed dolor id augue consectetur laoreet. Sed fermentum posuere ex, eu gravida urna facilisis a. Pellentesque fermentum in nisl blandit laoreet. Vestibulum congue, erat et molestie ullamcorper, erat massa vulputate nunc, a sagittis orci ipsum id eros. Mauris ac lorem rhoncus, lacinia neque vel, rhoncus dui. Ut eget nibh ut lectus finibus porttitor at non ligula. Aenean felis metus, pellentesque et varius ac, venenatis at velit. Sed posuere orci vel arcu egestas viverra. Aenean malesuada tincidunt augue pretium pellentesque. Maecenas sit amet mauris bibendum, facilisis massa a, accumsan ex.\r\n\r\nNullam est eros, placerat sit amet fermentum ac, lacinia vel lectus. Duis justo enim, dapibus eget blandit tempor, ultrices nec justo. Ut at feugiat lectus. Nullam sit amet mi aliquam dui fringilla scelerisque. Aliquam a enim augue. Morbi vitae magna ipsum. Suspendisse blandit non dui condimentum lobortis. Phasellus in arcu venenatis, tempor metus vitae, ultricies augue. Pellentesque sollicitudin augue eget gravida tristique. Aliquam porta sodales enim ac commodo. Nam ultrices efficitur est luctus scelerisque. Etiam ut est purus. Vivamus condimentum, nibh a pellentesque volutpat, augue justo sagittis nisi, ut scelerisque massa tortor at sapien. Maecenas eget sodales orci, ac accumsan dui. Donec dignissim euismod justo eu faucibus. Morbi euismod commodo lectus venenatis blandit.\r\n\r\nSuspendisse pretium commodo hendrerit. Integer mattis vitae ante non varius. Cras tincidunt venenatis arcu, vel bibendum turpis. Integer vitae nibh facilisis, fringilla metus elementum, ultricies nibh. Ut luctus a nisi non fermentum. Donec lobortis, est non rutrum rutrum, ex tortor commodo arcu, imperdiet laoreet quam lectus et massa. Aenean suscipit mi odio, sit amet maximus erat dignissim ac.', 'Quisque vel eros vitae massa finibus efficitur', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2017-09-25 05:53:06', '2017-09-25 05:53:06', '', 87, 'http://localhost/wpquiz/87-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2017-09-25 05:53:40', '2017-09-25 05:53:40', 'Sed eu justo ullamcorper, consequat tortor sed, pulvinar mi. Integer congue risus in nibh rutrum, quis ultrices leo mattis. Mauris in mi orci. Vestibulum varius non elit quis tincidunt. Quisque eleifend egestas consequat. Vestibulum blandit tellus ac cursus luctus. Nam at tristique sem. Mauris at turpis quam. Donec arcu magna, varius vehicula diam non, dapibus pharetra elit.\r\n\r\nDuis sed dolor id augue consectetur laoreet. Sed fermentum posuere ex, eu gravida urna facilisis a. Pellentesque fermentum in nisl blandit laoreet. Vestibulum congue, erat et molestie ullamcorper, erat massa vulputate nunc, a sagittis orci ipsum id eros. Mauris ac lorem rhoncus, lacinia neque vel, rhoncus dui. Ut eget nibh ut lectus finibus porttitor at non ligula. Aenean felis metus, pellentesque et varius ac, venenatis at velit. Sed posuere orci vel arcu egestas viverra. Aenean malesuada tincidunt augue pretium pellentesque. Maecenas sit amet mauris bibendum, facilisis massa a, accumsan ex.\r\n\r\nNullam est eros, placerat sit amet fermentum ac, lacinia vel lectus. Duis justo enim, dapibus eget blandit tempor, ultrices nec justo. Ut at feugiat lectus. Nullam sit amet mi aliquam dui fringilla scelerisque. Aliquam a enim augue. Morbi vitae magna ipsum. Suspendisse blandit non dui condimentum lobortis. Phasellus in arcu venenatis, tempor metus vitae, ultricies augue. Pellentesque sollicitudin augue eget gravida tristique. Aliquam porta sodales enim ac commodo. Nam ultrices efficitur est luctus scelerisque. Etiam ut est purus. Vivamus condimentum, nibh a pellentesque volutpat, augue justo sagittis nisi, ut scelerisque massa tortor at sapien. Maecenas eget sodales orci, ac accumsan dui. Donec dignissim euismod justo eu faucibus. Morbi euismod commodo lectus venenatis blandit.', 'Neque porro quisquam est qui dolorem ipsum', '', 'inherit', 'closed', 'closed', '', '88-revision-v1', '', '', '2017-09-25 05:53:40', '2017-09-25 05:53:40', '', 88, 'http://localhost/wpquiz/88-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2017-09-27 05:13:39', '2017-09-27 05:13:39', '', 'Question1', '', 'publish', 'closed', 'closed', '', 'question1', '', '', '2017-09-27 05:13:39', '2017-09-27 05:13:39', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&#038;p=93', 0, 'quiz_questions', '', 0),
(94, 1, '2017-09-27 05:15:31', '2017-09-27 05:15:31', '', 'Quiz Questions fields', '', 'publish', 'closed', 'closed', '', 'acf_quiz-questions-fields', '', '', '2017-09-27 06:19:45', '2017-09-27 06:19:45', '', 0, 'http://localhost/wpquiz/?post_type=acf&#038;p=94', 0, 'acf', '', 0),
(95, 1, '2017-09-27 05:46:50', '2017-09-27 05:46:50', '', 'Questions Correct Answers', '', 'publish', 'closed', 'closed', '', 'acf_questions-correct-answers', '', '', '2017-09-27 05:57:17', '2017-09-27 05:57:17', '', 0, 'http://localhost/wpquiz/?post_type=acf&#038;p=95', 0, 'acf', '', 0),
(96, 1, '2017-09-27 05:54:17', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 05:54:17', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=96', 0, 'quiz_questions', '', 0),
(97, 1, '2017-09-27 05:55:13', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 05:55:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=97', 0, 'quiz_questions', '', 0),
(98, 1, '2017-09-27 05:57:23', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 05:57:23', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=98', 0, 'quiz_questions', '', 0),
(99, 1, '2017-09-27 05:57:36', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 05:57:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=99', 0, 'quiz_questions', '', 0),
(100, 1, '2017-09-27 06:04:35', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:04:35', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=100', 0, 'quiz_questions', '', 0),
(101, 1, '2017-09-27 06:05:57', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:05:57', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=101', 0, 'quiz_questions', '', 0),
(102, 1, '2017-09-27 06:06:01', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:06:01', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=102', 0, 'quiz_questions', '', 0),
(103, 1, '2017-09-27 06:06:58', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:06:58', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=103', 0, 'quiz_questions', '', 0),
(104, 1, '2017-09-27 06:07:21', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:07:21', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=104', 0, 'quiz_questions', '', 0),
(105, 1, '2017-09-27 06:08:42', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:08:42', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=105', 0, 'quiz_questions', '', 0),
(106, 1, '2017-09-27 06:08:49', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:08:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=106', 0, 'quiz_questions', '', 0),
(107, 1, '2017-09-27 06:10:33', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:10:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=107', 0, 'quiz_questions', '', 0),
(108, 1, '2017-09-27 06:11:20', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:11:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=108', 0, 'quiz_questions', '', 0),
(109, 1, '2017-09-27 06:11:29', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:11:29', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=109', 0, 'quiz_questions', '', 0),
(110, 1, '2017-09-27 06:23:19', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:23:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=110', 0, 'quiz_questions', '', 0),
(111, 1, '2017-09-27 06:24:00', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:24:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=111', 0, 'quiz_questions', '', 0),
(112, 1, '2017-09-27 06:26:15', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:26:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=112', 0, 'quiz_questions', '', 0),
(113, 1, '2017-09-27 06:33:04', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:33:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=113', 0, 'quiz_questions', '', 0),
(114, 1, '2017-09-27 06:33:40', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:33:40', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=114', 0, 'quiz_questions', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(115, 1, '2017-09-27 06:34:31', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-27 06:34:31', '0000-00-00 00:00:00', '', 0, 'http://localhost/wpquiz/?post_type=quiz_questions&p=115', 0, 'quiz_questions', '', 0),
(116, 1, '2017-09-29 01:51:23', '2017-09-29 01:51:23', '\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac tellus in quam congue facilisis non ut nibh. Fusce ut arcu id massa vestibulum gravida et vitae est. Nullam ligula orci, laoreet eget nunc ac, dictum convallis erat. Integer bibendum egestas imperdiet. Nullam lobortis consectetur massa vitae fermentum. Mauris malesuada mauris a ante maximus convallis. Quisque vitae ipsum non eros cursus eleifend.\r\n\r\nSuspendisse ut molestie odio, ut porttitor ipsum. Nunc blandit velit erat, at tempor elit molestie non. Vestibulum sed mollis lacus, eget accumsan nisl. Maecenas turpis leo, fringilla ut pharetra in, imperdiet at nisi. Proin rhoncus eu ligula suscipit finibus. Donec tempor purus nibh, non vestibulum lorem posuere et. Donec tristique, mi non egestas iaculis, tellus erat interdum leo, nec molestie ligula lectus tristique nibh. Etiam mattis sem sed dolor cursus pharetra. Aenean ut libero ac nunc condimentum viverra vel eu mauris.\r\n\r\nDonec ut augue eu quam iaculis euismod a in enim. Cras viverra luctus velit, sed lobortis libero aliquam tempus. Mauris id massa odio. Praesent ac enim quis mi laoreet cursus ut ac turpis. Sed luctus semper iaculis. Morbi tincidunt pulvinar velit quis tempus. Duis ut elit sodales, iaculis metus id, scelerisque dui. Sed sed felis ac neque convallis varius in a metus. Praesent sed tristique libero. Donec sollicitudin est est, sed ultricies nibh auctor non. Quisque ut velit sit amet dolor iaculis vestibulum eu sed ligula.\r\n\r\nNam sollicitudin leo ac tortor feugiat, hendrerit rhoncus leo ornare. Vestibulum at leo nibh. Mauris eget rutrum sem, consequat pulvinar enim. Aenean vel dui odio. Donec consequat congue diam, ut luctus massa. Nam vel sagittis lorem, nec imperdiet justo. Nunc efficitur dui fermentum, lobortis massa a, rutrum odio.\r\n\r\nMauris eleifend, magna vitae iaculis scelerisque, nunc sem interdum tellus, lacinia accumsan mauris purus in sem. Curabitur tempus maximus odio, eu venenatis libero blandit eget. Sed dui est, suscipit et condimentum nec, consectetur nec felis. Integer molestie malesuada leo et semper. Donec porta felis vel mauris vulputate, vitae bibendum risus interdum. Pellentesque consectetur turpis vitae sapien volutpat, in finibus lectus dignissim. Integer gravida ante quam, quis tincidunt justo convallis id. Nullam viverra ligula feugiat arcu euismod congue at ut metus. Quisque varius nunc dui, gravida lobortis nisi condimentum id. ', 'About Us', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2017-09-29 01:51:23', '2017-09-29 01:51:23', '', 48, 'http://localhost/wpquiz/48-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2017-09-29 02:24:09', '2017-09-29 02:24:09', '', 'Contact page fields', '', 'publish', 'closed', 'closed', '', 'acf_contact-page-fields', '', '', '2017-09-29 02:50:48', '2017-09-29 02:50:48', '', 0, 'http://localhost/wpquiz/?post_type=acf&#038;p=117', 0, 'acf', '', 0),
(118, 1, '2017-09-29 02:30:06', '2017-09-29 02:30:06', '', 'Contact us', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2017-09-29 02:30:06', '2017-09-29 02:30:06', '', 53, 'http://localhost/wpquiz/53-revision-v1/', 0, 'revision', '', 0),
(119, 1, '2017-09-29 02:38:10', '2017-09-29 02:38:10', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387142.84010033106!2d-74.25819252532891!3d40.70583163828471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1475140387172\" style=\"border:0\"></iframe>', 'Contact us', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2017-09-29 02:38:10', '2017-09-29 02:38:10', '', 53, 'http://localhost/wpquiz/53-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2017-09-29 02:52:35', '2017-09-29 02:52:35', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387142.84010033106!2d-74.25819252532891!3d40.70583163828471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1475140387172\" style=\"border:0\"></iframe>', 'Contact us', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2017-09-29 02:52:35', '2017-09-29 02:52:35', '', 53, 'http://localhost/wpquiz/53-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2017-09-29 02:53:29', '2017-09-29 02:53:29', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387142.84010033106!2d-74.25819252532891!3d40.70583163828471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1475140387172\" style=\"border:0\"></iframe>', 'Contact us', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2017-09-29 02:53:29', '2017-09-29 02:53:29', '', 53, 'http://localhost/wpquiz/53-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2017-09-29 03:09:08', '2017-09-29 03:09:08', '\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac tellus in quam congue facilisis non ut nibh. Fusce ut arcu id massa vestibulum gravida et vitae est. Nullam ligula orci, laoreet eget nunc ac, dictum convallis erat. Integer bibendum egestas imperdiet. Nullam lobortis consectetur massa vitae fermentum. Mauris malesuada mauris a ante maximus convallis. Quisque vitae ipsum non eros cursus eleifend.\r\n\r\nSuspendisse ut molestie odio, ut porttitor ipsum. Nunc blandit velit erat, at tempor elit molestie non. Vestibulum sed mollis lacus, eget accumsan nisl. Maecenas turpis leo, fringilla ut pharetra in, imperdiet at nisi. Proin rhoncus eu ligula suscipit finibus. Donec tempor purus nibh, non vestibulum lorem posuere et. Donec tristique, mi non egestas iaculis, tellus erat interdum leo, nec molestie ligula lectus tristique nibh. Etiam mattis sem sed dolor cursus pharetra. Aenean ut libero ac nunc condimentum viverra vel eu mauris.\r\n\r\nDonec ut augue eu quam iaculis euismod a in enim. Cras viverra luctus velit, sed lobortis libero aliquam tempus. Mauris id massa odio. Praesent ac enim quis mi laoreet cursus ut ac turpis. Sed luctus semper iaculis. Morbi tincidunt pulvinar velit quis tempus. Duis ut elit sodales, iaculis metus id, scelerisque dui. Sed sed felis ac neque convallis varius in a metus. Praesent sed tristique libero. Donec sollicitudin est est, sed ultricies nibh auctor non. Quisque ut velit sit amet dolor iaculis vestibulum eu sed ligula.\r\n\r\nNam sollicitudin leo ac tortor feugiat, hendrerit rhoncus leo ornare. Vestibulum at leo nibh. Mauris eget rutrum sem, consequat pulvinar enim. Aenean vel dui odio. Donec consequat congue diam, ut luctus massa. Nam vel sagittis lorem, nec imperdiet justo. Nunc efficitur dui fermentum, lobortis massa a, rutrum odio.\r\n\r\nMauris eleifend, magna vitae iaculis scelerisque, nunc sem interdum tellus, lacinia accumsan mauris purus in sem. Curabitur tempus maximus odio, eu venenatis libero blandit eget. Sed dui est, suscipit et condimentum nec, consectetur nec felis. Integer molestie malesuada leo et semper. Donec porta felis vel mauris vulputate, vitae bibendum risus interdum. Pellentesque consectetur turpis vitae sapien volutpat, in finibus lectus dignissim. Integer gravida ante quam, quis tincidunt justo convallis id. Nullam viverra ligula feugiat arcu euismod congue at ut metus. Quisque varius nunc dui, gravida lobortis nisi condimentum id.\r\n', 'Categories', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2017-09-29 03:09:08', '2017-09-29 03:09:08', '', 49, 'http://localhost/wpquiz/49-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main Menu', 'main-menu', 0),
(3, 'java', 'java', 0),
(4, 'c', 'c', 0),
(5, 'c++', 'c-2', 0),
(6, 'html', 'html', 0),
(7, 'dotNet', 'dotnet', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(60, 2, 0),
(61, 2, 0),
(62, 2, 0),
(63, 2, 0),
(64, 2, 0),
(65, 2, 0),
(66, 2, 0),
(86, 1, 0),
(87, 1, 0),
(88, 1, 0),
(93, 3, 0),
(93, 4, 0),
(93, 5, 0),
(93, 6, 0),
(93, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 4),
(2, 2, 'nav_menu', '', 0, 7),
(3, 3, 'category', '', 0, 1),
(4, 4, 'category', '', 0, 1),
(5, 5, 'category', '', 0, 1),
(6, 6, 'category', '', 0, 1),
(7, 7, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'siteadmin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '1'),
(15, 1, 'session_tokens', 'a:2:{s:64:\"004d51593711eab092449100605ef9f9c855cfaae36b9b9ae3dabfe4f3944cd5\";a:4:{s:10:\"expiration\";i:1506654433;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36\";s:5:\"login\";i:1506481633;}s:64:\"e4152a727578a0a4f859a145d824d8ed0f0c83da8781be714b091c47e78821ce\";a:4:{s:10:\"expiration\";i:1507859319;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:77:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0\";s:5:\"login\";i:1506649719;}}'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '3'),
(17, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(20, 1, 'wp_user-settings', 'hidetb=1&libraryContent=browse&editor=html'),
(21, 1, 'wp_user-settings-time', '1506318766'),
(22, 1, 'closedpostboxes_quiz_questions', 'a:0:{}'),
(23, 1, 'metaboxhidden_quiz_questions', 'a:2:{i:0;s:6:\"acf_67\";i:1;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'siteadmin', '$P$BP5zXaLDIvM.7qQVy7Cerng/j01y6x0', 'siteadmin', 'deeraj16@gmail.com', '', '2017-09-24 10:05:23', '', 0, 'siteadmin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_custom_options_plus`
--
ALTER TABLE `wp_custom_options_plus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_custom_options_plus`
--
ALTER TABLE `wp_custom_options_plus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=980;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
